# Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
#
# Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# 
# Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
# 
#



HEADERS += \
    include/Vecteur.h \
    include/Systeme.h \
    include/Ressort.h \
    include/PlanFini.h \
    include/Plan.h \
    include/Pendule.h \
    include/Obstacle.h \
    include/ObjetMobile.h \
    include/IntegrateurEuler.h \
    include/IntegrateurRungeKutta.h \
    include/IntegrateurNewmark.h \
    include/Integrateur.h \
    include/Element.h \
    include/Dessinable.h \
    include/ChampForces.h \
    include/Brique.h \
    include/Balle.h \
    include/GUI/gui.h \
    include/Balle3d.h \
    include/Pendule3d.h \
    include/Ressort3d.h \
    include/Plan3d.h \
    include/PlanFini3d.h \
    include/Brique3d.h \
    include/GUI/FenetrePrincipale.h \
    include/GUI/AjouteElementDock.h \
    include/Couleur.h \
    include/ObjetCompose.h \
    include/Vent.h \
    include/Trampoline.h \
    include/Ventilateur.h \
    include/Ventilateur3d.h \
    include/Trampoline3d.h \
    include/Parseur.h \
    include/ChampForcesGlobal.h

SOURCES += \
    src/Vecteur.cpp \
    src/Systeme.cpp \
    src/Ressort.cpp \
    src/PlanFini.cpp \
    src/Plan.cpp \
    src/Pendule.cpp \
    src/Obstacle.cpp \
    src/ObjetMobile.cpp \
    src/IntegrateurEuler.cpp \
    src/IntegrateurRungeKutta.cpp \
    src/IntegrateurNewmark.cpp \
    src/Integrateur.cpp \
    src/Element.cpp \
    src/Dessinable.cpp \
    src/ChampForces.cpp \
    src/Brique.cpp \
    src/Balle.cpp \
    src/GUI/gui.cpp \
    src/Balle3d.cpp \
    src/Pendule3d.cpp \
    src/Ressort3d.cpp \
    src/Plan3d.cpp \
    src/PlanFini3d.cpp \
    src/Brique3d.cpp \
    src/GUI/FenetrePrincipale.cpp \
    src/GUI/AjouteElementDock.cpp \
    src/Couleur.cpp \
    src/ObjetCompose.cpp \
    src/Vent.cpp \
    src/Trampoline.cpp \
    src/Ventilateur.cpp \
    src/Ventilateur3d.cpp \
    src/Trampoline3d.cpp \
    src/Parseur.cpp \
    src/ChampForcesGlobal.cpp \
    src/Goldberg.cpp


QMAKE_CXXFLAGS += -std=c++0x

QMAKE_CXX = g++

OBJECTS_DIR = obj/

QT += opengl

LIBS += -lglut -lGL -lGLU

DESTDIR = bin/

MOC_DIR = moc/

RCC_DIR = rc/

RESOURCES += \
    src/icones.qrc










































































