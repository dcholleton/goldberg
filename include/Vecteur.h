/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Vecteur.h
 * @brief Header de la classe Vecteur
 */
#ifndef VECTEUR_H
#define VECTEUR_H

#include <vector>
#include <iostream>

/**
 * @class Vecteur
 * @brief Classe repr�sentant un Vecteur de dimension quelconque
 */
class Vecteur
{
    public:

        /**
         * @brief Construit un Vecteur nul dont la dimension est pass�e en argument
         * @param dimension La dimension du Vecteur
         */
        Vecteur(double dimension);

        /**
         * @brief Construit un Vecteur de dimension 3 initialis� par les coordonn�es x,y et z pass�es en arguments
         * @param x Premi�re coordonn�e du Vecteur
         * @param y Deuxi�me coordonn�e du Vecteur
         * @param z Troisi�me coordonn�e du Vecteur
         */
        Vecteur(double x, double y, double z);

        /**
         * @brief Construit un Vecteur gr�ce � la liste d'initialisation pass�e en argument
         * @param coordonnees Liste d'initialisation qui permet de construire le Vecteur, sous la forme : { x , y , ... , z }
         */
        Vecteur(std::initializer_list<double> const& coordonnees);

        /**
         * @brief Ajoute au Vecteur courant une composante initialis�e � la valeur pass�e en argument
         * @param valeur Valeur � laquelle est initialis�e la nouvelle composantes du Vecteur, par d�fault 0
         */
        void augmente(double const& valeur = 0.);

        /**
         * @brief Accesseur pour Vecteur::m_composantes
         * @return Retourne le tableau dynamique (Vecteur::m_composantes) des composantes du Vecteur courant
         */
        std::vector<double> const& composantes() const;


        /**
         * @brief Cette m�thode sert � obtenir la dimension du Vecteur
         * @return Retourne la dimension du Vecteur
         */
        size_t size() const;

        /**
         * @brief Surcharge interne de l'op�rateur "[ ]"
         * @brief Accesseur pour une composante de Vecteur::m_composantes
         * @param index Index de la composante du Vecteur demand�e
         * @return Retourne une r�f�rence constante sur la composante du Vecteur demand�e
         */
        double const& operator[](size_t const& index) const;

        /**
         * @brief Surcharge interne de l'op�rateur "( )"
         * @brief Manipulateur pour une composante de Vecteur::m_composantes
         * @param index Index de la composante du Vecteur demand�e
         * @return Retourne une r�f�rence sur la composante du Vecteur demand�e
         */
        double& operator()(size_t const& index);

        /**
         * @brief Surchage externe de l'op�rateur "<<"
         * @brief Cette m�thode affiche les composantes du Vecteur, en ligne, entre parenth�ses, s�par�es par un espace. Si le vecteur est Vide, on affiche "(Vecteur vide)"
         * @param out Flux de sortie
         * @param a_afficher Le Vecteur � afficher
         * @return Retourne le flux
         * @note Fonction amie de la classe Vecteur
         * @note D�finie par rapport � la m�thode Vecteur::affiche
         */
        friend std::ostream& operator<<(std::ostream& out, Vecteur const& a_afficher);

        /**
         * @brief Surchage interne de l'operateur "=="
         * @param a_comparer Vecteur � comparer au Vecteur courant
         * @return Retourne 0 si les Vecteurs sont de tailles diff�rents, ou de tailles identiques mais avec des composantes diff�rentes
         * @return Retourne 1 si les Vecteurs sont �gaux
         */
        bool operator==(Vecteur const& a_comparer) const;

        /**
         * @brief Surchage interne de l'operateur "!="
         * @param a_comparer Vecteur � comparer au Vecteur courant
         * @return Retourne 1 si les Vecteurs sont de tailles diff�rents, ou de tailles identiques mais avec des composantes diff�rentes
         * @return Retourne 0 si les Vecteurs sont �gaux
         * @note D�fini � partir de Vecteur::operator==
         */
        bool operator!=(Vecteur const& a_comparer) const;

        /**
         * @brief Surcharge interne de l'op�rateur "+="
         * @brief Ajoute au Vecteur courant le Vecteur pass� en argument
         * @param a_ajouter Vecteur � ajouter au Vecteur courant
         * @return Retourne une r�f�rence sur le Vecteur courant
         * @note Si les deux Vecteurs sont de tailles diff�rentes, alors la m�thode consid�re les coordonn�es manquantes du plus petit comme nulles
         */
        Vecteur& operator+=(Vecteur const& a_ajouter);

        /**
         * @brief Surcharge interne de l'op�rateur "+"
         * @param a_ajouter Vecteur � sommer avec le Vecteur courant
         * @return Retourne la somme du Vecteur courant et du Vecteur pass� en argument
         * @note Si les deux Vecteurs sont de tailles diff�rentes, alors la m�thode consid�re les coordonn�es manquantes du plus petit comme nulles
         * @note D�fini � partir de Vecteur::operator+=
         */
        Vecteur operator+(Vecteur const& a_ajouter) const;

        /**
         * @brief Surcharge interne de l'op�rateur "-" unaire
         * @return Retourne le Vecteur oppos� au Vecteur courant
         */
        Vecteur operator-() const;

        /**
         * @brief Surcharge interne de l'op�rateur "-="
         * @brief Soustrait au Vecteur courant le Vecteur pass� en argument
         * @param a_soustraire Vecteur � soustraire au Vecteur courant
         * @return Retourne une r�f�rence sur le Vecteur courant
         * @note Si les deux Vecteurs sont de tailles diff�rentes, alors la m�thode consid�re les coordonn�es manquantes du plus petit comme nulles
         * @note D�fini � partir de Vecteur::operator+= et Vecteur::operator-
         */
        Vecteur& operator-=(Vecteur const& a_soustraire);

        /**
         * @brief Surcharge interne de l'op�rateur "-"
         * @param a_soustraire Vecteur � soustraire avec le Vecteur courant
         * @return Retourne la diff�rence des deux Vecteurs
         * @note Si les deux Vecteurs sont de tailles diff�rentes, alors la m�thode consid�re les coordonn�es manquantes du plus petit comme nulles
         * @note D�fini � partir de Vecteur::operator-=
         */
        Vecteur operator-(Vecteur const& a_soustraire) const;

        /**
         * @brief Surcharge interne de l'op�rateur "*="
         * @brief Multiplie le Vecteur courant par le scalaire pass� en argument
         * @param scalaire Scalaire multipliant le Vecteur courant
         * @return Retourne une r�f�rence sur le Vecteur courant
         */
        Vecteur& operator*=(double const& scalaire);

        /**
         * @brief Surcharge externe de l'op�rateur "*"
         * @param scalaire Scalaire multipliant le Vecteur pass� en argument
         * @param a_multiplier Vecteur �tant multipli� par le scalaire pass� en argument
         * @return Retourne le Vecteur r�sultant de la multiplication scalaire entre le Vecteur et le scalaire pass�s en arguments
         * @note D�fini � partir de Vecteur::operator*=
         * @note Fonction amie de la classe Vecteur
         * @note On d�fini deux prototype pour cette m�thode afin d'assurer la commutativit�
         */
        friend Vecteur operator*(double scalaire, Vecteur const& a_multiplier);

        /**
         * @brief Surcharge externe de l'op�rateur "*"
         * @param a_multiplier Vecteur �tant multipli� par le scalaire pass� en argument
         * @param scalaire Scalaire multipliant le Vecteur pass� en argument
         * @return Retourne le Vecteur r�sultant de la multiplication scalaire entre le Vecteur et le scalaire pass�s en arguments
         * @note D�fini � partir de Vecteur::operator*=
         * @note Fonction amie de la classe Vecteur
         * @note On d�fini deux prototype pour cette m�thode afin d'assurer la commutativit�
         */
        friend Vecteur operator*(Vecteur const& a_multiplier, double scalaire);

        /**
         * @brief Surcharge interne de l'op�rateur "/="
         * @brief Divise le Vecteur courant par le scalaire pass� en argument
         * @param scalaire Scalaire divisant le Vecteur courant
         * @return Retourne une r�f�rence sur le Vecteur courant
         * @note D�fini � partir de Vecteur::operator*=
         */
        Vecteur& operator/=(double const& scalaire);

        /**
         * @brief Surcharge externe de l'op�rateur "/"
         * @param a_diviser Vecteur �tant divis� par le scalaire pass� en argument
         * @param scalaire Scalaire divisant le Vecteur pass� en argument
         * @return Retourne le Vecteur r�sultant de la division scalaire du Vecteur par le scalaire pass� en argument
         * @note D�fini � partir de Vecteur::operator/=
         * @note Fonction amie de la classe Vecteur
         */
        friend Vecteur operator/(Vecteur const& a_diviser, double scalaire);

        /**
         * @brief Surcharge interne de l'op�rateur "*"
         * @param a_multiplier_scal Vecteur � multiplier par le Vecteur courant
         * @return Retourne le produit scalaire des Vecteurs pass�s en arguments
         * @note Si les deux Vecteurs sont de tailles diff�rentes, alors la m�thode consid�re les coordonn�es manquantes du plus petit comme nulles
         */
        double operator*(Vecteur const& a_multiplier_scal) const;

        /**
         * @brief Surcharge interne de l'op�rateur "^="
         * @brief Multiplie le Vecteur courant par le Vecteur pass� en argument selon les r�gles du produit vectoriel
         * @param a_multiplier_vect Vecteur multipliant le Vecteur courant
         * @return Retourne une r�f�rence sur le Vecteur courant
         * @note Attention, on multiplie le Vecteur courant � droite par le Vecteur pass� en argument : a ^= b <=> a = a ^ b
         * @note La m�thode est effective uniquement pour deux Vecteurs de dimension 3, si ce n'est pas le cas, le Vecteur courant est r�initialis� au Vecteur vide
         */
        Vecteur& operator^=(Vecteur const& a_multiplier_vect);

        /**
         * @brief Surcharge interne de l'op�rateur "^"
         * @param a_multiplier_vect Vecteur multipliant le Vecteur courant
         * @return Retourne le produit vectoriel du Vecteur courant par le Vecteur pass� en argument
         * @note La m�thode est effective uniquement pour deux Vecteurs de dimension 3, si ce n'est pas le cas, le Vecteur courant est r�initialis� au Vecteur vide
         * @note D�fini � partir de Vecteur::operator^
         */
        Vecteur operator^(Vecteur const& a_multiplier_vect) const;

        /**
         * @brief Acc�s � la norme du Vecteur courant
         * @return Retourne la norme du Vecteur courant
         * @note Si on souhaite �lever par la suite cette norme au carr�, il vaut mieux utiliser Vecteur::norme_carre()
         */
        double norme() const;

        /**
         * @brief Acc�s au carr� de la norme du Vecteur courant
         * @return Retourne le carr� de la norme du Vecteur courant
         */
        double norme_carre() const;

        /**
         * @brief Donne le Vecteur unitaire sur la direction du Vecteur courant
         * @return Retourne le Vecteur unitaire sur la direction du Vecteur courant
         */
        Vecteur unitaire() const;

    private:

        /**
         * @brief Cette m�thode affiche les composantes du Vecteur courant, en ligne, entre parenth�ses, s�par�es par un espace. Si le vecteur est Vide, on affiche "(Vecteur vide)"
         * @param out Flux de sortie
         * @return Retourne le flux
         */
        void affiche(std::ostream& out) const;

        /**
         * @brief Tableau dynamique repr�sentant les composantes du Vecteur
         */
        std::vector<double> m_composantes;
};

#endif // VECTEUR_H
