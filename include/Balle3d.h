/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Balle3d.h
 * @brief Header de la classe Balle3D
 */
#ifndef BALLE3D_H
#define BALLE3D_H

#include <string>
#include <GL/glu.h>
#include "Balle.h"
#include "Vecteur.h"
#include "Couleur.h"



/**
 * @class Balle3D
 * @brief Classe repr�sentant une Balle pour l'affichage graphique
 * @note H�rite de Balle
 */
class Balle3D : public Balle
{
    public:
        /**
         * @brief Construit une Balle3D
         * @param position Position de la Balle3D, par d�fault le Vecteur nul de dimension 3
         * @param vitesse Vitesse de la Balle3D, par d�fault le Vecteur nul de dimension 3
         * @param rayon Rayon de la Balle3D, par d�fault 1
         * @param masse_volumique Masse volumique de la Balle3D, par d�fault 1
         * @param force Forces exerc�es sur la Balle3D, par d�fault le Vecteur nul de dimension 3
         * @param color Couleur de la Balle3D, par d�fault Couleur(0,0,1,1) (Bleu)
         */
        Balle3D(Vecteur const& position = Vecteur(3), Vecteur const& vitesse = Vecteur(3), double const& rayon = 1, double const& masse_volumique = 1, Vecteur const& force = Vecteur(3), Couleur const& color  = Couleur(0,0,1,1));

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Balle3D();

        /**
         * @brief Dessine la Balle3D courante
         */
        virtual void dessine() const;

        /**
         * @brief Sert � obtenir le type Balle3D sous forme de cha�ne de caract�res
         * @return Retourne std::string("Balle3D")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;


        /**
         * @brief Sert � envoyer l'�tat de la Balle3D dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;


    private:

        /**
         * @brief GLUquadric* pour l'affichage de la sph�re
         */
        GLUquadric* m_sphere;


};

#endif // BALLE3D_H
