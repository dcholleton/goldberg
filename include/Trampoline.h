/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Trampoline.h
 * @brief Header de la classe Trampoline
 */
#ifndef TRAMPOLINE_H
#define TRAMPOLINE_H

#include <string>
#include <iostream>
#include <QFormLayout>
#include "Obstacle.h"
#include "Vecteur.h"
#include "Couleur.h"
#include "ObjetMobile.h"
#include "ObjetCompose.h"
#include "Brique.h"
#include "Brique3d.h"
#include "Couleur.h"

/**
 * @class Trampoline
 * @brief Classe repr�sentant un Trampoline
 * @note H�rite de ObjetCompose
 */
class Trampoline: public ObjetCompose
{
    public:
        /**
         * @brief Construit un Trampoline
         * @param origine Origine du Trampoline, par d�fault le Vecteur nul de dimension 3
         * @param longueur Longueur du Trampoline, par d�fault le Vecteur unitaire sur l'axe x {1,0,0}
         * @param largeur Largeur du Trampoline, par d�fault le Vecteur unitaire sur l'axe y {0,1,0}
         * @param hauteur Hauteur du Trampoline, par d�fault 1
         * @param colorA Couleur de la face sup�rieure du Trampoline, par d�fault Couleur(0,1,1,1)
         * @param colorB Couleur de la face inf�rieure du Trampoline, par d�fault Couleur(1,0,1,1)
         * @param colorC Couleur de la face gauche du Trampoline, par d�fault Couleur(1,1,0,1)
         * @param colorD Couleur de la face droite du Trampoline, par d�fault Couleur(0.5,1,1,1)
         * @param colorE Couleur de la face arri�re du Trampoline, par d�fault Couleur(1,0.5,1,1)
         * @param colorF Couleur de la face avant du Trampoline, par d�fault Couleur(1,1,0.5,1)
         * @param alpha Coefficient de rebonds du Trampoline, par d�fault 1.2
         */
        Trampoline(Vecteur const& origine = Vecteur(3), Vecteur const& longueur = Vecteur(1,0,0), Vecteur const& largeur = Vecteur(0,1,0), double const& hauteur = 1, Couleur const& colorA = Couleur(0,1,1,1), Couleur const& colorB = Couleur(1,0,1,1), Couleur const& colorC = Couleur(1,1,0,1), Couleur const& colorD = Couleur(0.5,1,1,1), Couleur const& colorE = Couleur(1,0.5,1,1), Couleur const& colorF = Couleur(1,1,0.5,1), double const& alpha = 1.2);

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Trampoline();

        /**
          * @brief Ajoute le Trampoline courant au Systeme pass� en argument
          * @param syst Systeme auquel on ajoute le Trampoline courant
          */
        virtual void ajoute_a(Systeme* syst);

    protected:
        /**
         * @brief Socle du Trampoline
         */
        Brique* m_brique1;

        /**
         * @brief Bloc �lastique du Trampoline
         */
        Brique* m_brique2;

};

#endif /* TRAMPOLINE_H */
