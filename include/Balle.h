/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Balle.h
 * @brief Header de la classe Balle
 */
#ifndef BALLE_H
#define BALLE_H

#include <string>
#include <iostream>
#include <QFormLayout>
#include "Vecteur.h"
#include "Couleur.h"
#include "ObjetMobile.h"



/**
 * @class Balle
 * @brief Classe repr�sentant une Balle
 * @note H�rite de ObjetMobile
 */
class Balle : public ObjetMobile
{
    public:

        /**
         * @brief Construit une Balle
         * @param position Position de la Balle, par d�fault le Vecteur nul de dimension 3
         * @param vitesse Vitesse de la Balle, par d�fault le Vecteur nul de dimension 3
         * @param rayon Rayon de la Balle, par d�fault 1
         * @param masse_volumique Masse volumique de la Balle, par d�fault 1
         * @param force Forces exerc�es sur la Balle, par d�fault le Vecteur nul de dimension 3
         * @param color Couleur de la Balle, par d�fault Couleur(0,0,1,1) (Bleu)
         */
        Balle(Vecteur const& position = Vecteur(3), Vecteur const& vitesse = Vecteur(3), double const& rayon = 1, double const& masse_volumique = 1, Vecteur const& force = Vecteur(3), Couleur const& color  = Couleur(0,0,1,1));

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Balle();

        /**
         * @brief Fonction d'�volution pour la Balle
         * @return Retourne le Vecteur d�_omega
         */
        Vecteur evolution() const;

        /**
         * @brief Accesseur pour le Vecteur position de la Balle
         * @return Retourne une r�f�rence constante sur le Vecteur Balle::m_omega
         */
        virtual Vecteur position() const;

        /**
         * @brief Accesseur pour le Vecteur vitesse de la Balle
         * @return Retourne le Vecteur Balle::m_d_omega
         */
        virtual Vecteur vitesse() const;

        /**
         * @brief Manipulateur pour la vitesse
         * @param new_vitesse Nouvelle valeur pour la vitesse de la Balle
         */
        virtual void set_vitesse(Vecteur const& new_vitesse);

        /**
         * @brief Ajoute une force sur la Balle
         * @param df Vecteur repr�sentant la force � exercer sur la Balle
         */
        virtual void ajoute_force(Vecteur const& df);

        /**
         * @brief Sert � obtenir le type Balle sous forme de cha�ne de caract�res
         * @return Retourne std::string("Balle")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;

        /**
         * @brief Sert � cr�er un Layout contenant les propri�t�s de la Balle en vue de l'interface graphique
         * @return Retourne un pointeur sur un QFormLayout dont les champs contiennent les propri�t�s de la Balle
         */
        virtual QFormLayout* proprietesLayout() const;

        /**
         * @brief Sert � envoyer l'�tat de la Balle dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;



    protected:

        /**
         * @brief Affiche la Balle courante dans le flux pass� en argument
         * @param out Flux de sortie
         */
        virtual void affiche(std::ostream& out) const;

};

#endif // BALLE_H
