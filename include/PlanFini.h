/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file PlanFini.h
 * @brief Header de la classe PlanFini
 */
#ifndef PLANFINI_H
#define PLANFINI_H

#include <string>
#include <QFormLayout>
#include <iostream>
#include "Plan.h"
#include "Vecteur.h"
#include "Couleur.h"
#include "ObjetMobile.h"

/**
 * @class PlanFini
 * @brief Classe repr�sentant une portion de Plan
 * @note H�rite de Plan
 */
class PlanFini: public Plan
{
    public:
        /**
         * @brief Construit un PlanFini
         * @param origine Origine du PlanFini, par d�fault le Vecteur nul de dimension 3
         * @param normale Normale au PlanFini, par d�fault le Vecteur unitaire sur l'axe z {0,0,1}
         * @param longueur Longueur du PlanFini, par d�fault le Vecteur unitaire sur l'axe x {1,0,0}
         * @param largeur Largeur du PlanFini, par d�fault le Vecteur unitaire sur l'axe y {0,1,0}
         * @param color Couleur du PlanFini, par d�fault Couleur(1,1,1,1) (Blanc)
         * @param alpha Coefficient de rebonds du PlanFini, par d�fault 0.8
         */
        PlanFini(Vecteur const& origine = Vecteur(3), Vecteur const& normale = Vecteur(0,0,1), Vecteur const& longueur = Vecteur(1,0,0), Vecteur const& largeur = Vecteur(0,1,0), Couleur const& color = Couleur(1,1,1,1), double const& alpha = 0.8 );

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~PlanFini();

        /**
         * @brief Sert � trouver le point du PlanFini le plus proche de l'ObjetMobile pass� en argument
         * @param objet ObjetMobile dont on doit d�terminer le point le plus proche
         * @return Retourne le Vecteur des coordonn�es du point du PlanFini le plus proche de l'ObjetMobile
         */
        virtual Vecteur pointPlusProche(ObjetMobile const& objet) const;

        /**
         * @brief Accesseur pour PlanFini::m_longueur
         * @return Retourne une r�f�rence constante sur PlanFini::m_longueur
         */
        Vecteur const& longueur() const;

        /**
         * @brief Accesseur pour PlanFini::m_largeur
         * @return Retourne une r�f�rence constante sur PlanFini::m_largeur
         */
        Vecteur const& largeur() const;


        /**
         * @brief Sert � obtenir le type PlanFini sous forme de cha�ne de caract�res
         * @return Retourne std::string("PlanFini")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;

        /**
         * @brief Sert � cr�er un Layout contenant les propri�t�s du PlanFini en vue de l'interface graphique
         * @return Retourne un pointeur sur un QFormLayout dont les champs contiennent les propri�t�s du PlanFini
         */
        virtual QFormLayout* proprietesLayout() const;
			
		/**
         * @brief Sert � envoyer l'�tat du PlanFini dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;


    protected:

        /**
         * @brief Affiche le PlanFini courant dans le flux pass� en argument
         * @param out Flux de sortie
         */
        virtual void affiche(std::ostream& out) const;

    private:

        /**
         * @brief Longueur du Plan
         */
        Vecteur const m_longueur;

        /**
         * @brief Largeur du Plan
         */
        Vecteur const m_largeur;
};

#endif /* PLANFINI_H */
