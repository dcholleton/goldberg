/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Obstacle.h
 * @brief Header de la classe Obstacle
 */
#ifndef OBSTACLE_H
#define OBSTACLE_H

#include "Element.h"
#include "Vecteur.h"

class Systeme;

/**
 * @class Obstacle
 * @brief Classe abstraite repr�sentant un Obstacle
 * @note H�rite de Element
 */
class Obstacle: public Element
{
    public:
        /**
         * @brief Construit un Obstacle
         * @param origine Origine de l'Obstacle, par d�fault le Vecteur nul de dimension 3
         * @param normale Normale � l'Obstacle, par d�fault le Vecteur unitaire sur l'axe z {0,0,1}
         * @param alpha Coefficient de rebonds de l'Obstacle, par d�fault 0.8
         */
        Obstacle(Vecteur const& origine = Vecteur(3), Vecteur const& normale = Vecteur(0,0,1), double const& alpha = 0.8 );

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Obstacle();

        /**
         * @brief Sert � trouver le point de l'Obstacle le plus proche de l'ObjetMobile pass� en argument
         * @param objet ObjetMobile dont on doit d�terminer le point le plus proche
         * @return Retourne le Vecteur des coordonn�es du point de l'Obstacle le plus proche de l'ObjetMobile
         * @note M�thode virtuelle pure
         */
        virtual Vecteur pointPlusProche(ObjetMobile const& objet) const = 0;

        /**
         * @brief Agit sur l'ObjetMobile pass� en argument
         * @param objet ObjetMobile sur lequel on agit
         */
        virtual void agit_sur(ObjetMobile& objet);

        /**
          * @brief Ajoute l'Obstacle courant au Systeme pass� en argument
          * @param syst Systeme auquel on ajoute l'Obstacle courant
          */
        virtual void ajoute_a(Systeme* syst);

        /**
         * @brief M�thode pour acc�der � la distance entre l'Obstacle courant et l'ObjetMobile pass� en argument
         * @param objet ObjetMobile avec lequel on doit mesurer la distance
         */
        virtual double distance(ObjetMobile const& objet) const;

        /**
         * @brief Accesseur pour Obstacle::m_normale
         * @return Retourne une r�f�rence constante sur Obstacle::m_normale
         */
        Vecteur const& normale() const;

        /**
         * @brief Accesseur pour Obstacle::m_origine
         * @return Retourne une r�f�rence constante sur Obstacle::m_origine
         */
        Vecteur const& origine() const;

        /**
         * @brief Accesseur pour Obstacle::m_alpha
         * @return Retourne une r�f�rence constante sur Obstacle::m_alpha
         */		
        double const& alpha() const;


    private:
        /**
         * @brief Coordonn�es de l'origine l'Obstacle
         */
        Vecteur const m_origine;

        /**
         * @brief Vecteur normal � l'Obstacle
         */
        Vecteur const m_normale;
        
        /**
         * @brief Coefficient de rebonds de l'Obstacle
         */
        double const m_alpha;

};

#endif /* OBSTACLE_H */
