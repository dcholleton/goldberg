/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file IntegrateurNewmark.h
 * @brief Header de la classe IntegrateurNewmark
 */
#ifndef INTEGRATEURNEWMARK_H
#define INTEGRATEURNEWMARK_H

#include "Integrateur.h"
#include "ObjetMobile.h"
#include "Vecteur.h"

/**
 * @class IntegrateurNewmark
 * @brief Classe repr�sentant un Int�grateur, selon la m�thode de Newmark
 * @note H�rite de Integrateur
 */
class IntegrateurNewmark: public Integrateur
{
    public:

        /**
         * @brief Construit un IntegrateurNewmark
         * @param dt Pas de temps, par d�fault 0,01
         * @param epsilon Valeur du epsilon n�cessaire pour l'int�gration, par d�fault 0.00001
         */
        IntegrateurNewmark(double const& dt = 0.01, double const& epsilon = 0.00001 );

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~IntegrateurNewmark();

        /**
         * @brief Int�gre l'ObjetMobile pass� en argument, en utilisant sa fonction �volution
         * @param objet ObjetMobile � int�grer
         */
        virtual void integre(ObjetMobile& objet) const;

    private:
        /**
         * @brief Param�tre pour contr�ler la convergence
         */
        double m_epsilon;
};

#endif // INTEGRATEURNEWMARK_H 
