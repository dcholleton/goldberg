/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file IntegrateurRungeKutta.h
 * @brief Header de la classe IntegrateurRungeKutta
 */
#ifndef INTEGRATEURRUNGEKUTTA_H
#define INTEGRATEURRUNGEKUTTA_H

#include "Integrateur.h"
#include "ObjetMobile.h"
#include "Vecteur.h"

/**
 * @class IntegrateurRungeKutta
 * @brief Classe repr�sentant un Int�grateur, selon la m�thode de Runge Kutta
 * @note H�rite de Integrateur
 */
class IntegrateurRungeKutta: public Integrateur
{
    public:

        /**
         * @brief Construit un IntegrateurRungeKutta
         * @param dt Pas de temps, par d�fault 0,01
         */
        IntegrateurRungeKutta(double const& dt = 0.01);

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~IntegrateurRungeKutta();

        /**
         * @brief Int�gre l'ObjetMobile pass� en argument, en utilisant sa fonction �volution
         * @param objet ObjetMobile � int�grer
         */
        virtual void integre(ObjetMobile& objet) const;
};

#endif // INTEGRATEURRUNGEKUTTA_H 
