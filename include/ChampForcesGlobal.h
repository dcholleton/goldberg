/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file ChampForcesGlobal.h
 * @brief Header de la classe ChampForcesGlobal
 */
#ifndef CHAMPFORCESGLOBAL_H
#define CHAMPFORCESGLOBAL_H

#include "ChampForces.h"


/**
 * @class ChampForcesGlobal
 * @brief Classe repr�sentant un Champ de Forces Global
 * @note H�rite de ChampForces
 */
class ChampForcesGlobal : public ChampForces
{
    public:
    
		/**
		 * @brief Construit un ChampForcesGlobal
         * @param intensite Intensit� du champ de forces, par d�fault Constantes::g
         */
        ChampForcesGlobal(Vecteur intensite = Constantes::g);
        
        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~ChampForcesGlobal();
        
		/**
         * @brief Sert � obtenir le type ChampForcesGlobal sous forme de cha�ne de caract�res
         * @return Retourne std::string("Champ de Force Global")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;
        
		/**
         * @brief Sert � cr�er un Layout contenant les propri�t�s du ChampForcesGlobal en vue de l'interface graphique
         * @return Retourne un pointeur sur un QFormLayout dont les champs contiennent les propri�t�s du ChampForcesGlobal
         */
        virtual QFormLayout* proprietesLayout() const;
        
		/**
         * @brief Agit sur l'ObjetMobile pass� en argument
         * @param objet L'ObjetMobile sur lequel on agit
         */
        virtual void agit_sur(ObjetMobile& objet);
        
		/**
         * @brief Sert � envoyer l'�tat du ChampForcesGlobal dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;


    protected:

		/**
         * @brief Affiche le ChampForcesGlobal courant dans le flux pass� en argument
         * @param out Flux de sortie
         */
        virtual void affiche(std::ostream& out) const;


};

#endif // CHAMPFORCESGLOBAL_H
