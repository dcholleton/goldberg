/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Ressort.h
 * @brief Header de la classe Ressort
 */
#ifndef RESSORT_H
#define RESSORT_H

#include <iostream>
#include <string>
#include <QFormLayout>
#include "Vecteur.h"
#include "ObjetMobile.h"
#include "Couleur.h"


/**
 * @class Ressort
 * @brief Classe repr�sentant un Ressort
 * @note H�rite de ObjetMobile
 */

class Ressort : public ObjetMobile
{
    public:
        /**
         * @brief Constuit un Ressort
         * @param omega Position du Ressort sur sa direction, par d�fault le Vecteur nul de dimension 1
         * @param d_omega Vitesse du Ressort sur sa direction, par d�fault le Vecteur nul de dimension 1
         * @param rayon Rayon de la Boule accroch�e au Ressort, par d�fault 1
         * @param masse_volumique Masse volumique de la Boule accroch�e au Ressort, par d�fault 1
         * @param force Forces exerc�es sur la Boule accroch�e au Ressort, par d�fault le Vecteur nul de dimension 3
         * @param raideur Raideur du Ressort, par d�fault 10
         * @param frottement Coefficient de frottement du Ressort, par d�fault 1
         * @param direction Direction du Ressort, par d�faut le Vecteur unitaire sur l'axe x {1,0,0}
         * @param attache Point d'attache du Ressort, par d�fault le Vecteur nul de dimension 3
         * @param color Couleur du Ressort, par d�fault Couleur(0,1,0,1) (Vert)
         */
        Ressort(Vecteur const& omega = Vecteur(1), Vecteur const& d_omega = Vecteur(1), double const& rayon = 1, double const& masse_volumique = 1, Vecteur const& force = Vecteur(3), double const& raideur = 1 , double const& frottement = 1, Vecteur const& direction = Vecteur(1,0,0), Vecteur const& attache = Vecteur(3), Couleur const& color  = Couleur(0,1,0,1));

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Ressort();

        /**
         * @brief Fonction d'�volution pour le Ressort
         * @return Retourne le Vecteur d�_omega
         */
        virtual Vecteur evolution() const;

        /**
         * @brief Accesseur pour Ressort::m_raideur
         * @return Retourne une r�f�rence constante sur Ressort::m_raideur
         */
        double const& raideur() const;

        /**
         * @brief Accesseur pour Ressort::m_frottement
         * @return Retourne une r�f�rence constante sur Ressort::m_frottement
         */
        double const& frottement() const;

        /**
         * @brief Accesseur pour Ressort::m_direction
         * @return Retourne une r�f�rence constante sur Ressort::m_direction
         */
        Vecteur const& direction() const;

        /**
         * @brief Accesseur pour le Vecteur position de la Boule accroch�e au Ressort
         * @return Retourne le Vecteur position de la Boule accroch�e au Ressort
         */
        Vecteur position() const;

        /**
         * @brief Retourne la vitesse de la Boule accroch�e au Ressort
         * @return Retourne le Vecteur vitesse de la Boule accroch�e au Ressort
         */
        Vecteur vitesse() const;

        /**
         * @brief Manipulateur pour la vitesse
         * @param new_vitesse Nouvelle valeur pour la vitesse du Ressort
         */
        virtual void set_vitesse(Vecteur const& new_vitesse);

        /**
         * @brief Ajoute une force sur le Ressort
         * @param df Vecteur repr�sentant la force � exercer sur le Ressort
         */
        virtual void ajoute_force(Vecteur const& df);

        /**
        * @brief Accesseur pour le Vecteur Ressort::m_attache du Ressort
        * @return Retourne une r�f�rence constante sur le Vecteur Ressort::m_attache du Ressort
        */
        Vecteur const& attache() const;

        /**
         * @brief Sert � obtenir le type Ressort sous forme de cha�ne de caract�res
         * @return Retourne std::string("Ressort")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;

        /**
         * @brief Sert � cr�er un Layout contenant les propri�t�s du Ressort en vue de l'interface graphique
         * @return Retourne un pointeur sur un QFormLayout dont les champs contiennent les propri�t�s du Ressort
         */
        virtual QFormLayout* proprietesLayout() const;

		/**
         * @brief Sert � envoyer l'�tat du Ressort dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;


    protected:

        /**
         * @brief Affiche le Ressort courant dans le flux pass� en argument
         * @param out Flux de sortie
         */
        virtual void affiche(std::ostream& out) const;

    private:

        /**
         * @brief Constante de raideur du Ressort
         */
        double const m_raideur;

        /**
         * @brief Coefficient de frottement du Ressort
         */
        double const m_frottement;

        /**
         * @brief Vecteur unitaire selon la direction du Ressort
         */
        Vecteur const m_direction;

        /**
         * @brief Coordonn�es du point d'attache du Ressort
         */
        Vecteur const m_attache;

};

#endif
