/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Ventilateur3d.h
 * @brief Header de la classe Ventilateur3D
 */#ifndef VENTILATEUR3D_H
#define VENTILATEUR3D_H

#include <string>
#include <iostream>
#include <QFormLayout>
#include "Obstacle.h"
#include "Vecteur.h"
#include "Couleur.h"
#include "ObjetMobile.h"
#include "ObjetCompose.h"
#include "Brique.h"
#include "Vent.h"
#include "Brique3d.h"
#include "Ventilateur.h"

/**
 * @class Ventilateur3D
 * @brief Classe repr�sentant un Ventilateur pour l'affichage graphique
 * @note H�rite de Ventilateur
 */
class Ventilateur3D : public Ventilateur
{
    public:
    
		/**
         * @brief Construit un Ventilateur3D
         * @param origine Origine du Ventilateur3D, par d�fault le Vecteur nul de dimension 3
         * @param normale Normale au Ventilateur, par d�fault le Vecteur unitaire sur l'axe z {0,0,1}
         * @param longueur Longueur du Ventilateur3D, par d�fault le Vecteur unitaire sur l'axe x {1,0,0}
         * @param largeur Largeur du Ventilateur3D, par d�fault le Vecteur unitaire sur l'axe y {0,1,0}
         * @param hauteur Hauteur du Ventilateur3D, par d�fault 0.1
         * @param profondeur Profondeur de la zone d'effet du Ventilateur3D, par d�fault 1
         * @param colorA Couleur de la face inf�rieure du Ventilateur3D, par d�fault Couleur(0,1,1,1)
         * @param colorB Couleur de la face sup�rieure du Ventilateur3D, par d�fault Couleur(1,0,1,1)
         * @param colorC Couleur de la face gauche du Ventilateur3D, par d�fault Couleur(1,1,0,1)
         * @param colorD Couleur de la face droite du Ventilateur3D, par d�fault Couleur(0.5,1,1,1)
         * @param colorE Couleur de la face arri�re du Ventilateur3D, par d�fault Couleur(1,0.5,1,1)
         * @param colorF Couleur de la face avant du Ventilateur3D, par d�fault Couleur(1,1,0.5,1)
         * @param alpha Coefficient de rebonds du Ventilateur3D, par d�faut 0.8
         * @param intensite Intensit� du Ventilateur3D, par d�fault 3.5
         */
        Ventilateur3D(Vecteur const& origine = Vecteur(3), Vecteur const& normale = Vecteur(0,0,1), Vecteur const& longueur = Vecteur(1,0,0), Vecteur const& largeur = Vecteur(0,1,0), double const& hauteur = 0.1, double const& profondeur = 1, Couleur const& colorA = Couleur(1,1,1,1), Couleur const& colorB = Couleur(0.6,0.6,0.6,1), Couleur const& colorC = Couleur(0.5,0.5,0.5,1), Couleur const& colorD = Couleur(0.5,0.5,0.5,1), Couleur const& colorE = Couleur(0.3,0.3,0.3,1), Couleur const& colorF = Couleur(0.3,0.3,0.3,1), double const& alpha = 0.8, double const& intensite = 3.5);

		/**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Ventilateur3D();

		/**
          * @brief Ajoute le Ventilateur3D courant au Systeme pass� en argument
          * @param syst Systeme auquel on ajoute le Ventilateur3D courant
          */
        virtual void ajoute_a(Systeme* syst);
};

#endif // VENTILATEUR3D_H
