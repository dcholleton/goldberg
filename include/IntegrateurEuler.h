/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file IntegrateurEuler.h
 * @brief Header de la classe IntegrateurEuler
 */
#ifndef INTEGRATEUREULER_H
#define INTEGRATEUREULER_H

#include "Integrateur.h"
#include "ObjetMobile.h"

/**
 * @class IntegrateurEuler
 * @brief Classe repr�sentant un Int�grateur, selon la m�thode d'Euler-Cromer
 * @note H�rite de Integrateur
 */
class IntegrateurEuler: public Integrateur
{
    public:

        /**
         * @brief Construit un IntegrateurEuler
         * @param dt Pas de temps, par d�fault 0,01
         */
        IntegrateurEuler(double const& dt = 0.01);

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~IntegrateurEuler();

        /**
         * @brief Int�gre l'ObjetMobile pass� en argument, en utilisant sa fonction �volution
         * @param objet ObjetMobile � int�grer
         */
        virtual void integre(ObjetMobile& objet) const;
};

#endif // INTEGRATEUREULER_H
