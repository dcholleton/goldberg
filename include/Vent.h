/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Vent.h
 * @brief Header de la classe Vent
 */

#ifndef VENT_H
#define VENT_H

#include <QFormLayout>
#include "include/ChampForces.h"
#include "include/Vecteur.h"


/**
 * @class Vent
 * @brief Classe repr�sentant un champ de forces localis�
 * @note H�rite de ChampForces
 */
class Vent : public ChampForces
{
    public:

		/**
		 * @brief Construit un Vent
		 * @param origine Origine du Vent, par d�fault le Vecteur nul de dimension 3
		 * @param normale Normale au Vent (direction du souffle), par d�fault le Vecteur unitaire sur l'axe z {0,0,1}
         * @param longueur Longueur du Vent, par d�fault le Vecteur unitaire sur l'axe x {1,0,0}
         * @param largeur Largeur du Vent, par d�fault le Vecteur unitaire sur l'axe y {0,1,0}
         * @param profondeur Profondeur de la zone d'effet du Vent, par d�fault 1
         * @param intensite Intensit� du Vent, par d�fault 3.5
         */
        Vent(Vecteur const& origine = Vecteur(3), Vecteur const& normale = Vecteur (0, 0, 1), Vecteur const& longueur = Vecteur (1, 0, 0) , Vecteur const& largeur = Vecteur( 0, 1, 0), double const& profondeur = 1, double const& intensite = 3.5);
	
		/**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Vent();


		/**
         * @brief Agit sur l'ObjetMobile pass� en argument
         * @param objet ObjetMobile sur lequel on agit
         */
        virtual void agit_sur(ObjetMobile &objet);

		/**
		 * @brief Accesseur pour le Vecteur Vent::m_origine
		 * @return Retourne une r�f�rence constante sur Vent::m_origine
		 */
        Vecteur const& origine() const;
        
        /**
		 * @brief Accesseur pour le Vecteur Vent::m_normale
		 * @return Retourne une r�f�rence constante sur Vent::m_normale
		 */
        Vecteur const& normale() const;
        
        /**
		 * @brief Accesseur pour le Vecteur Vent::m_longueur
		 * @return Retourne une r�f�rence constante sur Vent::m_longueur
		 */
        Vecteur const& longueur() const;
        
        /**
		 * @brief Accesseur pour le Vecteur Vent::m_largeur
		 * @return Retourne une r�f�rence constante sur Vent::m_largeur
		 */
        Vecteur const& largeur() const;
        
        /**
		 * @brief Accesseur pour Vent::m_profondeur
		 * @return Retourne une r�f�rence constante sur Vent::m_profondeur
		 */
        double const& profondeur() const;

        /**
         * @brief Sert � obtenir le type Vent sous forme de cha�ne de caract�res
         * @return Retourne std::string("Vent")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;

		/**
         * @brief Sert � cr�er un Layout contenant les propri�t�s du Vent en vue de l'interface graphique
         * @return Retourne un pointeur sur un QFormLayout dont les champs contiennent les propri�t�s du Vent
         */
        virtual QFormLayout* proprietesLayout() const;

		/**
         * @brief Sert � envoyer l'�tat du Vent dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;


    private:

		/**
		 * @brief Origine du Vent
		 */
        Vecteur m_origine;
        
        /**
         * @brief Normale (et donc direction) du Vent
         */
        Vecteur m_normale;
        
        /**
         * @brief Longueur de la base de la zone d'effet du Vent
         */
        Vecteur m_longueur;
        
        /**
         * @brief Largeur de la base de la zone d'effet du Vent
         */
        Vecteur m_largeur;
        
        /**
         * @brief Profondeur de la zone d'effet du Vent
         */
        double m_profondeur;

    protected:
        
        /**
         * @brief Affiche le Champ de Force courant dans le flux pass� en argument
         * @param out Flux de sortie
         */
        virtual void affiche(std::ostream& out) const;


};

#endif // VENT_H
