/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Pendule.h
 * @brief Header de la classe Pendule
 */
#ifndef PENDULE_H
#define PENDULE_H

#include <string>
#include <QFormLayout>
#include <iostream>
#include "Vecteur.h"
#include "ObjetMobile.h"

/**
 * @class Pendule
 * @brief Classe repr�sentant un Pendule plan
 * @note H�rite de ObjetMobile
 */

class Pendule : public ObjetMobile
{
    public:

        /**
         * @brief Constuit un Pendule
         * @param omega Angle du Pendule, par d�fault le Vecteur nul de dimension 1
         * @param d_omega Vitesse Angulaire du Pendule, par d�fault le Vecteur nul de dimension 1
         * @param rayon Rayon de la Boule suspendue au Pendule, par d�fault 1
         * @param masse_volumique Masse volumique de la Boule suspendue au Pendule, par d�fault 1
         * @param force Forces exerc�es sur la Boule suspendue au Pendule, par d�fault le Vecteur nul de dimension 3
         * @param longueur Longueur du Pendule, par d�fault 10
         * @param frottement Coefficient de frottements du Pendule, par d�fault 1
         * @param direction Direction du Pendule, par d�faut le Vecteur unitaire sur l'axe x {1,0,0}
         * @param attache Point d'attache du Pendule, par d�fault le Vecteur nul de dimension 3
         * @param color Couleur du Pendule, par d�fault Couleur(1,0,0,1) (Rouge)
         */
        Pendule(Vecteur const& omega = Vecteur(1), Vecteur const& d_omega = Vecteur(1), double const& rayon = 0.1, double const& masse_volumique = 1, Vecteur const& force = Vecteur(3), double const& longueur = 10 , double const& frottement = 1, Vecteur const& direction = Vecteur(1,0,0), Vecteur const& attache = Vecteur(3), Couleur const& color  = Couleur(1,0,0,1));

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Pendule();

        /**
         * @brief Fonction d'�volution pour le Pendule
         * @return Retourne le Vecteur d�_omega
         */
        virtual Vecteur evolution() const;

        /**
         * @brief Accesseur pour Pendule::m_longueur
         * @return Retourne une r�f�rence constante sur Pendule::m_longueur
         */
        double const& longueur() const;

        /**
         * @brief Accesseur pour Pendule::m_frottement
         * @return Retourne une r�f�rence constante sur Pendule::m_frottement
         */
        double const& frottement() const;

        /**
         * @brief Accesseur pour Pendule::m_direction
         * @return Retourne une r�f�rence constante sur le Vecteur Pendule::direction
         */
        Vecteur const& direction() const;


        /**
         * @brief Accesseur pour le Vecteur position de la Boule suspendue au Pendule
         * @return Retourne le Vecteur position du centre de masse du Pendule
         */
        virtual Vecteur position() const;

        /**
         * @brief Accesseur pour le Vecteur vitesse de la Boule suspendue au Pendule
         * @return Retourne le Vecteur vitesse du centre de masse du Pendule
         */
        virtual Vecteur vitesse() const;

        /**
         * @brief Manipulateur pour la vitesse
         * @param new_vitesse Nouvelle valeur pour la vitesse du Pendule
         */
        virtual void set_vitesse(Vecteur const& new_vitesse);

        /**
         * @brief Ajoute une force sur le Pendule
         * @param df Vecteur repr�sentant la force � exercer sur le Pendule
         */
        virtual void ajoute_force(Vecteur const& df);

        /**
         * @brief Accesseur pour le Vecteur Pendule::m_attache du Pendule
         * @return Retourne une r�f�rence constante sur le Vecteur Pendule::m_attache du Pendule
         */
        Vecteur const& attache() const;

        /**
         * @brief Sert � obtenir le type Pendule sous forme de cha�ne de caract�res
         * @return Retourne std::string("Pendule")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;

        /**
         * @brief Sert � cr�er un Layout contenant les propri�t�s du Pendule en vue de l'interface graphique
         * @return Retourne un pointeur sur un QFormLayout dont les champs contiennent les propri�t�s du Pendule
         */
        virtual QFormLayout* proprietesLayout() const;

        /**
         * @brief Sert � envoyer l'�tat du Pendule dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;


    protected:

        /**
         * @brief Affiche le Pendule courant dans le flux pass� en argument
         * @param out Flux de sortie
         */
        virtual void affiche(std::ostream& out) const;

    private:

        /**
         * @brief Longueur du Pendule
         */
        double const m_longueur;

        /**
         * @brief Coefficient de frottement du Pendule
         */
        double const m_frottement;

        /**
         * @brief Vecteur unitaire normal � Constantes::g, dans le plan d'oscillation du Pendule
         */
        Vecteur const m_direction;

        /**
         * @brief Coordonn�es du point d'attache du Pendule
         */
        Vecteur const m_attache;

};

#endif
