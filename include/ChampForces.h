/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file ChampForces.h
 * @brief Header de la classe ChampForces
 */
#ifndef CHAMPFORCES_H
#define CHAMPFORCES_H

#include <string>
#include <iostream>
#include <QFormLayout>
#include "ObjetMobile.h"
#include "Vecteur.h"
#include "Element.h"

class Systeme;

/**
 * @brief Namespace contenant des constantes physiques
 */
namespace Constantes
{
/**
 * @brief Intensit� du champ de pesanteur
 */
Vecteur const g = {0.,0., -9.81};

/**
 * @brief Masse volumique de l'air
 */
double const RHO_AIR = 1.2;
}

/**
 * @class ChampForces
 * @brief Classe abstraite repr�sentant un Champ de Forces
 * @note H�rite de Element
 */
class ChampForces : public Element
{
    public:

        /**
         * @brief Construit un ChampForces
         * @param intensite Intensit� du champ de forces, par d�fault Constantes::g
         */
        ChampForces(Vecteur intensite = Vecteur(3));

		/**
         * @brief Destructeur, implant� pour le polymorphisme
         */
		virtual ~ChampForces();

        /**
          * @brief Ajoute le ChampForces courant au Systeme pass� en argument
          * @param syst Systeme auquel on ajoute le ChampForces courant
          */
        virtual void ajoute_a(Systeme* syst);

        /**
         * @brief Accesseur pour le Vecteur ChampForces::m_intensite
         * @return Retourne une r�f�rence constante sur ChampForces::m_intensite
         */
        Vecteur const& intensite() const;

		/**
		 * @brief M�thode inutile, impl�mant�e pour la forme (et par volont� du professeur)
		 */
        virtual double distance(ObjetMobile const& objet) const;


    private:

        /**
         * @brief Vecteur constant repr�sentant l'intensit� du champ de forces
         */
        Vecteur const m_intensite;

};

#endif // CHAMPFORCES_H
