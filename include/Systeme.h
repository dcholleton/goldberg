/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Systeme.h
 * @brief Header de la classe Systeme
 */
#ifndef SYSTEME_H
#define SYSTEME_H

#include <iostream>
#include <vector>
#include <memory>
#include "Integrateur.h"
#include "ObjetMobile.h"
#include "ChampForces.h"
#include "ObjetCompose.h"
#include "Obstacle.h"

/**
 * @class Systeme
 * @brief Classe repr�sentant un syst�me � simuler
 * @note H�rite de QObject
 */
class Systeme : public QObject
{
        Q_OBJECT

    public:
        /**
         * @brief Construit un Systeme dont les attributs Systeme::m_objets_mobiles, Systeme::m_obstacles et Systeme::m_champs sont des vector vides
         * @param dt Valeur du pas de temps, par d�fault 0,01
         * @param integrateur Type d'Integrateur � utiliser, par d�fault TypeIntegrateur::Euler
         */
        Systeme(double const& dt = 0.01, int const& integrateur = int(TypeIntegrateur::Euler));

        /**
         * @brief Suppression du constructeur de copie
         * @note Delete
         */
        Systeme(Systeme const&) = delete;

        /**
         * @brief Suppression de l'op�rateur de copie
         * @note Delete
         */
        Systeme& operator=(Systeme const&) = delete;

        /**
         * @brief M�thode d'�volution du syst�me
         */
        void evolue();

        /**
         * @brief Surcharge externe de l'op�rateur "<<"
         * @brief Affiche le Systeme
         * @param out Flux de sortie
         * @param a_afficher Systeme � afficher
         * @return Retourne le flux de sortie
         * @note Fonction amie de la classe Systeme
         * @note D�fini par rapport � Systeme::dessine()
         */
        friend std::ostream& operator<<(std::ostream& out, Systeme const& a_afficher);

        /**
         * @brief Affiche le Systeme en bouclant sur Systeme::m_objets_mobiles, Systeme::m_obstacles et Systeme::m_champs et en appelant la m�thode dessine de chaque �l�ment
         */
        void dessine() const;

        /**
         * @brief Accesseur pour Systeme::m_objets_mobiles
         * @return Retourne une r�f�rence constante sur Systeme::m_objets_mobiles
         */
        std::vector< std::unique_ptr<ObjetMobile> >& objets_mobiles();

        /**
         * @brief Accesseur pour Systeme::m_obstacles
         * @return Retourne une r�f�rence constante sur Systeme::m_obstacles
         */
        std::vector< std::unique_ptr<Obstacle> >& obstacles();

        /**
         * @brief Accesseur pour Systeme::m_champs
         * @return Retourne une r�f�rence constante sur Systeme::m_champs
         */
        std::vector< std::unique_ptr<ChampForces> >& champs();

        /**
         * @brief Accesseur pour le pas de temps de Systeme::m_integrateur
         * @return Retourne la valeur du pas de temps de Systeme::m_integrateur
         */
        double dt() const;

    public slots:

        /**
         * @brief Manipulateur pour le pas de temps de Systeme::m_integrateur
         * @param new_dt Nouveau pas de temps pour Systeme::m_integrateur
         */
        void set_dt(double const& new_dt);

        /**
         * @brief Ajoute un unique_ptr sur un ObjetMobile au vector Systeme::m_objets_mobiles
         * @param objet Pointeur sur l'ObjetMobile � ajouter au Systeme
         */
        void ajoute(ObjetMobile* objet);

        /**
         * @brief Ajoute un unique_ptr sur un Obstacle au vector Systeme::m_obstacles
         * @param obstacle Pointeur sur l'Obstacle � ajouter au Systeme
         */
        void ajoute(Obstacle* obstacle);

        /**
         * @brief Ajoute un unique_ptr sur un ChampForces au vector Systeme::m_champs
         * @param champ Pointeur sur le ChampForces � ajouter au Systeme
         */
        void ajoute(ChampForces* champ);

        /**
         * @brief Ajoute un ObjetCompose au Systeme
         * @param objet Pointeur sur l'ObjetCompose � ajouter au Systeme
         */
        void ajoute(ObjetCompose* objet);

		/**
		 * @brief Manipulateur pour le type d'Integrateur
		 * @param index Num�ro du type d'Integrateur (cf TypeIntegrateur)
		 */
        void set_integrateur(int const& index);

		/**
		 * @brief Supprime tous les Elements du Systeme courant
		 */
        void reset();

    private:
        /**
         * @brief Collection de std::unique_ptr sur des objets de type ObjetMobile
         */
        std::vector< std::unique_ptr<ObjetMobile> > m_objets_mobiles;

        /**
         * @brief Collection de std::unique_ptr sur des objets de type Obstacle
         */
        std::vector< std::unique_ptr<Obstacle> >  m_obstacles;

        /**
         * @brief Collection de std::unique_ptr sur des objets de type ChampForces
         */
        std::vector< std::unique_ptr<ChampForces> > m_champs;

        /**
         * @brief L'int�grateur pour la m�thode Systeme::evolue
         */
        std::unique_ptr<Integrateur> m_integrateur;

    signals:

        /**
         * @brief Signal �mit lors de l'ajout d'un ObjetMobile au Systeme
         * @param objet Pointeur sur l'ObjetMobile ajout� au Systeme
         */
        void newObjet(ObjetMobile* objet);

        /**
         * @brief Signal �mit lors de l'ajout d'un Obstacle au Systeme
         * @param obstacle Pointeur sur l'Obstacle ajout� au Systeme
         */
        void newObstacle(Obstacle* obstacle);

        /**
         * @brief Signal �mit lors de l'ajout d'un ChampForces au Systeme
         * @param champ Pointeur sur le ChampForces ajout� au Systeme
         */
        void newChamp(ChampForces* champ);

};

#endif /* SYSTEME_H */
