/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Ventilateur.h
 * @brief Header de la classe Ventilateur
 */
#ifndef VENTILATEUR_H
#define VENTILATEUR_H

#include <string>
#include <iostream>
#include <QFormLayout>
#include "Obstacle.h"
#include "Vecteur.h"
#include "Couleur.h"
#include "ObjetMobile.h"
#include "ObjetCompose.h"
#include "Brique.h"
#include "Vent.h"
#include "Brique3d.h"

/**
 * @class Ventilateur
 * @brief Classe repr�sentant un Ventilateur
 * @note H�rite de ObjetCompose
 */
class Ventilateur: public ObjetCompose
{
    public:
        /**
         * @brief Construit un Ventilateur
         * @param origine Origine du Ventilateur, par d�fault le Vecteur nul de dimension 3
         * @param normale Normale au Ventilateur, par d�fault le Vecteur unitaire sur l'axe z {0,0,1}
         * @param longueur Longueur du Ventilateur, par d�fault le Vecteur unitaire sur l'axe x {1,0,0}
         * @param largeur Largeur du Ventilateur, par d�fault le Vecteur unitaire sur l'axe y {0,1,0}
         * @param hauteur Hauteur du Ventilateur, par d�fault 0.1
         * @param profondeur Profondeur de la zone d'effet du Ventilateur, par d�fault 1
         * @param colorA Couleur de la face inf�rieure du Ventilateur, par d�fault Couleur(0,1,1,1)
         * @param colorB Couleur de la face sup�rieure du Ventilateur, par d�fault Couleur(1,0,1,1)
         * @param colorC Couleur de la face gauche du Ventilateur, par d�fault Couleur(1,1,0,1)
         * @param colorD Couleur de la face droite du Ventilateur, par d�fault Couleur(0.5,1,1,1)
         * @param colorE Couleur de la face arri�re du Ventilateur, par d�fault Couleur(1,0.5,1,1)
         * @param colorF Couleur de la face avant du Ventilateur, par d�fault Couleur(1,1,0.5,1)
         * @param alpha Coefficient de rebonds du Ventilateur, par d�faut 0.8
         * @param intensite Intensit� du Ventilateur, par d�fault 3.5
         */
        Ventilateur(Vecteur const& origine = Vecteur(3), Vecteur const& normale = Vecteur(0,0,1), Vecteur const& longueur = Vecteur(1,0,0), Vecteur const& largeur = Vecteur(0,1,0), double const& hauteur = 0.1, double const& profondeur = 1, Couleur const& colorA = Couleur(1,1,1,1), Couleur const& colorB = Couleur(0.6,0.6,0.6,1), Couleur const& colorC = Couleur(0.5,0.5,0.5,1), Couleur const& colorD = Couleur(0.5,0.5,0.5,1), Couleur const& colorE = Couleur(0.3,0.3,0.3,1), Couleur const& colorF = Couleur(0.3,0.3,0.3,1), double const& alpha = 0.8, double const& intensite = 3.5);

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Ventilateur();

        /**
          * @brief Ajout l'Element courant au Systeme pass� en argument
          * @param syst Systeme auquel on ajoute l'Element courant
          */
        virtual void ajoute_a(Systeme* syst);

    protected:

        /**
         * @brief Soufflerie du Ventilateur
         */
        Vent* m_vent;

        /**
         * @brief Base du Ventilateur
         */
        Brique* m_brique;



};

#endif /* VENTILATEUR_H */
