/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file ObjetMobile.h
 * @brief Header de la classe ObjetMobile
 */
#ifndef OBJETMOBILE_H
#define OBJETMOBILE_H

#include <string>
#include <iostream>
#include "Vecteur.h"
#include "Element.h"
#include "Couleur.h"

class Systeme;

/**
 * @class ObjetMobile
 * @brief Classe abstraite repr�sentant un objet mobile
 * @note H�rite de Element
 */
class ObjetMobile : public Element
{
    public:

        /**
         * @brief Construit un ObjetMobile
         * @param omega Valeur d'initialisation du Vecteur ObjetMobile::m_omega, par d�fault le Vecteur nul de dimension 3
         * @param d_omega Valeur d'initialisation du Vecteur ObjetMobile::m_d_omega, par d�fault le Vecteur nul de dimension 3
         * @param rayon Rayon de l'ObjetMobile, par d�fault 1
         * @param masse_volumique Masse volumique de l'ObjetMobile, par d�fault 1
         * @param force Forces exerc�es sur l'ObjetMobile, par d�fault le Vecteur nul de dimension 3
         * @param color Couleur de l'ObjetMobile, par d�fault Couleur(1,1,1,1) (Blanc)
         */
        ObjetMobile(Vecteur const& omega = Vecteur(3), Vecteur const& d_omega = Vecteur(3), double const& rayon = 1, double const& masse_volumique = 1, Vecteur const& force = Vecteur(3), Couleur const& color = Couleur(1,1,1,1));

        /**
         * @brief Construit un ObjetMobile gr�ce � la taille de son Vecteur ObjetMobile::m_omega
         * @param taille Taille des Vecteurs ObjetMobile::m_omega, ObjetMobile::m_d_omega et ObjetMobile::m_force
         */
        ObjetMobile(size_t const& taille);

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~ObjetMobile();

        /**
         * @brief Fonction d'�volution pour l'ObjetMobile
         * @return Retourne le Vecteur d�_omega
         * @note M�thode virtuelle pure
         */
        virtual Vecteur evolution() const = 0;

        /**
         * @brief M�thode pour acc�der � la distance entre l'ObjetMobile courant et l'ObjetMobile pass� en argument
         * @param objet ObjetMobile avec lequel on doit mesurer la distance
         */
        virtual double distance(ObjetMobile const& objet) const;

        /**
         * @brief M�thode pour agir sur un autre objet mobile
         * @param objet ObjetMobile avec lequel on agit
         */
        virtual void agit_sur(ObjetMobile& objet);

        /**
          * @brief Ajout l'ObjetMobile courant au Systeme pass� en argument
          * @param syst Systeme auquel on ajoute l'ObjetMobile courant
          */
        virtual void ajoute_a(Systeme* syst);

        /**
         * @brief Ajoute une force sur l'ObjetMobile
         * @param df Vecteur repr�sentant la force � exercer sur l'ObjetMobile
         */
        virtual void ajoute_force(Vecteur const& df);

        /**
         * @brief Accesseur pour la masse de l'ObjetMobile
         * @return Retourne la masse de l'ObjetMobile
         * @note Calcul�e � partir du rayon et de la masse volumique de l'ObjetMobile
         */
        double masse() const;

        /**
         * @brief Accesseur pour le Vecteur ObjetMobile::m_omega
         * @return Retourne une r�f�rence constante sur le Vecteur ObjetMobile::m_omega
         */
        Vecteur const& omega() const;

        /**
         * @brief Accesseur pour le Vecteur ObjetMobile::m_d_omega
         * @return Retourne une r�f�rence constante sur le Vecteur ObjetMobile::m_d_omega
         */
        Vecteur const& d_omega() const;

        /**
         * @brief Accesseur pour le Vecteur ObjetMobile::m_force
         * @return Retourne une r�f�rence constante sur le Vecteur ObjetMobile::m_force
         */
        Vecteur const& force() const;

        /**
         * @brief Accesseur pour ObjetMobile::m_rayon
         * @return Retourne une r�f�rence constante sur ObjetMobile::m_rayon
         */
        double const& rayon() const;

        /**
         * @brief Accesseur pour ObjetMobile::m_masse_volumique
         * @return Retourne une r�f�rence constante sur ObjetMobile::m_masse_volumique
         */
        double const& masse_volumique() const;

        /**
         * @brief Manipulateur pour le Vecteur ObjetMobile::m_omega
         * @param new_omega Nouvelle valeur pour le Vecteur ObjetMobile::m_omega
         */
        void set_omega(Vecteur const& new_omega);

        /**
         * @brief Manipulateur pour le Vecteur ObjetMobile::m_d_omega
         * @param new_d_omega Nouvelle valeur pour le Vecteur ObjetMobile::m_d_omega
         */
        void set_d_omega(Vecteur const& new_d_omega);

        /**
         * @brief Accesseur pour le Vecteur position de l'ObjetMobile
         * @return Retourne le Vecteur position de l'ObjetMobile
         * @note M�thode virtuelle pure
         */
        virtual Vecteur position() const = 0;

        /**
         * @brief Accesseur pour le Vecteur vitesse de l'ObjetMobile
         * @return Retourne le Vecteur vitesse de l'ObjetMobile
         * @note M�thode virtuelle pure
         */
        virtual Vecteur vitesse() const = 0;

        /**
         * @brief Manipulateur pour la vitesse
         * @param new_vitesse Nouvelle valeur pour la vitesse de l'ObjetMobile
         */
        virtual void set_vitesse(Vecteur const& new_vitesse) = 0;

        /**
         * @brief R�initialise au Vecteur vide de dimension 3 le Vecteur ObjetMobile::m_force
         */
        void reset_force();

    protected:

        /**
         * @brief Couleur de l'ObjetMobile
         */
        Couleur m_couleur;

    private:
        /**
         * @brief Vecteur d'�tat de l'ObjetMobile
         */
        Vecteur m_omega;

        /**
         * @brief D�riv�e temporelle du Vecteur d'�tat de l'ObjetMobile
         */
        Vecteur m_d_omega;

        /**
         * @brief Constante repr�sentant le rayon de l'ObjetMobile
         */
        double const m_rayon;

        /**
         * @brief Constante repr�sentant la masse volumique de l'ObjetMobile
         */
        double const m_masse_volumique;

        /**
         * @brief Vecteur repr�sentant les forces exerc�es dur l'ObjetMobile
         */
        Vecteur m_force;
};

#endif
