/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file AjouteElementDock.h
 * @brief Header de la classe AjouteElementDock
 */
#ifndef AJOUTEELEMENTDOCK_H
#define AJOUTEELEMENTDOCK_H

#include <QWidget>
#include "include/Systeme.h"
#include "gui.h"

/**
 * @class AjouteElementDock
 * @brief Classe g�rant le Dock d'Ajout des Elements
 * @note H�rite de QDockWidget
 */
class AjouteElementDock : public QDockWidget
{
        Q_OBJECT

    public:

        /**
         * @brief Construit un AjouteElementDock
         * @param systeme Pointeur sur le Systeme auquel on doit ajouter des Elements, valeur d'initialisation de AjouteElementDock::m_systeme
         * @param simulation Pointeur sur la GUI qui affiche le Systeme, valeur d'initialisation de AjouteElementDock::m_simulation
         * @param name Nom du QWidget, aucun par d�fault
         * @param parent Pointeur sur le QWidget parent, pointeur nul par d�fault
         */
        AjouteElementDock(Systeme* systeme, GUI* simulation, char* name = 0, QWidget* parent = 0);

    private:

        /**
         * @brief Pointeur sur la QGroupBox de cr�ation de Balle
         */
        QGroupBox* m_CreationBalle;

        /**
         * @brief Pointeur sur la QGroupBox de cr�ation de Pendule
         */
        QGroupBox* m_CreationPendule;

        /**
         * @brief Pointeur sur la QGroupBox de cr�ation de Ressort
         */
        QGroupBox* m_CreationRessort;

        /**
         * @brief Pointeur sur la QGroupBox de cr�ation de Brique
         */
        QGroupBox* m_CreationBrique;

        /**
         * @brief Pointeur sur la QGroupBox de cr�ation de PlanFini
         */
        QGroupBox* m_CreationPlanFini;

        /**
         * @brief Pointeur sur la QGroupBox de cr�ation de Plan
         */
        QGroupBox* m_CreationPlan;

        /**
         * @brief Pointeur sur la QGroupBox de cr�ation de Plan
         */
        QGroupBox* m_CreationVent;

		/**
         * @brief Pointeur sur la QGroupBox de cr�ation de ChampForcesGlobal
         */
        QGroupBox* m_CreationChampForcesGlobal;

		/**
         * @brief Pointeur sur la QGroupBox de cr�ation de Ventilateur
         */
        QGroupBox* m_CreationVentilateur;

		/**
         * @brief Pointeur sur la QGroupBox de cr�ation de Trampoline
         */
        QGroupBox* m_CreationTrampoline;

        /**
         * @brief Pointeur sur le Systeme auquel on ajout des Elements
         */
        Systeme* m_systeme;

        /**
         * @brief Pointeur sur la GUI qui affiche AjouteElementDock::m_systeme
         */
        GUI* m_simulation;

    public slots:

        /**
         * @brief Slot qui g�n�re AjouteElementDock::m_CreationBalle
         */
        void genereCreationBalle();

        /**
         * @brief Slot qui g�n�re AjouteElementDock::m_CreationPendule
         */
        void genereCreationPendule();

        /**
         * @brief Slot qui g�n�re AjouteElementDock::m_CreationRessort
         */
        void genereCreationRessort();

        /**
         * @brief Slot qui g�n�re AjouteElementDock::m_CreationBrique
         */
        void genereCreationBrique();

        /**
         * @brief Slot qui g�n�re AjouteElementDock::m_CreationPlanFini
         */
        void genereCreationPlanFini();

        /**
         * @brief Slot qui g�n�re AjouteElementDock::m_CreationPlan
         */
        void genereCreationPlan();

        /**
         * @brief Slot qui g�n�re AjouteElementDock::m_CreationPlan
         */
        void genereCreationVent();

		/**
         * @brief Slot qui g�n�re AjouteElementDock::m_CreationChampForcesGlobal
         */
        void genereCreationChampForcesGlobal();
        
        /**
         * @brief Slot qui g�n�re AjouteElementDock::m_CreationVentilateur
         */
        void genereCreationVentilateur();
        
        /**
         * @brief Slot qui g�n�re AjouteElementDock::m_CreationTrampoline
         */
        void genereCreationTrampoline();

        /**
         * @brief Slot qui cr�� et ajoute une Balle3D � AjouteElementDock::m_systeme � partir des informations r�cup�r�es dans AjouteElementDock::m_CreationBalle
         * @brief Redessine la sc�ne de AjouteElementDock::m_simulation
         */
        void creeBalle();

        /**
         * @brief Slot qui cr�� et ajoute un Pendule3D � AjouteElementDock::m_systeme � partir des informations r�cup�r�es dans AjouteElementDock::m_CreationPendule
         * @brief Redessine la sc�ne de AjouteElementDock::m_simulation
         */
        void creePendule();

        /**
         * @brief Slot qui cr�� et ajoute un Ressort3D � AjouteElementDock::m_systeme � partir des informations r�cup�r�es dans AjouteElementDock::m_CreationRessort
         * @brief Redessine la sc�ne de AjouteElementDock::m_simulation
         */
        void creeRessort();

        /**
         * @brief Slot qui cr�� et ajoute une Brique3D � AjouteElementDock::m_systeme � partir des informations r�cup�r�es dans AjouteElementDock::m_CreationBrique
         * @brief Redessine la sc�ne de AjouteElementDock::m_simulation
         */
        void creeBrique();

        /**
         * @brief Slot qui cr�� et ajoute un PlanFini3D � AjouteElementDock::m_systeme � partir des informations r�cup�r�es dans AjouteElementDock::m_CreationPlanFini
         * @brief Redessine la sc�ne de AjouteElementDock::m_simulation
         */
        void creePlanFini();

        /**
         * @brief Slot qui cr�� et ajoute un Plan3D � AjouteElementDock::m_systeme � partir des informations r�cup�r�es dans AjouteElementDock::m_CreationPlan
         * @brief Redessine la sc�ne de AjouteElementDock::m_simulation
         */
        void creePlan();

		/**
         * @brief Slot qui cr�� et ajoute un Vent � AjouteElementDock::m_systeme � partir des informations r�cup�r�es dans AjouteElementDock::m_CreationVent
         * @brief Redessine la sc�ne de AjouteElementDock::m_simulation
         */
        void creeVent();

        /**
         * @brief Slot qui cr�� et ajoute un ChampForcesGlobal � AjouteElementDock::m_systeme � partir des informations r�cup�r�es dans AjouteElementDock::m_CreationChampForcesGlobal
         * @brief Redessine la sc�ne de AjouteElementDock::m_simulation
         */
        void creeChampForcesGlobal();

        /**
         * @brief Slot qui cr�� et ajoute un Ventilateur � AjouteElementDock::m_systeme � partir des informations r�cup�r�es dans AjouteElementDock::m_CreationVentilateur
         * @brief Redessine la sc�ne de AjouteElementDock::m_simulation
         */
        void creeVentilateur();

        /**
         * @brief Slot qui cr�� et ajoute un Trampoline � AjouteElementDock::m_systeme � partir des informations r�cup�r�es dans AjouteElementDock::m_CreationTrampoline
         * @brief Redessine la sc�ne de AjouteElementDock::m_simulation
         */
        void creeTrampoline();
};

#endif // AJOUTEELEMENTDOCK_H
