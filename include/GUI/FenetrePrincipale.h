/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file FenetrePrincipale.h
 * @brief Header de la classe FenetrePrincipale
 */
#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QModelIndex>
#include <QStandardItemModel>
#include <QWidget>
#include "include/Systeme.h"
#include "gui.h"
#include "include/ObjetMobile.h"
#include "include/Obstacle.h"
#include "include/ChampForces.h"

/**
 * @class FenetrePrincipale
 * @brief Classe g�rant la fen�tre principale de l'application
 * @note H�rite de QMainWindow
 */
class FenetrePrincipale : public QMainWindow
{
        Q_OBJECT

    public:

        /**
         * @brief Construit la fen�tre principale
         * @param systeme Pointeur sur le Systeme � simuler, pointeur nul par d�fault
         * @param nom Nom de la fen�tre, par d�fault le nom du projet
         * @param parent Pointeur sur le QWidget parent, pointeur nul par d�fault
         */
        explicit FenetrePrincipale(Systeme* systeme = 0, char* nom = 0, QWidget *parent = 0);

        /**
         * @brief D�truit FenetrePrincipale::m_model
         */
        ~FenetrePrincipale();

		/**
		 * @brief Vide les mod�les enfants de FenetrePrincipale::m_model
		 */
        void clearModel();

    private:

        /**
         * @brief G�n�re FenetrePrincipale::m_model
         * @param systeme Pointeur sur le Systeme dont on doit r�cup�rer les informations
         */
        void genereModel(Systeme* systeme);

        /**
         * @brief G�n�re Fenetre::m_simulation
         * @param systeme Pointeur sur le Systeme qui doit �tre affich�
         */
        void genereSimulation(Systeme* systeme);

        /**
         * @brief Pointeur sur la GUI qui g�re l'affichage du Systeme
         */
        GUI* m_simulation;

        /**
         * @brief Pointeur sur un mod�le repr�sentant les diff�rents �l�ments du Systeme
         */
        QStandardItemModel* m_model;

        /**
         * @brief Pointeur sur la QGroupBox qui contient l'affichage des propri�t�s de l'Element s�l�ctionn�
         */
        QGroupBox* m_proprietes;

    public slots:

        /**
         * @brief Slot qui ajoute un ObjetMobile � FenetrePrincipale::m_model
         * @param objet Pointeur sur l'ObjetMobile � ajouter � FenetrePrincipale::m_model
         */
        void ajouteObjet(ObjetMobile* objet);

        /**
         * @brief Slot qui ajoute un Obstacle � FenetrePrincipale::m_model
         * @param obstacle Pointeur sur l'Obstacle � ajouter � FenetrePrincipale::m_model
         */
        void ajouteObstacle(Obstacle* obstacle);

        /**
         * @brief Slot qui ajoute un ChampForces � FenetrePrincipale::m_model
         * @param champ Pointeur sur le ChampForces � ajouter � FenetrePrincipale::m_model
         */
        void ajouteChamp(ChampForces* champ);

        /**
         * @brief Slot qui actualise FenetrePrincipale::proprietes
         * @param index QModelIndex de l'Element s�l�ctionn� dans FenetrePrincipale::m_model
         */
        void generePropriete(QModelIndex const& index = QModelIndex());

        /**
         * @brief Supprime l'Element s�lectionn�
         */
        void supprimeElement();

		/**
		 * @brief Enregistre le Systeme courant dans un fichier XML
		 */
        void enregistrer();

		/**
		 * @brief Cr�� un nouveau Systeme � partir d'un fichier XML
		 */
        void ouvrir();

		/**
		 * @brief Cr�� un Systeme vide
		 */
        void nouveau();


};

#endif // FENETREPRINCIPALE_H
