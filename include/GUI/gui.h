/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file gui.h
 * @brief Header de la classe GUI
 */
#ifndef GUI_H
#define GUI_H

#include <QtOpenGL>
#include <QWidget>
#include <QTimer>
#include "include/Systeme.h"

/**
 * @class GUI
 * @brief Classe g�rant l'affichage graphique
 * @note H�rite de QGLWidget
 */
class GUI : public QGLWidget
{
        Q_OBJECT

    public:

        /**
         * @brief Construit un objet de la classe GUI
         * @param systeme Pointeur sur le syst�me � simuler
         * @param imagesParSecondes Nombre d'images (et donc d'int�grations) par secondes, par d�fault 50
         * @param parent Pointeur sur le QWidget parent, par d�fault aucun
         * @param nom Nom de la fen�tre
         */
        explicit GUI(Systeme* systeme, int imagesParSecondes = 50, QWidget* parent = 0, char* nom = 0);

        /**
         * @brief Accesseur pour GUI::m_systeme
         * @return Retourne GUI::m_systeme
         */
        Systeme* systeme();

        /**
         * @brief Accesseur pour GUI::m_Timer
         * @return Retourne GUI::m_Timer
         */
        QTimer* timer() const;

    protected:

        /**
         * @brief Initialise la sc�ne OpenGL
         */
        virtual void initializeGL();

        /**
         * @brief Met � jour l'affichage lors du redimensionnement du Widget
         */
        virtual void resizeGL(int width, int height);

        /**
         * @brief Met � jour la sc�ne OpenGL
         */
        virtual void paintGL();

        /**
         * @brief G�re les �v�nements claviers
         * @param keyEvent Ev�nement clavier
         */
        virtual void keyPressEvent(QKeyEvent* keyEvent);

        /**
         * @brief G�re les �v�nements souris
         * @param mouseEvent Ev�nement souris
         */
        virtual void mousePressEvent (QMouseEvent* mouseEvent);

       /**
		* @brief G�re les �v�nements molette
        * @param wheel_event Ev�nement molette
        */
        virtual void wheelEvent (QWheelEvent* wheel_event);
        
        /**
         * @brief G�re les d�placements de la souris
         * @param move_event Ev�nement souris
         */
        virtual void mouseMoveEvent (QMouseEvent* move_event);

		/**
         * @brief G�re les clics de la souris
         * @param mouseEvent Ev�nement souris
         */
        virtual void mouseDoubleClickEvent(QMouseEvent * mouseEvent);


    private:

        /**
         * @brief Timer d�finissant le temps entre deux avanc�e du syst�me
         */
        QTimer* m_Timer;

        /**
         * @brief Pointeur sur le syst�me � simuler
         */
        Systeme* m_systeme;

        /**
         * @brief Variable pour la gestion de la cam�ra, distance au point vis�
         */
        double h;

        /**
         * @brief Variable pour la gestion de la cam�ra, angle de rotation autour du point vis�
         */
        double theta;

        /**
         * @brief Variable pour la gestion de la cam�ra, d�placement en hauteur
         */
        double z;

        /**
         * @brief Variable pour la gestion de la cam�ra, d�placement selon l'axe y
         */
        double y;

        QPoint lastPos;

    public slots:

        /**
         * @brief Slot recevant les signaux du timer
         * @brief Fait �voluer le syst�me et redessine la sc�ne � chaque signal re�u
         */
        virtual void timeOutSlot();

        /**
         * @brief Suspends la simulation
         * @note Emet GUI::simulationStopped(true)
         */
        void stopSimulation();

        /**
         * @brief D�marre la simulation
         * @note Emet GUI::simulationStarted(true)
         */
        void startSimulation();

        /**
         * @brief Manipulateur pour GUI::m_Timer
         * @param nmbrImgSec Nouveau nombre d'images par secondes de GUI::m_timer
         */
        void setImagesParSecondes(int nmbrImgSec);


    signals:

        /**
         * @brief Signal �mit lors du d�marrage de la simulation
         */
        void simulationStarted(bool = true);

        /**
         * @brief Signal �mit lors de l'arr�t de la simulation
         */
        void simulationStopped(bool = true);

};

#endif // GUI_H
