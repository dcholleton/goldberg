/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Dessinable.h
 * @brief Header de la classe Dessinable
 */
#ifndef DESSINABLE_H
#define DESSINABLE_H

#include <iostream>

/**
 * @class Dessinable
 * @brief Classe repr�sentant un Dessinable quelconque
 */
class Dessinable
{
    public:

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Dessinable();

        /**
         * @brief Affiche le Dessinable sur cout
         */
        virtual void dessine() const;

        /**
         * @brief Surcharge externe de l'op�rateur "<<"
         * @brief Affiche le Dessinable
         * @param out Flux de sortie
         * @param a_afficher Dessinable � afficher
         * @return Retourne le flux de sortie
         * @note Fonction amie de la classe Dessinable
         * @note D�fini par rapport � Dessinable::affiche()
         */
        friend std::ostream& operator<<(std::ostream& out, Dessinable const& a_afficher);

    protected:

        /**
         * @brief Affiche le Dessinable courant dans le flux pass� en argument
         * @param out Flux de sortie
         * @note M�thode virtuelle pure
         */
        virtual void affiche(std::ostream& out) const = 0;


};

#endif // DESSINABLE_H
