/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Plan3d.h
 * @brief Header de la classe Plan3D
 */
#ifndef PLAN3D_H
#define PLAN3D_H

#include <string>
#include "Plan.h"
#include "Vecteur.h"
#include "Couleur.h"


/**
 * @class Plan3D
 * @brief Classe repr�sentant un Plan pour l'affichage graphique
 * @note H�rite de Plan
 */
class Plan3D : public Plan
{
    public:
        /**
         * @brief Construit un Plan3D
         * @param origine Origine du Plan3D, par d�fault le Vecteur nul de dimension 3
         * @param normale Normale au Plan3D, par d�fault le Vecteur unitaire sur l'axe z {0,0,1}
         * @param color Couleur du Plan, par d�fault Couleur(1,1,1,1) (Blanc)
         * @param alpha Coefficient de rebonds du Plan3D, par d�fault 0.8
         */
        Plan3D(Vecteur const& origine = Vecteur(3), Vecteur const& normale = Vecteur(0,0,1), Couleur const& color = Couleur(1,1,1,1), double const& alpha = 0.8);

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Plan3D();

        /**
         * @brief Dessine le Plan3D courant
         */
        virtual void dessine() const;


        /**
         * @brief Sert � obtenir le type Plan3D sous forme de cha�ne de caract�res
         * @return Retourne std::string("Plan3D")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;

		/**
         * @brief Sert � envoyer l'�tat du Plan3D dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;

};

#endif // PLAN3D_H
