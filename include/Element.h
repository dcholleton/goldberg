/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Element.h
 * @brief Header de la classe Element
 */
#ifndef ELEMENT_H
#define ELEMENT_H

#include <string>
#include <QFormLayout>
#include "Dessinable.h"



class ObjetMobile;

class Systeme;

/**
 * @class Element
 * @brief Classe abstraite repr�sentant un Element quelconque du syst�me
 * @note H�rite de Dessinable
 */
class Element : public Dessinable
{
    public:

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Element();

        /**
         * @brief Agit sur l'ObjetMobile pass� en argument
         * @param objet ObjetMobile sur lequel on agit
         * @note M�thode virtuelle pure
         */
        virtual void agit_sur(ObjetMobile& objet) = 0 ;

        /**
         * @brief Donne la distance entre l'Element courant et l'ObjetMobile pass� en argument
         * @param objet ObjetMobile avec lequel on mesure la distance
         * @note M�thode virtuelle pure
         */
        virtual double distance(ObjetMobile const& objet) const = 0 ;

        /**
          * @brief Ajout l'Element courant au Systeme pass� en argument
          * @param syst Systeme auquel on ajoute l'Element courant
          * @note M�thode virtuelle pure
          */
        virtual void ajoute_a(Systeme* syst) = 0;

        /**
         * @brief Sert � obtenir le type de l'Element sous forme de cha�ne de caract�res
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         * @note M�thode virtuelle pure
         */
        virtual std::string type() const = 0;

        /**
         * @brief Sert � cr�er un Layout contenant les propri�t�s de l'Element en vue de l'interface graphique
         * @return Retourne un pointeur sur un QFormLayout dont les champs contiennent les propri�t�s de l'Element
         * @note M�thode virtuelle pure
         */
        virtual QFormLayout* proprietesLayout() const = 0;

		/**
         * @brief Sert � envoyer l'�tat de l'Element dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         * @note M�thode virtuelle pure
         */
        virtual void enregistrer(std::ofstream& out) const = 0 ;

};

#endif /* ELEMENT_H */
