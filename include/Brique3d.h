/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Brique3d.h
 * @brief Header de la classe Brique3D
 */
#ifndef BRIQUE3D_H
#define BRIQUE3D_H

#include <string>
#include "Brique.h"
#include "Vecteur.h"
#include "Couleur.h"



/**
 * @class Brique3D
 * @brief Classe repr�sentant une Brique pour l'affichage graphique
 * @note H�rite de Brique
 */
class Brique3D : public Brique
{
    public:
        /**
         * @brief Construit une Brique3D
         * @param origine Origine de la Brique3D, par d�fault le Vecteur nul de dimension 3
         * @param longueur Longueur de la Brique3D, par d�fault le Vecteur unitaire sur l'axe x {1,0,0}
         * @param largeur Largeur de la Brique3D, par d�fault le Vecteur unitaire sur l'axe y {0,1,0}
         * @param hauteur Hauteur de la Brique3D, par d�fault 1
         * @param colorA Couleur de la face inf�rieure de la Brique3D, par d�fault Couleur(0,1,1,1)
         * @param colorB Couleur de la face sup�rieure de la Brique3D, par d�fault Couleur(1,0,1,1)
         * @param colorC Couleur de la face gauche de la Brique3D, par d�fault Couleur(1,1,0,1)
         * @param colorD Couleur de la face droite de la Brique3D, par d�fault Couleur(0.5,1,1,1)
         * @param colorE Couleur de la face arri�re de la Brique3D, par d�fault Couleur(1,0.5,1,1)
         * @param colorF Couleur de la face avant de la Brique3D, par d�fault Couleur(1,1,0.5,1)
         * @param alpha Coefficient de rebond de la Brique3D
         */
        Brique3D(Vecteur const& origine = Vecteur(3), Vecteur const& longueur = Vecteur(1,0,0), Vecteur const& largeur = Vecteur(0,1,0), double const& hauteur = 1, Couleur const& colorA = Couleur(0,1,1,1), Couleur const& colorB = Couleur(1,0,1,1), Couleur const& colorC = Couleur(1,1,0,1), Couleur const& colorD = Couleur(0.5,1,1,1), Couleur const& colorE = Couleur(1,0.5,1,1), Couleur const& colorF = Couleur(1,1,0.5,1), double const& alpha = 0.8);

		/**
		 * @brief Constructeur de copie de Brique3D
		 * @param brique Brique copi�e
		 */
        Brique3D(Brique const& brique);

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Brique3D();

        /**
         * @brief Dessine la Balle3D courante
         */
        virtual void dessine() const;

        /**
         * @brief Sert � obtenir le type Brique3D sous forme de cha�ne de caract�res
         * @return Retourne std::string("Brique3D")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;
        
        /**
         * @brief Sert � envoyer l'�tat de la Brique dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;


};

#endif // BRIQUE3D_H
