/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Pendule3d.h
 * @brief Header de la classe Pendule3D
 */
#ifndef PENDULE3D_H
#define PENDULE3D_H

#include <GL/glu.h>
#include <string>
#include "Pendule.h"
#include "Vecteur.h"
#include "Couleur.h"

/**
 * @class Pendule3D
 * @brief Classe repr�sentant un Pendule pour l'affichage graphique
 * @note H�rite de Pendule
 */
class Pendule3D : public Pendule
{
    public:
        /**
         * @brief Constuit un Pendule3D
         * @param omega Angle du Pendule3D, par d�fault le Vecteur nul de dimension 1
         * @param d_omega Vitesse Angulaire du Pendule3D, par d�fault le Vecteur nul de dimension 1
         * @param rayon Rayon de la Boule suspendue au Pendule3D, par d�fault 1
         * @param masse_volumique Masse volumique de la Boule suspendue au Pendule3D, par d�fault 1
         * @param force Forces exerc�es sur la Boule suspendue au Pendule3D, par d�fault le Vecteur nul de dimension 3
         * @param longueur Longueur du Pendule3D, par d�fault 10
         * @param frottement Coefficient de frottements du Pendule3D, par d�fault 1
         * @param direction Direction du Pendule3D, par d�faut le Vecteur unitaire sur l'axe x {1,0,0}
         * @param attache Point d'attache du Pendule3D, par d�fault le Vecteur nul de dimension 3
         * @param color Couleur du Pendule3D, par d�fault Couleur(1,0,0,1) (Rouge)
         */
        Pendule3D(Vecteur const& omega = Vecteur(1), Vecteur const& d_omega = Vecteur(1), double const& rayon = 0.1, double const& masse_volumique = 1, Vecteur const& force = Vecteur(3), double const& longueur = 10 , double const& frottement = 1, Vecteur const& direction = Vecteur(1,0,0), Vecteur const& attache = Vecteur(3), Couleur const& color  = Couleur(1,0,0,1));

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Pendule3D();

        /**
         * @brief Dessine le Pendule3D courant
         */
        virtual void dessine() const;

        /**
         * @brief Sert � obtenir le type Pendule3D sous forme de cha�ne de caract�res
         * @return Retourne std::string("Pendule3D")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;

		/**
         * @brief Sert � envoyer l'�tat du Pendule3D dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;

    private:

        /**
         * @brief GLUquadric* pour l'affichage de la sph�re
         */
        GLUquadric* m_sphere;
};

#endif // PENDULE3D_H
