/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Couleur.h
 * @brief Header de la classe Couleur
 */
#ifndef COULEUR_H
#define COULEUR_H

/**
 * @class Couleur
 * @brief Classe mod�lisant une couleur selon le mod�le RGBA
 */
class Couleur
{
    public:

        /**
         * @brief Construit une Couleur
         * @param rouge Valeur d'initialisation de Couleur::m_rouge, par d�fault 1.0
         * @param vert Valeur d'initialisation de Couleur::m_vert, par d�fault 1.0
         * @param bleu Valeur d'initialisation de Couleur::m_bleu, par d�fault 1.0
         * @param alpha Valeur d'initialisation de Couleur::m_alpha, par d�fault 1.0
         */
        Couleur(double const& rouge = 1, double const & vert = 1, double const & bleu = 1, double const & alpha = 1);

        /**
         * @brief Accesseur pour Couleur::m_rouge
         * @return Retourne une r�f�rence constante sur Couleur::m_rouge
         */
        double const & rouge() const;

        /**
         * @brief Accesseur pour Couleur::m_vert
         * @return Retourne une r�f�rence constante sur Couleur::m_vert
         */
        double const & vert() const;

        /**
         * @brief Accesseur pour Couleur::m_bleu
         * @return Retourne une r�f�rence constante sur Couleur::m_bleu
         */
        double const & bleu() const;

        /**
         * @brief Accesseur pour Couleur::m_alpha
         * @return Retourne une r�f�rence constante sur Couleur::m_alpha
         */
        double const & alpha() const;

        /**
         * @brief Manipulateur pour Couleur
         * @param rouge Nouvelle valeur pour Couleur::m_rouge
         * @param vert Nouvelle valeur pour Couleur::m_vert
         * @param bleu Nouvelle valeur pour Couleur::m_bleu
         * @param alpha Nouvelle valeur pour Couleur::m_alpha
         */
        void setCouleur(double const & rouge, double const & vert, double const & bleu, double const & alpha =1);

    private:

        /**
         * @brief Pourcentage de rouge
         */
        double m_rouge;

        /**
         * @brief Pourcentage de vert
         */
        double m_vert;

        /**
         * @brief Pourcentage de bleu
         */
        double m_bleu;

        /**
         * @brief Pourcentage d'opacit�
         */
        double m_alpha;
};

#endif // COULEUR_H
