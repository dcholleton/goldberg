/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file ObjetCompose.h
 * @brief Header de la classe ObjetCompose
 */
#ifndef OBJETCOMPOSE_H
#define OBJETCOMPOSE_H

class Systeme;

/**
 * @class ObjetCompose
 * @brief Classe abstraite repr�sentant un objet compos� de plusieurs Elements
 */
class ObjetCompose
{
    public:


        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~ObjetCompose();

        /**
          * @brief Ajoute l'ObjetCompose courant au Systeme pass� en argument
          * @param syst Systeme auquel on ajoute l'ObjetCompose courant
          * @note M�thode virtuelle pure
          */
        virtual void ajoute_a(Systeme* syst) = 0;
};

#endif // OBJETCOMPOSE_H
