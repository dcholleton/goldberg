/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Plan.h
 * @brief Header de la classe Plan
 */
#ifndef PLAN_H
#define PLAN_H

#include <string>
#include <iostream>
#include <QFormLayout>
#include "Obstacle.h"
#include "ObjetMobile.h"
#include "Vecteur.h"
#include "Couleur.h"

/**
 * @class Plan
 * @brief Classe repr�sentant un Plan infini
 * @note H�rite de Obstacle
 */
class Plan: public Obstacle
{
    public:
        /**
         * @brief Construit un Plan
         * @param origine Origine du Plan, par d�fault le Vecteur nul de dimension 3
         * @param normale Normale au Plan, par d�fault le Vecteur unitaire sur l'axe z {0,0,1}
         * @param color Couleur du Plan, par d�fault Couleur(1,1,1,1) (Blanc)
         * @param alpha Coefficient de rebonds du Plan, par d�fault 0.8
         */
        Plan(Vecteur const& origine = Vecteur(3), Vecteur const& normale = Vecteur(0,0,1), Couleur const& color = Couleur(1,1,1,1), double const& alpha = 0.8);

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Plan();

        /**
         * @brief Sert � trouver le point du Plan le plus proche de l'ObjetMobile pass� en argument
         * @param objet ObjetMobile dont on doit d�terminer le point le plus proche
         * @return Retourne le Vecteur des coordonn�es du point du Plan le plus proche de l'ObjetMobile
         */
        virtual Vecteur pointPlusProche(ObjetMobile const& objet) const;

        /**
         * @brief Sert � obtenir le type Plan sous forme de cha�ne de caract�res
         * @return Retourne std::string("Plan")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;

        /**
         * @brief Sert � cr�er un Layout contenant les propri�t�s du Plan en vue de l'interface graphique
         * @return Retourne un pointeur sur un QFormLayout dont les champs contiennent les propri�t�s du Plan
         */
        virtual QFormLayout* proprietesLayout() const;

		/**
         * @brief Sert � envoyer l'�tat du Plan dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;


    protected:

        /**
         * @brief Affiche le Plan courant dans le flux pass� en argument
         * @param out Flux de sortie
         */
        virtual void affiche(std::ostream& out) const;

        /**
         * @brief Couleur du Plan
         */
        Couleur m_couleur;

};

#endif /* PLAN_H */
