/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Brique.h
 * @brief Header de la classe Brique
 */
#ifndef BRIQUE_H
#define BRIQUE_H

#include <string>
#include <iostream>
#include <QFormLayout>
#include "Obstacle.h"
#include "Vecteur.h"
#include "Couleur.h"
#include "ObjetMobile.h"


/**
 * @class Brique
 * @brief Classe repr�sentant une Brique
 * @note H�rite de Obstacle
 */
class Brique: public Obstacle
{
    public:
        /**
         * @brief Construit une Brique
         * @param origine Origine de la Brique, par d�fault le Vecteur nul de dimension 3
         * @param longueur Longueur de la Brique, par d�fault le Vecteur unitaire sur l'axe x {1,0,0}
         * @param largeur Largeur de la Brique, par d�fault le Vecteur unitaire sur l'axe y {0,1,0}
         * @param hauteur Hauteur de la Brique, par d�fault 1
         * @param colorA Couleur de la face inf�rieure de la Brique, par d�fault Couleur(0,1,1,1)
         * @param colorB Couleur de la face sup�rieure de la Brique, par d�fault Couleur(1,0,1,1)
         * @param colorC Couleur de la face gauche de la Brique, par d�fault Couleur(1,1,0,1)
         * @param colorD Couleur de la face droite de la Brique, par d�fault Couleur(0.5,1,1,1)
         * @param colorE Couleur de la face arri�re de la Brique, par d�fault Couleur(1,0.5,1,1)
         * @param colorF Couleur de la face avant de la Brique, par d�fault Couleur(1,1,0.5,1)
         * @param alpha Coefficient de rebond de la Brique
         */
        Brique(Vecteur const& origine = Vecteur(3), Vecteur const& longueur = Vecteur(1,0,0), Vecteur const& largeur = Vecteur(0,1,0), double const& hauteur = 1, Couleur const& colorA = Couleur(0,1,1,1), Couleur const& colorB = Couleur(1,0,1,1), Couleur const& colorC = Couleur(1,1,0,1), Couleur const& colorD = Couleur(0.5,1,1,1), Couleur const& colorE = Couleur(1,0.5,1,1), Couleur const& colorF = Couleur(1,1,0.5,1), double const& alpha = 0.8);

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Brique();

        /**
         * @brief Sert � trouver le point de la Brique le plus proche de l'ObjetMobile pass� en argument
         * @param objet ObjetMobile dont on doit d�terminer le point le plus proche
         * @return Retourne le Vecteur des coordonn�es du point de la Brique le plus proche de l'ObjetMobile
         */
        virtual Vecteur pointPlusProche(ObjetMobile const& objet) const;

        /**
         * @brief Accesseur pour Brique::m_longueur
         * @return Retourne une r�f�rence constante sur Brique::m_longueur
         */
        Vecteur const& longueur() const;

        /**
         * @brief Accesseur pour Brique::m_largeur
         * @return Retourne une r�f�rence constante sur Brique::m_largeur
         */
        Vecteur const& largeur() const;

        /**
         * @brief Accesseur pour Brique::m_hauteur
         * @return Retourne une r�f�rence constante sur Brique::m_hauteur
         */
        double const& hauteur() const;

        /**
         * @brief Sert � obtenir le type Brique sous forme de cha�ne de caract�res
         * @return Retourne std::string("Brique")
         * @note Utilis�e pour la liste des Elements dans l'interface graphique
         */
        virtual std::string type() const;

        /**
         * @brief Sert � cr�er un Layout contenant les propri�t�s de la Brique en vue de l'interface graphique
         * @return Retourne un pointeur sur un QFormLayout dont les champs contiennent les propri�t�s de la Brique
         */
        virtual QFormLayout* proprietesLayout() const;


        /**
         * @brief Sert � envoyer l'�tat de la Brique dans le flux pass� en argument, dans un style XML
         * @param out Flux de sortie
         */
        virtual void enregistrer(std::ofstream& out) const;

    protected:

        /**
         * @brief Affiche la Brique courante dans le flux pass� en argument
         * @param out Flux de sortie
         */
        virtual void affiche(std::ostream& out) const;

        /**
         * @brief Couleur de la face inf�rieure de la Brique
         */
        Couleur m_couleurA;

        /**
         * @brief Couleur de la face sup�rieure de la Brique
         */
        Couleur m_couleurB;

        /**
         * @brief Couleur de la face gauche de la Brique
         */
        Couleur m_couleurC;

        /**
         * @brief Couleur de la face droite de la Brique
         */
        Couleur m_couleurD;

        /**
         * @brief Couleur de la face arri�re de la Brique
         */
        Couleur m_couleurE;

        /**
         * @brief Couleur de la face avant de la Brique
         */
        Couleur m_couleurF;

    private:

        /**
         * @brief Longueur de la Brique
         */
        Vecteur const m_longueur;

        /**
         * @brief Largeur de la Brique
         */
        Vecteur const m_largeur;

        /**
         * @brief Hauteur de la Brique
         */
        double const m_hauteur;

};

#endif /* BRIQUE_H */
