/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Integrateur.h
 * @brief Header de la classe Integrateur
 */
#ifndef INTEGRATEUR_H
#define INTEGRATEUR_H

#include <string>
#include "ObjetMobile.h"

/**
 * @brief Namespace contenant les diff�rents types d'Integrateur
 */
namespace TypeIntegrateur
{
/**
 * @brief Liste �num�r�e r�pertoriant les diff�rents types d'Integrateur
 */
enum Repertoire_Integrateur { Euler, RungeKutta, Newmark } ;
}

/**
 * @class Integrateur
 * @brief Classe repr�sentant un Int�grateur
 */
class Integrateur
{
    public:
        /**
         * @brief Construit un Integrateur
         * @param dt Pas de temps, par d�fault 0,01
         */
        Integrateur(double const& dt = 0.01);

        /**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Integrateur();

        /**
         * @brief Int�gre l'ObjetMobile pass� en argument, en utilisant sa fonction �volution
         * @param objet ObjetMobile � int�grer
         */
        virtual void integre(ObjetMobile& objet) const = 0;

        /**
         * @brief Accesseur pour le pas de temps
         * @return Retourne une r�f�rence constante sur le Integrateur::m_dt
         */
        double dt() const;

        /**
         * @brief Manipulateur pour le pas de temps
         * @param new_dt Nouvelle valeur pour Integrateur::m_dt
         */
        void set_dt(double const& new_dt);

    private:

        /**
         * @brief Pas de temps pour l'int�gration
         */
        double m_dt;

};

#endif /* INTEGRATEUR_H */
