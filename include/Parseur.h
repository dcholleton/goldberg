/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Parseur.h
 * @brief Header de la classe Parseur
 */
#ifndef PARSEUR_H
#define PARSEUR_H

#include <string>
#include <fstream>
#include <iostream>
#include "Vecteur.h"
#include "Systeme.h"
#include "Balle.h"
#include "Balle3d.h"
#include "Pendule.h"
#include "Pendule3d.h"
#include "Ressort.h"
#include "Ressort3d.h"
#include "Plan.h"
#include "Plan3d.h"
#include "PlanFini.h"
#include "PlanFini3d.h"
#include "Brique.h"
#include "Brique3d.h"
#include "Vent.h"
#include "ChampForcesGlobal.h"
#include "Ventilateur.h"
#include "Ventilateur3d.h"
#include "Trampoline.h"
#include "Trampoline3d.h"

/**
 * @class Parseur
 * @brief Classe repr�sentant un Parseur XML
 */
class Parseur
{
    public:
    
        Parseur(std::string const& adresse, Systeme* syst);
        ~Parseur();

		/**
		 * @brief Balise Systeme
		 * @note Attribut statique
		 */
        static const std::string SYSTEME;
        
        /**
		 * @brief Balise ObjetMobile
		 * @note Attribut statique
		 */
        static const std::string OBJET_MOBILE;
        
        /**
		 * @brief Balise Obstacle
		 * @note Attribut statique
		 */
        static const std::string OBSTACLE;
        
        /**
		 * @brief Balise ObjetCompose
		 * @note Attribut statique
		 */
        static const std::string OBJET_COMPOSE;
        
        /**
		 * @brief Balise ChampForces
		 * @note Attribut statique
		 */
        static const std::string CHAMP_FORCE;

        /**
		 * @brief Balise Position
		 * @note Attribut statique
		 */
        static const std::string POSITION;
        
        /**
		 * @brief Balise Vitesse
		 * @note Attribut statique
		 */
        static const std::string VITESSE;
        
        /**
		 * @brief Balise Force
		 * @note Attribut statique
		 */
        static const std::string FORCE;
        
        /**
		 * @brief Balise Rayon
		 * @note Attribut statique
		 */
        static const std::string RAYON;
        
        /**
		 * @brief Balise Masse_Volumique
		 * @note Attribut statique
		 */
        static const std::string MASSE_VOLUMIQUE;
        
        /**
		 * @brief Balise Couleur
		 * @note Attribut statique
		 */
        static const std::string COULEUR;
        
        /**
		 * @brief Balise Angle
		 * @note Attribut statique
		 */
        static const std::string ANGLE;
        
        /**
		 * @brief Balise Vitese_Angulaire
		 * @note Attribut statique
		 */
        static const std::string VITESSE_ANGULAIRE;
        
        /**
		 * @brief Balise Direction
		 * @note Attribut statique
		 */
        static const std::string DIRECTION;
        
        /**
		 * @brief Balise Attache
		 * @note Attribut statique
		 */
        static const std::string ATTACHE;
        
        /**
		 * @brief Balise Longueur
		 * @note Attribut statique
		 */
        static const std::string LONGUEUR;
        
        /**
		 * @brief Balise Frottement
		 * @note Attribut statique
		 */
        static const std::string FROTTEMENT;
        
        /**
		 * @brief Balise Raideur
		 * @note Attribut statique
		 */
        static const std::string RAIDEUR;
        
        /**
		 * @brief Balise Origine
		 * @note Attribut statique
		 */
        static const std::string ORIGINE;
        
        /**
		 * @brief Balise Normale
		 * @note Attribut statique
		 */
        static const std::string NORMALE;
        
        /**
		 * @brief Balise Coefficient_Rebond
		 * @note Attribut statique
		 */
        static const std::string COEFFICIENT_REBOND;
        
        /**
		 * @brief Balise Largeur
		 * @note Attribut statique
		 */
        static const std::string LARGEUR;
        
        /**
		 * @brief Balise Hauteur
		 * @note Attribut statique
		 */
        static const std::string HAUTEUR;
        
        /**
		 * @brief Balise Face_A
		 * @note Attribut statique
		 */
        static const std::string FACE_A;
        
        /**
		 * @brief Balise Face_B
		 * @note Attribut statique
		 */
        static const std::string FACE_B;
        
        /**
		 * @brief Balise Face_C
		 * @note Attribut statique
		 */
        static const std::string FACE_C;
        
        /**
		 * @brief Balise Face_D
		 * @note Attribut statique
		 */
        static const std::string FACE_D;
        
        /**
		 * @brief Balise Face_E
		 * @note Attribut statique
		 */
        static const std::string FACE_E;
        
        /**
		 * @brief Balise Face_F
		 * @note Attribut statique
		 */
        static const std::string FACE_F;
        
        /**
		 * @brief Balise Profondeur
		 * @note Attribut statique
		 */
        static const std::string PROFONDEUR;
        
        /**
		 * @brief Balise Intensite
		 * @note Attribut statique
		 */
        static const std::string INTENSITE;

		/**
		 * @brief Balise Balle
		 * @note Attribut statique
		 */
        static const std::string BALLE;
      
        /**
		 * @brief Balise Balle3D
		 * @note Attribut statique
		 */
        static const std::string BALLE3D;
      
        /**
		 * @brief Balise Ressort
		 * @note Attribut statique
		 */
        static const std::string RESSORT;
      
        /**
		 * @brief Balise Ressort3D
		 * @note Attribut statique
		 */
        static const std::string RESSORT3D;
      
        /**
		 * @brief Balise Pendule
		 * @note Attribut statique
		 */
        static const std::string PENDULE;
      
        /**
		 * @brief Balise Pendule3D
		 * @note Attribut statique
		 */
        static const std::string PENDULE3D;
      
        /**
		 * @brief Balise Plan
		 * @note Attribut statique
		 */
        static const std::string PLAN;
      
        /**
		 * @brief Balise Plan3D
		 * @note Attribut statique
		 */
        static const std::string PLAN3D;
      
        /**
		 * @brief Balise Plan_Fini
		 * @note Attribut statique
		 */
        static const std::string PLAN_FINI;
      
        /**
		 * @brief Balise Plan_Fini3D
		 * @note Attribut statique
		 */
        static const std::string PLAN_FINI3D;
      
        /**
		 * @brief Balise Brique
		 * @note Attribut statique
		 */
        static const std::string BRIQUE;
      
        /**
		 * @brief Balise Brique3D
		 * @note Attribut statique
		 */
        static const std::string BRIQUE3D;
      
        /**
		 * @brief Balise Vent
		 * @note Attribut statique
		 */
        static const std::string VENT;
      
        /**
		 * @brief Balise Champ_Forces_Global
		 * @note Attribut statique
		 */
        static const std::string CHAMP_FORCE_GLOBAL;
      
        /**
		 * @brief Balise Ventilateur
		 * @note Attribut statique
		 */
        static const std::string VENTILATEUR;
      
        /**
		 * @brief Balise Ventilateur3D
		 * @note Attribut statique
		 */
        static const std::string VENTILATEUR3D;
      
        /**
		 * @brief Balise Trampoline
		 * @note Attribut statique
		 */
        static const std::string TRAMPOLINE;
      
        /**
		 * @brief Balise Trampoline3D
		 * @note Attribut statique
		 */
        static const std::string TRAMPOLINE3D;

        /**
		 * @brief Cha�ne de caract�res "	"
		 * @note Attribut statique
		 */
        static const std::string TABULATION;

		/**
		 * @brief Extrait les donn�es du fichier XML et construit le Systeme en cons�quences
		 */
        void extraire();
        
        /**
         * @brief Enregistre le Systeme dans un fichier XML
         */
        void enregistrer();
        
		/**
		 * @brief Manipulateur pour Parseur::m_adresse
		 * @param adresse Nouvelle adresse pour le Parseur
		 */
		void setAdresse(std::string const& adresse);

    private:

		/**
		 * @brief M�thode de lecture d'un double
		 * @return Double lu
		 */
        double lireDouble();
        
        /**
		 * @brief M�thode de lecture d'un Vecteur tridimensionnel
		 * @return Vecteur lu
		 */
        Vecteur lireVecteur3D();
        
        /**
		 * @brief M�thode de lecture d'un Vecteur unidimensionnel
		 * @return Vecteur lu
		 */
        Vecteur lireVecteur1D();
        
        /**
		 * @brief M�thode de lecture d'une Couleur
		 * @return Couleur lue
		 */
        Couleur lireCouleur();
        
        /**
		 * @brief M�thode de lecture d'une balise
		 * @brief Stock la balise dans Parseur::m_tampon
		 */
        void lireBalise();
        
        /**
         * @brief M�thode permettant de ne pas tenir compte de commentaire
         * @brief Utilis�e lors de l'appel � Parseur::lireBalise()
         */
        void sauterCommentaire();

		/**
		 * @brief Cr�� une Balle � partir des donn�es du fichier XML
		 */ 
        void creerBalle();
        
        /**
		 * @brief Cr�� un Pendule � partir des donn�es du fichier XML
		 */
        void creerPendule();
        
        /**
		 * @brief Cr�� un Ressort � partir des donn�es du fichier XML
		 */
        void creerRessort();
        
        /**
		 * @brief Cr�� un Plan � partir des donn�es du fichier XML
		 */
        void creerPlan();
        
        /**
		 * @brief Cr�� un PlanFini � partir des donn�es du fichier XML
		 */
        void creerPlanFini();
        
        /**
		 * @brief Cr�� une Brique � partir des donn�es du fichier XML
		 */
        void creerBrique();
        
        /**
		 * @brief Cr�� un Vent � partir des donn�es du fichier XML
		 */
        void creerVent();
        
        /**
		 * @brief Cr�� un ChampForcesGlobal � partir des donn�es du fichier XML
		 */
        void creerChampForcesGlobal();
        
        /**
		 * @brief Cr�� un Ventilateur � partir des donn�es du fichier XML
		 */
        void creerVentilateur();
        
        /**
		 * @brief Cr�� un Trampoline � partir des donn�es du fichier XML
		 */
        void creerTrampoline();

        /**
		 * @brief Cr�� une Balle3D � partir des donn�es du fichier XML
		 */
        void creerBalle3D();
        
        /**
		 * @brief Cr�� un Pendule3D � partir des donn�es du fichier XML
		 */
        void creerPendule3D();
        
        /**
		 * @brief Cr�� un Ressort3D � partir des donn�es du fichier XML
		 */
        void creerRessort3D();
        
        /**
		 * @brief Cr�� un Plan3D � partir des donn�es du fichier XML
		 */
        void creerPlan3D();
        
        /**
		 * @brief Cr�� un PlanFini3D � partir des donn�es du fichier XML
		 */
        void creerPlanFini3D();
        
        /**
		 * @brief Cr�� une Brique3D � partir des donn�es du fichier XML
		 */
        void creerBrique3D();
        
        /**
		 * @brief Cr�� un Ventilateur3D � partir des donn�es du fichier XML
		 */
        void creerVentilateur3D();
        
        /**
		 * @brief Cr�� un Trampoline3D � partir des donn�es du fichier XML
		 */
        void creerTrampoline3D();

		/**
		 * @brief Parcours le fichier XML pour trouver quel type d'ObjetMobile y est renseign�
		 */
        void parcourirObjetMobile();

		/**
		 * @brief Parcours le fichier XML pour trouver quel type d'Obstacle y est renseign�
		 */
        void parcourirObstacle();

		/**
		 * @brief Parcours le fichier XML pour trouver quel type d'ObjetCompose y est renseign�
		 */
        void parcourirObjetCompose();

		/**
		 * @brief Parcours le fichier XML pour trouver quel type de ChampForces y est renseign�
		 */
        void parcourirChampForces();

		/**
		 * @brief Adresse du fichier XML
		 */
        std::string m_adresse;

		/**
		 * @brief Flux de lecture dans le fichier XML
		 */
        std::ifstream m_fichier;

		/**
		 * @brief Pointeur sur le Systeme sur lequel on travaille
		 */
        Systeme* m_systeme;

		/**
		 * @brief Tampon contenant la derni�re balise lue
		 */
        std::string m_tampon;

};

#endif // PARSEUR_H
