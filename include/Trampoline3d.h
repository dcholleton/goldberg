/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Trampoline3d.h
 * @brief Header de la classe Trampoline3D
 */
#ifndef TRAMPOLINE3D_H
#define TRAMPOLINE3D_H

#include "Trampoline.h"
#include "Systeme.h"

/**
 * @class Trampoline3D
 * @brief Classe repr�sentant un Trampoline pour l'affichage graphique
 * @note H�rite de Trampoline
 */
class Trampoline3D : public Trampoline
{
    public:
		/**
         * @brief Construit un Trampoline3D
         * @param origine Origine du Trampoline3D, par d�fault le Vecteur nul de dimension 3
         * @param longueur Longueur du Trampoline3D, par d�fault le Vecteur unitaire sur l'axe x {1,0,0}
         * @param largeur Largeur du Trampoline3D, par d�fault le Vecteur unitaire sur l'axe y {0,1,0}
         * @param hauteur Hauteur du Trampoline3D, par d�fault 1
         * @param colorA Couleur de la face sup�rieure du Trampoline3D, par d�fault Couleur(0,1,1,1)
         * @param colorB Couleur de la face inf�rieure du Trampoline3D, par d�fault Couleur(1,0,1,1)
         * @param colorC Couleur de la face gauche du Trampoline3D, par d�fault Couleur(1,1,0,1)
         * @param colorD Couleur de la face droite du Trampoline3D, par d�fault Couleur(0.5,1,1,1)
         * @param colorE Couleur de la face arri�re du Trampoline3D, par d�fault Couleur(1,0.5,1,1)
         * @param colorF Couleur de la face avant du Trampoline3D, par d�fault Couleur(1,1,0.5,1)
         * @param alpha Coefficient de rebonds du Trampoline3D, par d�fault 1.2
         */
        Trampoline3D(Vecteur const& origine = Vecteur(3), Vecteur const& longueur = Vecteur(1,0,0), Vecteur const& largeur = Vecteur(0,1,0), double const& hauteur = 1, Couleur const& colorA = Couleur(0,1,1,1), Couleur const& colorB = Couleur(1,0,1,1), Couleur const& colorC = Couleur(1,1,0,1), Couleur const& colorD = Couleur(0.5,1,1,1), Couleur const& colorE = Couleur(1,0.5,1,1), Couleur const& colorF = Couleur(1,1,0.5,1), double const& alpha = 1.2);


		/**
         * @brief Destructeur, implant� pour le polymorphisme
         */
        virtual ~Trampoline3D();

		
		/**
          * @brief Ajoute le Trampoline3D courant au Systeme pass� en argument
          * @param syst Systeme auquel on ajoute le Trampoline3D courant
          */
        virtual void ajoute_a(Systeme* syst);
};

#endif // TRAMPOLINE3D_H
