/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Pendule.cpp
 * @brief D�finition des m�thodes de la classe Pendule
 */
#include <cmath>
#include <iostream>
#include <string>
#include <QFormLayout>
#include <QString>
#include <QLabel>
#include "include/Pendule.h"
#include "include/ObjetMobile.h"
#include "include/ChampForces.h"
#include "include/Couleur.h"
#include "include/Vecteur.h"
#include "include/Parseur.h"

using namespace std;

Pendule::Pendule(Vecteur const& position, Vecteur const& vitesse, double const& rayon, double const& masse_volumique, Vecteur const& force, double const& longueur, double const& frottement, Vecteur const& direction, Vecteur const& attache, Couleur const& color)
    : ObjetMobile(position, vitesse, rayon, masse_volumique, force, color), m_longueur(longueur), m_frottement(frottement), m_direction(direction), m_attache(attache)
{

}

Pendule::~Pendule()
{

}

Vecteur Pendule::evolution() const
{
    return { 1/( masse() * m_longueur ) * ( cos(omega()[0]) * force() * m_direction - sin(omega()[0]) * force() * Constantes::g.unitaire() - m_frottement / m_longueur * d_omega()[0] ) };
}

double const& Pendule::longueur() const
{
    return m_longueur;
}

double const& Pendule::frottement() const
{
    return m_frottement;
}

Vecteur Pendule::position() const
{
    return m_attache + m_longueur*cos(omega()[0])*Constantes::g.unitaire() + m_longueur*sin(omega()[0])*m_direction;
}


Vecteur Pendule::vitesse() const
{
    return m_longueur*d_omega()[0]*(cos(omega()[0])*m_direction - sin(omega()[0])*Constantes::g.unitaire());
}

void Pendule::set_vitesse(Vecteur const& new_vitesse)
{
	// On garde uniquement la composante tangentielle de la vitesse
	
    Vecteur e_theta( -sin(omega()[0]) * Constantes::g.unitaire() + cos(omega()[0]) * m_direction.unitaire() );

    double vitesse(new_vitesse*e_theta);

    set_d_omega( {vitesse/m_longueur});
}

Vecteur const& Pendule::direction() const
{
    return m_direction;
}

Vecteur const& Pendule::attache() const
{
    return m_attache;
}

void Pendule::affiche(std::ostream& out) const
{
    out << "un pendule :"                          << endl
        <<  masse()      << " # masse pendule"     << endl
        <<  m_longueur   << " # longueur pendule"  << endl
        <<  m_frottement << " # frottement pendule"<< endl
        <<  m_attache    << "# attache pendule"    << endl
        <<  m_direction  << "# direction pendule"  << endl
        <<  omega()      << "# Omega pendule"      << endl
        <<  d_omega()    << "# Omega' pendule"     << endl
        <<  evolution()  << "# evolution pendule"  << endl
        <<  force() 	 << "# force pendule"      << endl
        <<  vitesse()    << "# vitesse pendule"    << endl
        <<  position()   << "# position pendule"   << endl
        << endl;
        
}

void Pendule::ajoute_force(Vecteur const& df)
{
	// On garde uniquement la composante tengentielle de la force
    Vecteur e_theta( -sin(omega()[0]) * Constantes::g.unitaire() + cos(omega()[0]) * m_direction.unitaire() );
    Vecteur force((df*e_theta)*e_theta);
    ObjetMobile::ajoute_force(force);
}

string Pendule::type() const
{
    return "Pendule";
}
QFormLayout* Pendule::proprietesLayout() const
{
    QFormLayout* mon_layout = new QFormLayout;
    QLabel* obj_type = new QLabel(QString::fromStdString(type()));
    mon_layout->addRow("Objet : ", obj_type);
    QLabel* obj_lon = new QLabel(QString::number(longueur()));
    mon_layout->addRow("Longueur : ", obj_lon);
    QLabel* obj_masse = new QLabel(QString::number(masse()));
    mon_layout->addRow("Masse de la boule : ", obj_masse);
    QLabel* obj_rayon = new QLabel(QString::number(rayon()));
    mon_layout->addRow("Rayon de la boule : ", obj_rayon);
    QLabel* obj_masse_vol = new QLabel(QString::number(masse_volumique()));
    mon_layout->addRow("Masse volumique de la boule : ", obj_masse_vol);
    QLabel* obj_frott = new QLabel(QString::number(frottement()));
    mon_layout->addRow("Coefficient de frottement : ", obj_frott);
    QLabel* obj_attache = new QLabel(QString::number(attache()[0]) + "  " + QString::number(attache()[1]) + "  " + QString::number(attache()[2]));
    mon_layout->addRow("Point d'attache : ", obj_attache);
    QLabel* obj_position = new QLabel(QString::number(position()[0]) + "  " + QString::number(position()[1]) + "  " + QString::number(position()[2]));
    mon_layout->addRow("Position de la boule : ", obj_position);
    QLabel* obj_angle = new QLabel(QString::number(omega()[0]));
    mon_layout->addRow("Angle : ", obj_angle);
    QLabel* obj_vitesse = new QLabel(QString::number(vitesse()[0]) + "  " + QString::number(vitesse()[1]) + "  " + QString::number(vitesse()[2]));
    mon_layout->addRow("Vitesse de la boule : ", obj_vitesse);
    QLabel* obj_vitesse_angu = new QLabel(QString::number(d_omega()[0]));
    mon_layout->addRow("Vitesse angulaire : ", obj_vitesse_angu);
    QLabel* obj_accel_angu = new QLabel(QString::number(evolution()[0]));
    mon_layout->addRow("Acc�l�ration angulaire : ", obj_accel_angu);
    QLabel* obj_force = new QLabel(QString::number(force()[0]) + "  " + QString::number(force()[1]) + "  " + QString::number(force()[2]));
    mon_layout->addRow("Forces subies par la boule : ", obj_force);
    QLabel* obj_normale = new QLabel(QString::number(direction()[0]) + "  " + QString::number(direction()[1]) + "  " + QString::number(direction()[2]));
    mon_layout->addRow("Normale au plan du mouvement : ", obj_normale);
    return mon_layout;
}


void Pendule::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::PENDULE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ANGLE + "> "
        << omega() << " </" + Parseur::ANGLE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::VITESSE_ANGULAIRE + "> "
        << d_omega() << " </" + Parseur::VITESSE_ANGULAIRE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FORCE + "> "
        << force() << " </" + Parseur::FORCE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::DIRECTION + "> "
        << direction() << " </" + Parseur::DIRECTION + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ATTACHE + "> "
        << attache() << " </" + Parseur::ATTACHE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::RAYON + "> "
        << rayon() << " </" + Parseur::RAYON + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::MASSE_VOLUMIQUE + "> "
        << masse_volumique() << " </" + Parseur::MASSE_VOLUMIQUE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LONGUEUR + "> "
        << longueur() << " </" + Parseur::LONGUEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FROTTEMENT + "> "
        << frottement() << " </" + Parseur::FROTTEMENT + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> "
        << m_couleur.rouge() << " " << m_couleur.vert() << " "<< m_couleur.bleu() << " "  << m_couleur.alpha() << " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::PENDULE + ">" << endl;

}
