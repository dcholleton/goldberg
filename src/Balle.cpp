/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Balle.cpp
 * @brief D�finition des m�thodes de la classe Balle
 */

#include <cmath>
#include <string>
#include <QFormLayout>
#include <QLabel>
#include <QString>
#include <QPushButton>
#include <iostream>
#include "include/Parseur.h"
#include "include/Vecteur.h"
#include "include/Couleur.h"
#include "include/Balle.h"
#include "include/ObjetMobile.h"

using namespace std;

Balle::Balle(Vecteur const& position, Vecteur const& vitesse, double const& rayon, double const& masse_volumique, Vecteur const& force, Couleur const& color)
    : ObjetMobile(position, vitesse, rayon, masse_volumique, force, color)
{

}

Balle::~Balle()
{

}

void Balle::affiche(std::ostream& out) const
{
    out << "une balle :"                                   << endl
        << rayon()           << " # rayon balle"           << endl
        << masse()           << " # masse balle"           << endl
        << position()        << "# position balle"         << endl
        << vitesse()         << "# vitesse balle" 	       << endl
        << force()           << "# force balle"    		   << endl
        << evolution()       << "# f() balle"    		   << endl
        << masse_volumique() << " # masse volumique balle" << endl
        << endl;
}


Vecteur Balle::evolution() const
{
    return ( force() / masse() );
}

Vecteur Balle::position() const
{
    return omega();
}

Vecteur Balle::vitesse() const
{
    return d_omega();
}


void Balle::set_vitesse(Vecteur const& new_vitesse)
{
    set_d_omega(new_vitesse);
}

void Balle::ajoute_force(Vecteur const& df)
{
    ObjetMobile::ajoute_force(df);
}

string Balle::type() const
{
    return "Balle";
}

QFormLayout* Balle::proprietesLayout() const
{
    QFormLayout* mon_layout = new QFormLayout;
    QLabel* obj_type = new QLabel(QString::fromStdString(type()));
    mon_layout->addRow("Objet : ", obj_type);
    QLabel* obj_masse = new QLabel(QString::number(masse()));
    mon_layout->addRow("Masse : ", obj_masse);
    QLabel* obj_rayon = new QLabel(QString::number(rayon()));
    mon_layout->addRow("Rayon : ", obj_rayon);
    QLabel* obj_masse_vol = new QLabel(QString::number(masse_volumique()));
    mon_layout->addRow("Masse volumique : ", obj_masse_vol);
    QLabel* obj_position = new QLabel(QString::number(position()[0]) + "  " + QString::number(position()[1]) + "  " + QString::number(position()[2]));
    mon_layout->addRow("Position : ", obj_position);
    QLabel* obj_vitesse = new QLabel(QString::number(vitesse()[0]) + "  " + QString::number(vitesse()[1]) + "  " + QString::number(vitesse()[2]));
    mon_layout->addRow("Vitesse : ", obj_vitesse);
    QLabel* obj_accel = new QLabel(QString::number(evolution()[0]) + "  " + QString::number(evolution()[1]) + "  " + QString::number(evolution()[2]));
    mon_layout->addRow("Acc�l�ration : ", obj_accel);
    QLabel* obj_force = new QLabel(QString::number(force()[0]) + "  " + QString::number(force()[1]) + "  " + QString::number(force()[2]));
    mon_layout->addRow("Forces subies : ", obj_force);
    return mon_layout;
}

void Balle::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::BALLE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::POSITION + "> "
        << position() << " </" + Parseur::POSITION + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::VITESSE + "> "
        << vitesse() << " </" + Parseur::VITESSE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FORCE + "> "
        << force() << " </" + Parseur::FORCE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::RAYON + "> "
        << rayon() << " </" + Parseur::RAYON + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::MASSE_VOLUMIQUE + "> "
        << masse_volumique() << " </" + Parseur::MASSE_VOLUMIQUE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> "
        << m_couleur.rouge() << " " << m_couleur.vert() << " "<< m_couleur.bleu() << " "  << m_couleur.alpha() << " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::BALLE + ">" << endl;

}
