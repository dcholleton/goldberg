/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Trampoline3d.cpp
 * @brief D�finition des m�thodes de la classe Trampoline3D
 */
#include "include/Trampoline3d.h"
#include "include/Trampoline.h"
#include "include/Systeme.h"
#include "include/Brique3d.h"
#include "include/Brique.h"

Trampoline3D::Trampoline3D(Vecteur const& origine, Vecteur const& longueur, Vecteur const& largeur, double const& hauteur, Couleur const& colorA, Couleur const& colorB, Couleur const& colorC, Couleur const& colorD, Couleur const& colorE, Couleur const& colorF, double const& alpha)
    :Trampoline(origine,longueur,largeur,hauteur,colorA,colorB,colorC,colorD,colorE,colorF,alpha)
{
}

Trampoline3D::~Trampoline3D()
{
	
}



void Trampoline3D::ajoute_a(Systeme* syst)
{
    Brique3D* brique1 = new Brique3D(*m_brique1);
    Brique3D* brique2 = new Brique3D(*m_brique2);
    brique1->ajoute_a(syst);
    brique2->ajoute_a(syst);
    delete m_brique1; // On supprime les vieilles briques, qui ne seront pas plac�es dans des unique_ptr
    delete m_brique2;
}
