/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Vent.cpp
 * @brief D�finition des m�thodes de la classe Vent
 */
#include <QLabel>
#include "include/Vent.h"
#include "include/ChampForces.h"
#include "include/Vecteur.h"
#include "include/Parseur.h"

using namespace std;

Vent::Vent(Vecteur const& origine, Vecteur const& normale, Vecteur const& longueur, Vecteur const& largeur, double const& profondeur, double const& intensite)
    :ChampForces(intensite * normale), m_origine(origine), m_normale(normale.unitaire()), m_longueur(longueur - (longueur*m_normale)*m_normale), m_largeur(largeur - (largeur*m_normale)*m_normale - (largeur*m_longueur.unitaire())* (m_longueur.unitaire()) ), m_profondeur(profondeur)
{
}

Vent::~Vent()
{
}


Vecteur const& Vent::origine() const
{
    return m_origine;
}

Vecteur const& Vent::normale() const
{
    return m_normale;
}

Vecteur const& Vent::longueur() const
{
    return m_longueur;
}


Vecteur const& Vent::largeur() const
{
    return m_largeur;
}


double const& Vent::profondeur() const
{
    return m_profondeur;
}



void Vent::agit_sur(ObjetMobile &objet)
{
    double p_normale = ( objet.position() - origine() ) * normale().unitaire(); // On calcul les projections
    double p_longueur = ( objet.position() - origine() ) * longueur().unitaire();
    double p_largeur = ( objet.position() - origine() ) * largeur().unitaire();
    // Si l'objet est dans la zone d'effet, on applique la force
    if ( p_normale > 0 && p_normale < profondeur() && p_longueur > 0 && p_longueur < longueur().norme() && p_largeur > 0 && p_largeur < largeur().norme() )
    {
        objet.ajoute_force(3.77*(profondeur()-p_normale)/profondeur()*(intensite()*intensite())*(objet.rayon()*objet.rayon())*normale());
    }
}

string Vent::type() const
{
    return "Vent";
}

QFormLayout* Vent::proprietesLayout() const
{
    QFormLayout* mon_layout = new QFormLayout;
    QLabel* obj_type = new QLabel(QString::fromStdString(type()));
    mon_layout->addRow("�l�ment : ", obj_type);
    QLabel* obj_orig = new QLabel(QString::number(origine()[0]) + "  " + QString::number(origine()[1]) + "  " + QString::number(origine()[2]));
    mon_layout->addRow("Origine : ", obj_orig);
    QLabel* obj_norm = new QLabel(QString::number(normale()[0]) + "  " + QString::number(normale()[1]) + "  " + QString::number(normale()[2]));
    mon_layout->addRow("Normale au plan de souffle : ", obj_norm);
    QLabel* obj_long = new QLabel(QString::number(longueur()[0]) + "  " + QString::number(longueur()[1]) + "  " + QString::number(longueur()[2]));
    mon_layout->addRow("Longueur : ", obj_long);
    QLabel* obj_larg = new QLabel(QString::number(largeur()[0]) + "  " + QString::number(largeur()[1]) + "  " + QString::number(largeur()[2]));
    mon_layout->addRow("Largeur : ", obj_larg);
    QLabel* obj_profondeur = new QLabel(QString::number(profondeur()));
    mon_layout->addRow("Profondeur : ", obj_profondeur);
    QLabel* obj_intensite = new QLabel(QString::number(intensite().norme()));
    mon_layout->addRow("Intensit� : ", obj_intensite);
    return mon_layout;
}

void Vent::affiche(ostream& out) const
{
    out     << "un vent : " << endl
            << m_origine << "# origine champ de forces global" << endl
            << m_normale << "# normale champ de forces global" << endl
            << m_longueur << "# longueur champ de forces global"  << endl
            << m_largeur << "# largeur champ de forces global"  <<endl
            << m_profondeur << "# profondeur champ de forces global"  << endl
            << intensite() << "# intensit� champ de forces global" << endl;

}

void Vent::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::VENT + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ORIGINE + "> "
        << origine() << " </" + Parseur::ORIGINE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::NORMALE + "> "
        << normale() << " </" + Parseur::NORMALE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LONGUEUR + "> "
        << longueur() << " </" + Parseur::LONGUEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LARGEUR + "> "
        << largeur() << " </" + Parseur::LARGEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::PROFONDEUR + "> "
        << profondeur() << " </" + Parseur::PROFONDEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::INTENSITE + "> "
        << intensite().norme() << " </" + Parseur::INTENSITE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::VENT + ">" << endl;

}
