/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Ventilateur.cpp
 * @brief D�finition des m�thodes de la classe Ventilateur
 */

#include <iostream>
#include <QFormLayout>
#include <QString>
#include <QLabel>
#include "include/Ventilateur.h"
#include "include/PlanFini.h"
#include "include/Vecteur.h"
#include "include/Obstacle.h"
#include "include/Couleur.h"
#include "include/ObjetMobile.h"
#include "include/ObjetCompose.h"
#include "include/Brique.h"
#include "include/Vent.h"

using namespace std;

Ventilateur::Ventilateur(Vecteur const& origine, Vecteur const& normale, Vecteur const& longueur, Vecteur const& largeur, double const& hauteur, double const& profondeur, Couleur const& colorA, Couleur const& colorB, Couleur const& colorC, Couleur const& colorD, Couleur const& colorE, Couleur const& colorF, double const& alpha, double const& intensite)
    : m_vent(new Vent(origine, normale, longueur, largeur, profondeur, intensite)), m_brique(new Brique(origine, m_vent->longueur(), m_vent->largeur(), hauteur, colorA, colorB, colorC, colorD, colorE, colorF, alpha))
{

}


Ventilateur::~Ventilateur()
{

}

void Ventilateur::ajoute_a(Systeme* syst)
{
    m_brique->ajoute_a(syst);
    m_vent->ajoute_a(syst);
}
