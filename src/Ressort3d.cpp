/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Ressort3d.cpp
 * @brief D�finition des m�thodes de la classe Ressort3D
 */
#include <GL/glu.h>
#include <string>
#include "include/Ressort3d.h"
#include "include/Couleur.h"
#include "include/Vecteur.h"
#include "include/Ressort.h"
#include "include/Parseur.h"


using namespace std;

Ressort3D::Ressort3D(Vecteur const& omega, Vecteur const& d_omega, double const& rayon, double const& masse_volumique, Vecteur const& force, double const& raideur, double const& frottement, Vecteur const& direction, Vecteur const& attache, Couleur const& color)
    :Ressort(omega, d_omega, rayon, masse_volumique, force, raideur, frottement, direction, attache, color), m_sphere(gluNewQuadric())
{
}

Ressort3D::~Ressort3D()
{
    gluDeleteQuadric(m_sphere);
}

void Ressort3D::dessine() const
{
    glPushMatrix();
    glColor4d(m_couleur.rouge(),  m_couleur.vert(), m_couleur.bleu(), m_couleur.alpha());
    glBegin(GL_LINES); // On dessine le "fil" du ressort (on fait un fil, plus simple � dessiner que des spires)
    glVertex3d(attache()[0],attache()[1],attache()[2]);
    glVertex3d(position()[0], position()[1], position()[2]);
    glEnd();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glTranslated(position()[0], position()[1], position()[2]);
    gluSphere(m_sphere, rayon(), 30, 30); // Puis on dessine la Sph�re
    glPopMatrix();
}

string Ressort3D::type() const
{
    return "Ressort3D";
}

void Ressort3D::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::RESSORT3D + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::POSITION + "> "
        << omega() << " </" + Parseur::POSITION + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::VITESSE + "> "
        << d_omega() << " </" + Parseur::VITESSE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FORCE + "> "
        << force() << " </" + Parseur::FORCE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::DIRECTION + "> "
        << direction() << " </" + Parseur::DIRECTION + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ATTACHE + "> "
        << attache() << " </" + Parseur::ATTACHE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::RAYON + "> "
        << rayon() << " </" + Parseur::RAYON + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::MASSE_VOLUMIQUE + "> "
        << masse_volumique() << " </" + Parseur::MASSE_VOLUMIQUE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::RAIDEUR + "> "
        << raideur() << " </" + Parseur::RAIDEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FROTTEMENT + "> "
        << frottement() << " </" + Parseur::FROTTEMENT + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> "
        << m_couleur.rouge() << " " << m_couleur.vert() << " "<< m_couleur.bleu() << " "  << m_couleur.alpha() << " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::RESSORT3D + ">" << endl;

}

