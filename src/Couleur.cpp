/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Couleur.cpp
 * @brief D�finition des m�thodes de la classe Couleur
 */
#include "include/Couleur.h"


Couleur::Couleur(double const & rouge, double const & vert, double const & bleu, double const & alpha)
    :m_rouge(rouge), m_vert(vert), m_bleu(bleu), m_alpha(alpha)
{

}

double const & Couleur::rouge() const
{
    return m_rouge;
}

double const & Couleur::vert() const
{
    return m_vert;
}

double const & Couleur::bleu() const
{
    return m_bleu;
}

double const & Couleur::alpha() const
{
    return m_alpha;
}

void Couleur::setCouleur(double const & rouge, double const & vert, double const & bleu, double const & alpha)
{
    m_rouge = rouge;
    m_vert = vert;
    m_bleu = bleu;
    m_alpha = alpha;
}

