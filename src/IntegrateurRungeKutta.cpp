/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file IntegrateurRungeKutta.cpp
 * @brief D�finition des m�thodes de la classe IntegrateurRungeKutta
 */
#include "../include/Integrateur.h"
#include "../include/IntegrateurRungeKutta.h"
#include "../include/Vecteur.h"
#include "../include/ObjetMobile.h"


using namespace std;

IntegrateurRungeKutta::IntegrateurRungeKutta(double const& dt)
    : Integrateur(dt)
{

}

IntegrateurRungeKutta::~IntegrateurRungeKutta()
{

}

void IntegrateurRungeKutta::integre(ObjetMobile& objet) const
{
	// Impl�mant� selon le compl�ment math�matique
    Vecteur omega (objet.omega());
    Vecteur d_omega (objet.d_omega());

    Vecteur k1 (d_omega);
    Vecteur K1 (objet.evolution());
    Vecteur k2 (d_omega + dt()*K1/2);
    objet.set_omega(omega + dt()*k1/2);
    objet.set_d_omega(k2);
    Vecteur K2 (objet.evolution());
    Vecteur k3 (d_omega + dt()*K2/2);
    objet.set_omega(omega + dt()*k2/2);
    objet.set_d_omega(k3);
    Vecteur K3 (objet.evolution());
    Vecteur k4 (d_omega + dt()*K3/2);
    objet.set_omega(omega + dt()*k3/2);
    objet.set_d_omega(k4);
    Vecteur K4 (objet.evolution());

    objet.set_omega( omega + dt()*( k1 + k2 + k3 + k4 )/6 );
    objet.set_d_omega( d_omega + dt()*( K1 + K2 + K3 + K4 )/6  );
}

