/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Parseur.cpp
 * @brief D�finition des m�thodes de la classe Parseur
 */
#include "include/Parseur.h"
#include <string>
#include <fstream>
#include "include/Vecteur.h"
#include "include/Systeme.h"
#include "include/Balle.h"
#include "include/Balle3d.h"
#include "include/Pendule.h"
#include "include/Pendule3d.h"
#include "include/Ressort.h"
#include "include/Ressort3d.h"
#include "include/Plan.h"
#include "include/Plan3d.h"
#include "include/PlanFini.h"
#include "include/PlanFini3d.h"
#include "include/Brique.h"
#include "include/Brique3d.h"
#include "include/Vent.h"
#include "include/Ventilateur.h"
#include "include/Ventilateur3d.h"
#include "include/Trampoline.h"
#include "include/ChampForcesGlobal.h"
#include "include/Trampoline3d.h"

using namespace std;



// D�claration des Balises

const string Parseur::TABULATION = "    ";

const string Parseur::SYSTEME = "Systeme";
const string Parseur::OBJET_MOBILE = "ObjetMobile";
const string Parseur::OBSTACLE = "Obstacle";
const string Parseur::OBJET_COMPOSE = "ObjetCompose";
const string Parseur::CHAMP_FORCE = "ChampForces";

const string Parseur::POSITION = "Position";
const string Parseur::VITESSE = "Vitesse";
const string Parseur::FORCE = "Force";
const string Parseur::RAYON = "Rayon";
const string Parseur::MASSE_VOLUMIQUE = "Masse_Volumique";
const string Parseur::COULEUR = "Couleur";
const string Parseur::ANGLE = "Angle";
const string Parseur::VITESSE_ANGULAIRE = "Vitesse_Angulaire";
const string Parseur::DIRECTION = "Direction";
const string Parseur::ATTACHE = "Attache";
const string Parseur::LONGUEUR = "Longueur";
const string Parseur::FROTTEMENT = "Frottement";
const string Parseur::RAIDEUR = "Raideur";
const string Parseur::ORIGINE = "Origine";
const string Parseur::NORMALE = "Normale";
const string Parseur::COEFFICIENT_REBOND = "Coefficient_Rebond";
const string Parseur::LARGEUR = "Largeur";
const string Parseur::HAUTEUR = "Hauteur";
const string Parseur::FACE_A = "FaceA";
const string Parseur::FACE_B = "FaceB";
const string Parseur::FACE_C = "FaceC";
const string Parseur::FACE_D = "FaceD";
const string Parseur::FACE_E = "FaceE";
const string Parseur::FACE_F = "FaceF";
const string Parseur::PROFONDEUR = "Profondeur";
const string Parseur::INTENSITE = "Intensite";


const string Parseur::BALLE = "Balle";
const string Parseur::BALLE3D = "Balle3D";
const string Parseur::RESSORT = "Ressort";
const string Parseur::RESSORT3D = "Ressort3D";
const string Parseur::PENDULE = "Pendule";
const string Parseur::PENDULE3D = "Pendule3D";
const string Parseur::PLAN = "Plan";
const string Parseur::PLAN3D = "Plan3D";
const string Parseur::PLAN_FINI = "PlanFini";
const string Parseur::PLAN_FINI3D = "PlanFini3D";
const string Parseur::BRIQUE = "Brique";
const string Parseur::BRIQUE3D = "Brique3D";
const string Parseur::VENT = "Vent";
const string Parseur::CHAMP_FORCE_GLOBAL = "ChampForcesGlobal";
const string Parseur::VENTILATEUR = "Ventilateur";
const string Parseur::VENTILATEUR3D = "Ventilateur3D";
const string Parseur::TRAMPOLINE = "Trampoline";
const string Parseur::TRAMPOLINE3D = "Trampoline3D";







Parseur::Parseur(string const& adresse, Systeme* syst )
    : m_adresse(adresse), m_systeme(syst)
{


}

Parseur::~Parseur()
{
    if ( m_fichier.is_open()  )
    {
        m_fichier.close();

    }
}

void Parseur::extraire()
{
    try
    {
        m_fichier.open(m_adresse);

        if( m_fichier.is_open() )
        {

            lireBalise();
            
            // On v�rifie la balise Systeme

            if ( m_tampon == Parseur::SYSTEME )
            {

                lireBalise();

                do
                {
					// On cherche quel Balise est pr�sente tant qu'on ne tombe pas sur la fermeture de la balise Systeme
					// On aurait pr�f�r� utiliser un switch, mais on a pas r�ussi avec des string
                    if ( m_tampon == Parseur::OBJET_MOBILE )
                    {
                        parcourirObjetMobile();
                        lireBalise();
                    }
                    else if( m_tampon == Parseur::OBSTACLE )
                    {
                        parcourirObstacle();
                        lireBalise();
                    }
                    else if( m_tampon == Parseur::OBJET_COMPOSE )
                    {
                        parcourirObjetCompose();
                        lireBalise();
                    }
                    else if( m_tampon == Parseur::CHAMP_FORCE )
                    {
                        parcourirChampForces();
                        lireBalise();
                    }


                }
                while( m_tampon != "/" + Parseur::SYSTEME );

            }
            m_fichier.close();
        }
    }
    catch (string error)
    {
		// Si on a une erreur de Balise, on l'attrape et on l'affiche sur la sortie d'erreur standard
        cerr << endl << error << endl;
    }
}



double Parseur::lireDouble()
{
    double x;
    m_fichier >> x;
    return x;
}

Vecteur Parseur::lireVecteur3D()
{
    double x, y, z;

    x = lireDouble();

    y = lireDouble();

    z = lireDouble();

    return Vecteur(x,y,z);
}

Vecteur Parseur::lireVecteur1D()
{
    double x(lireDouble());

    return Vecteur( {x});
}

Couleur Parseur::lireCouleur()
{
    double R, G, B, A;
    R = lireDouble();
    G = lireDouble();
    B = lireDouble();
    A = lireDouble();
    return Couleur(R, G, B, A);
}
void Parseur::lireBalise()
{

    m_fichier >> m_tampon ;
    // Si on tombe sur un commentaire, on le saute
    if (m_tampon == "<!--")
    {
        sauterCommentaire();
        lireBalise();
    }
    // Sinon, on v�rifie qu'on a bien une Balise, car on avait eu un petit bug la dessus
    else
    {
        if(m_tampon.size() > 1)
        {
            m_tampon = m_tampon.substr(1, m_tampon.size()-2) ;
        }
        else
        {
            m_tampon.clear() ;
        }
    }
}

void Parseur::sauterCommentaire()
{
    do
    {
		// Tant qu'on a pas la fun du bloc de commentaire, on lit le fichier
        m_fichier >> m_tampon;
    }
    while (m_tampon != "-->");
}



void Parseur::creerBalle()
{
	// On initialise a des valeurs par d�fault les param�tres du constructeur
	Vecteur position = Vecteur(3), vitesse = Vecteur(3), force = Vecteur(3);
    double rayon = 1, masse_volumique = 1;
    Couleur color(0,0,1,1);


    lireBalise();
	// Et on cherche les balises pr�sentes pour modifier ces valeurs par d�fault
	// On arr�te la recherche � la fermeture de la Balise Balle
	// On utilise le m�me syst�me pour tous les Elements, mais on ne recommenteras pas (on recommence les commentaires � parcourirObstacle)
    do
    {
        if ( m_tampon == Parseur::POSITION )
        {
            position = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::POSITION )
            {
				// Si on ne tombe pas sur la bonne balise attendue, on lance une exception
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::VITESSE )
        {
            vitesse = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::VITESSE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::FORCE )
        {
            force = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::FORCE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::RAYON )
        {

            rayon = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::RAYON )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::MASSE_VOLUMIQUE )
        {
            masse_volumique = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::MASSE_VOLUMIQUE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            color = lireCouleur();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COULEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

    }
    while ( m_tampon != "/" + Parseur::BALLE );

	// Enfin, on cr�� la Balle
    m_systeme->ajoute( new Balle(position, vitesse, rayon, masse_volumique, force, color) );
}

void Parseur::creerPendule()
{
    Vecteur omega = Vecteur(1), d_omega = Vecteur(1), force = Vecteur(3), direction = Vecteur(0,1,0), attache = Vecteur(3);
    double rayon = 0.1, masse_volumique = 200, longueur = 0.5 , frottement = 0.05;
    Couleur color(1,0,0,1);


    lireBalise();

    do
    {
        if ( m_tampon == Parseur::ANGLE )
        {
            omega = lireVecteur1D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ANGLE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::VITESSE_ANGULAIRE )
        {
            d_omega = lireVecteur1D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::VITESSE_ANGULAIRE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::FORCE )
        {
            force = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::FORCE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::DIRECTION )
        {
            direction = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::DIRECTION )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::ATTACHE )
        {
            attache = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ATTACHE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::RAYON )
        {

            rayon = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::RAYON )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::MASSE_VOLUMIQUE )
        {
            masse_volumique = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::MASSE_VOLUMIQUE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::FROTTEMENT )
        {
            frottement = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::FROTTEMENT )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            color = lireCouleur();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COULEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

    }
    while ( m_tampon != "/" + Parseur::PENDULE );

    m_systeme->ajoute( new Pendule( omega, d_omega, rayon, masse_volumique, force, longueur, frottement, direction, attache, color) );
}





void Parseur::creerRessort()
{
    Vecteur omega = Vecteur(1), d_omega = Vecteur(1), force = Vecteur(3), direction = Vecteur(0,0,1), attache = Vecteur(3);
    double rayon = 0.1, masse_volumique = 200, raideur = 10 , frottement = 0.05;
    Couleur color(0,1,0,1);


    lireBalise();

    do
    {
        if ( m_tampon == Parseur::POSITION )
        {
            omega = lireVecteur1D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::POSITION )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::VITESSE )
        {
            d_omega = lireVecteur1D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::VITESSE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::FORCE )
        {
            force = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::FORCE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::DIRECTION )
        {
            direction = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::DIRECTION )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::ATTACHE )
        {
            attache = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ATTACHE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::RAYON )
        {

            rayon = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::RAYON )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::MASSE_VOLUMIQUE )
        {
            masse_volumique = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::MASSE_VOLUMIQUE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::RAIDEUR )
        {
            raideur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::RAIDEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::FROTTEMENT )
        {
            frottement = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::FROTTEMENT )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            color = lireCouleur();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COULEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

    }
    while ( m_tampon != "/" + Parseur::RESSORT );

    m_systeme->ajoute( new Ressort( omega, d_omega, rayon, masse_volumique, force, raideur, frottement, direction, attache, color) );
}


void Parseur::creerPlan()
{
    Vecteur origine = Vecteur(3), normale = Vecteur(0,0,1);
    Couleur color(1,1,1,1);
    double alpha = 0.8;


    lireBalise();

    do
    {
        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::NORMALE )
        {
            normale = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::NORMALE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COEFFICIENT_REBOND )
        {
            alpha = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COEFFICIENT_REBOND )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            color = lireCouleur();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COULEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

    }
    while( m_tampon != "/" + Parseur::PLAN );

    m_systeme->ajoute( new Plan (origine, normale, color, alpha ));
}

void Parseur::creerPlanFini()
{
    Vecteur origine = Vecteur(3), normale = Vecteur(0,0,1), longueur = Vecteur(1,0,0), largeur = Vecteur(0,1,0);
    Couleur color(1,1,1,1);
    double alpha = 0.8;


    lireBalise();

    do
    {
        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::NORMALE )
        {
            normale = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::NORMALE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LARGEUR )
        {
            largeur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LARGEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COEFFICIENT_REBOND )
        {
            alpha = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COEFFICIENT_REBOND )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            color = lireCouleur();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COULEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

    }
    while( m_tampon != "/" + Parseur::PLAN_FINI );

    m_systeme->ajoute( new PlanFini (origine, normale, longueur, largeur, color, alpha ));
}


void Parseur::creerBrique()
{
    Vecteur origine = Vecteur(3), longueur = Vecteur(0,1,0), largeur = Vecteur(1,0,0);
    Couleur colorA = Couleur(1,0,0,1), colorB = Couleur(0,1,0,1), colorC = Couleur(0,0,1,1), colorD = Couleur(1,1,0,1), colorE = Couleur(0,1,1,1), colorF = Couleur(1,0,1,1);
    double alpha = 0.8, hauteur = 1;


    lireBalise();

    do
    {

        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LARGEUR )
        {
            largeur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LARGEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COEFFICIENT_REBOND )
        {
            alpha = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COEFFICIENT_REBOND )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::HAUTEUR )
        {
            hauteur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::HAUTEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

        else if( m_tampon == Parseur::COULEUR )
        {
            lireBalise();

            do
            {

                if( m_tampon == Parseur::FACE_A )
                {
                    colorA = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_A )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_B )
                {
                    colorB = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_B )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_C )
                {
                    colorC = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_C )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_D )
                {
                    colorD = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_D )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_E )
                {
                    colorE = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_E )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_F )
                {
                    colorF = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_F )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }

            }
            while (m_tampon != "/" + Parseur::COULEUR);
            lireBalise();
        }

    }
    while( m_tampon != "/" + Parseur::BRIQUE );
    m_systeme->ajoute( new Brique(origine, longueur, largeur, hauteur, colorA, colorB, colorC, colorD, colorE, colorF, alpha) );
}

void Parseur::creerVent()
{
    Vecteur origine = Vecteur(3), normale = Vecteur (0, 0, 1), longueur = Vecteur (0, 1, 0) , largeur = Vecteur( 1, 0, 0);
    double profondeur = 1, intensite = 3.5;


    lireBalise();

    do
    {
        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::NORMALE )
        {
            normale = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::NORMALE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LARGEUR )
        {
            largeur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LARGEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::PROFONDEUR )
        {
            profondeur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::PROFONDEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::INTENSITE )
        {
            intensite = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::INTENSITE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
    }
    while( m_tampon != "/" + Parseur::VENT );
    m_systeme->ajoute( new Vent(origine, normale, longueur, largeur, profondeur, intensite) );
}

void Parseur::creerChampForcesGlobal()
{
    Vecteur intensite(0,0,-9.81);


    lireBalise();

    do
    {

        if( m_tampon == Parseur::INTENSITE )
        {
            intensite = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::INTENSITE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
    }
    while( m_tampon != "/" + Parseur::CHAMP_FORCE_GLOBAL );
    m_systeme->ajoute( new ChampForcesGlobal(intensite) );

}

void Parseur::creerVentilateur()
{
    Vecteur origine = Vecteur(3), normale = Vecteur (0, 0, 1), longueur = Vecteur (1, 0, 0) , largeur = Vecteur( 0, 1, 0);
    double profondeur = 1, intensite = 3.5, hauteur = 0.1, alpha = 0.8;
    Couleur colorA = Couleur(1,1,1,1), colorB = Couleur(0.6,0.6,0.6,1), colorC = Couleur(0.5,0.5,0.5,1), colorD = Couleur(0.5,0.5,0.5,1), colorE = Couleur(0.3,0.3,0.3,1), colorF = Couleur(0.3,0.3,0.3,1);


    lireBalise();

    do
    {
        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::NORMALE )
        {
            normale = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::NORMALE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LARGEUR )
        {
            largeur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LARGEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::PROFONDEUR )
        {
            profondeur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::PROFONDEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::INTENSITE )
        {
            intensite = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::INTENSITE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COEFFICIENT_REBOND )
        {
            alpha = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COEFFICIENT_REBOND )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::HAUTEUR )
        {
            hauteur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::HAUTEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

        else if( m_tampon == Parseur::COULEUR )
        {
            lireBalise();

            do
            {

                if( m_tampon == Parseur::FACE_A )
                {
                    colorA = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_A )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_B )
                {
                    colorB = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_B )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_C )
                {
                    colorC = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_C )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_D )
                {
                    colorD = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_D )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_E )
                {
                    colorE = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_E )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_F )
                {
                    colorF = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_F )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }

            }
            while (m_tampon != "/" + Parseur::COULEUR);
            lireBalise();
        }

    }
    while( m_tampon != "/" + Parseur::VENTILATEUR );

    m_systeme->ajoute( new Ventilateur(origine, normale, longueur, largeur, hauteur, profondeur, colorA, colorB, colorC, colorD, colorE, colorF, alpha, intensite) );
}

void Parseur::creerTrampoline()
{
    Vecteur origine = Vecteur(3), longueur = Vecteur(1,0,0), largeur = Vecteur(0,1,0);
    Couleur colorA = Couleur(0,1,1,1), colorB = Couleur(1,0,1,1), colorC = Couleur(1,1,0,1), colorD = Couleur(0.5,1,1,1), colorE = Couleur(1,0.5,1,1), colorF = Couleur(1,1,0.5,1);
    double alpha = 1.5, hauteur = 1;


    lireBalise();

    do
    {
        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LARGEUR )
        {
            largeur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LARGEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::HAUTEUR )
        {
            hauteur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::HAUTEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COEFFICIENT_REBOND )
        {
            alpha = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COEFFICIENT_REBOND )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            lireBalise();

            do
            {

                if( m_tampon == Parseur::FACE_A )
                {
                    colorA = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_A )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_B )
                {
                    colorB = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_B )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_C )
                {
                    colorC = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_C )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_D )
                {
                    colorD = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_D )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_E )
                {
                    colorE = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_E )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_F )
                {
                    colorF = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_F )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }

            }
            while (m_tampon != "/" + Parseur::COULEUR);
            lireBalise();
        }
    }
    while( m_tampon != "/" + Parseur::TRAMPOLINE );
    m_systeme->ajoute( new Trampoline(origine, longueur, largeur, hauteur, colorA, colorB, colorC, colorD, colorE, colorF, alpha) );
}

void Parseur::creerBalle3D()
{
    Vecteur position = Vecteur(3), vitesse = Vecteur(3), force = Vecteur(3);
    double rayon = 1, masse_volumique = 1;
    Couleur color  = Couleur(0,0,1,1);


    lireBalise();

    do
    {
        if ( m_tampon == Parseur::POSITION )
        {
            position = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::POSITION )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::VITESSE )
        {
            vitesse = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::VITESSE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::FORCE )
        {
            force = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::FORCE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::RAYON )
        {

            rayon = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::RAYON )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::MASSE_VOLUMIQUE )
        {
            masse_volumique = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::MASSE_VOLUMIQUE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            color = lireCouleur();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COULEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

    }
    while ( m_tampon != "/" + Parseur::BALLE3D );

    m_systeme->ajoute( new Balle3D(position, vitesse, rayon, masse_volumique, force, color) );
}

void Parseur::creerPendule3D()
{
    Vecteur omega = Vecteur(1), d_omega = Vecteur(1), force = Vecteur(3), direction = Vecteur(0,1,0), attache = Vecteur(3);
    double rayon = 0.1, masse_volumique = 200, longueur = 0.5 , frottement = 0.05;
    Couleur color(1,0,0,1);


    lireBalise();

    do
    {
        if ( m_tampon == Parseur::ANGLE )
        {
            omega = lireVecteur1D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ANGLE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::VITESSE_ANGULAIRE )
        {
            d_omega = lireVecteur1D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::VITESSE_ANGULAIRE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::FORCE )
        {
            force = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::FORCE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::DIRECTION )
        {
            direction = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::DIRECTION )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::ATTACHE )
        {
            attache = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ATTACHE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::RAYON )
        {

            rayon = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::RAYON )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::MASSE_VOLUMIQUE )
        {
            masse_volumique = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::MASSE_VOLUMIQUE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::FROTTEMENT )
        {
            frottement = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::FROTTEMENT )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            color = lireCouleur();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COULEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

    }
    while ( m_tampon != "/" + Parseur::PENDULE3D );

    m_systeme->ajoute( new Pendule3D( omega, d_omega, rayon, masse_volumique, force, longueur, frottement, direction, attache, color) );
}
void Parseur::creerRessort3D()
{
    Vecteur omega = Vecteur(1), d_omega = Vecteur(1), force = Vecteur(3), direction = Vecteur(0,0,1), attache = Vecteur(3);
    double rayon = 0.1, masse_volumique = 200, raideur = 10 , frottement = 0.05;
    Couleur color(0,1,0,1);


    lireBalise();

    do
    {
        if ( m_tampon == Parseur::POSITION )
        {
            omega = lireVecteur1D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::POSITION )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::VITESSE )
        {
            d_omega = lireVecteur1D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::VITESSE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::FORCE )
        {
            force = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::FORCE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::DIRECTION )
        {
            direction = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::DIRECTION )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::ATTACHE )
        {
            attache = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ATTACHE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::RAYON )
        {

            rayon = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::RAYON )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();

        }
        else if( m_tampon == Parseur::MASSE_VOLUMIQUE )
        {
            masse_volumique = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::MASSE_VOLUMIQUE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::RAIDEUR )
        {
            raideur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::RAIDEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::FROTTEMENT )
        {
            frottement = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::FROTTEMENT )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            color = lireCouleur();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COULEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

    }
    while ( m_tampon != "/" + Parseur::RESSORT3D );

    m_systeme->ajoute( new Ressort3D( omega, d_omega, rayon, masse_volumique, force, raideur, frottement, direction, attache, color) );
}



void Parseur::creerPlan3D()
{
    Vecteur origine = Vecteur(3), normale = Vecteur(0,0,1);
    Couleur color = Couleur(1,1,1,1);
    double alpha = 0.8;


    lireBalise();

    do
    {
        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::NORMALE )
        {
            normale = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::NORMALE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COEFFICIENT_REBOND )
        {
            alpha = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COEFFICIENT_REBOND )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            color = lireCouleur();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COULEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

    }
    while( m_tampon != "/" + Parseur::PLAN3D );

    m_systeme->ajoute( new Plan3D (origine, normale, color, alpha ));
}

void Parseur::creerPlanFini3D()
{
    Vecteur origine = Vecteur(3), normale = Vecteur(0,0,1), longueur = Vecteur(1,0,0), largeur = Vecteur(0,1,0);
    Couleur color(1,1,1,1);
    double alpha = 0.8;


    lireBalise();

    do
    {
        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::NORMALE )
        {
            normale = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::NORMALE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LARGEUR )
        {
            largeur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LARGEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COEFFICIENT_REBOND )
        {
            alpha = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COEFFICIENT_REBOND )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            color = lireCouleur();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COULEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

    }
    while( m_tampon != "/" + Parseur::PLAN_FINI3D );

    m_systeme->ajoute( new PlanFini3D (origine, normale, longueur, largeur, color, alpha ));
}

void Parseur::creerBrique3D()
{
    Vecteur origine = Vecteur(3), longueur = Vecteur(0,1,0), largeur = Vecteur(1,0,0);
    Couleur colorA = Couleur(1,0,0,1), colorB = Couleur(0,1,0,1), colorC = Couleur(0,0,1,1), colorD = Couleur(1,1,0,1), colorE = Couleur(0,1,1,1), colorF = Couleur(1,0,1,1);
    double alpha = 0.8, hauteur = 1;


    lireBalise();

    do
    {

        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LARGEUR )
        {
            largeur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LARGEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COEFFICIENT_REBOND )
        {
            alpha = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COEFFICIENT_REBOND )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::HAUTEUR )
        {
            hauteur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::HAUTEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

        else if( m_tampon == Parseur::COULEUR )
        {
            lireBalise();

            do
            {

                if( m_tampon == Parseur::FACE_A )
                {
                    colorA = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_A )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_B )
                {
                    colorB = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_B )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_C )
                {
                    colorC = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_C )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_D )
                {
                    colorD = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_D )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_E )
                {
                    colorE = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_E )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_F )
                {
                    colorF = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_F )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }

            }
            while (m_tampon != "/" + Parseur::COULEUR);
            lireBalise();
        }

    }
    while( m_tampon != "/" + Parseur::BRIQUE3D );
    m_systeme->ajoute( new Brique3D(origine, longueur, largeur, hauteur, colorA, colorB, colorC, colorD, colorE, colorF, alpha) );
}

void Parseur::creerVentilateur3D()
{
    Vecteur origine = Vecteur(3), normale = Vecteur (0, 0, 1), longueur = Vecteur (1, 0, 0) , largeur = Vecteur( 0, 1, 0);
    double profondeur = 1, intensite = 3.5, hauteur = 0.1, alpha = 0.8;
    Couleur colorA = Couleur(1,1,1,1), colorB = Couleur(0.6,0.6,0.6,1), colorC = Couleur(0.5,0.5,0.5,1), colorD = Couleur(0.5,0.5,0.5,1), colorE = Couleur(0.3,0.3,0.3,1), colorF = Couleur(0.3,0.3,0.3,1);


    lireBalise();

    do
    {
        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::NORMALE )
        {
            normale = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::NORMALE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LARGEUR )
        {
            largeur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LARGEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::PROFONDEUR )
        {
            profondeur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::PROFONDEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::INTENSITE )
        {
            intensite = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::INTENSITE )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COEFFICIENT_REBOND )
        {
            alpha = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COEFFICIENT_REBOND )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::HAUTEUR )
        {
            hauteur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::HAUTEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }

        else if( m_tampon == Parseur::COULEUR )
        {
            lireBalise();

            do
            {

                if( m_tampon == Parseur::FACE_A )
                {
                    colorA = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_A )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_B )
                {
                    colorB = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_B )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_C )
                {
                    colorC = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_C )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_D )
                {
                    colorD = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_D )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_E )
                {
                    colorE = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_E )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_F )
                {
                    colorF = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_F )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }

            }
            while (m_tampon != "/" + Parseur::COULEUR);
            lireBalise();
        }

    }
    while( m_tampon != "/" + Parseur::VENTILATEUR3D );

    m_systeme->ajoute( new Ventilateur3D(origine, normale, longueur, largeur, hauteur, profondeur, colorA, colorB, colorC, colorD, colorE, colorF, alpha, intensite) );
}

void Parseur::creerTrampoline3D()
{
    Vecteur origine = Vecteur(3), longueur = Vecteur(1,0,0), largeur = Vecteur(0,1,0);
    Couleur colorA = Couleur(0,1,1,1), colorB = Couleur(1,0,1,1), colorC = Couleur(1,1,0,1), colorD = Couleur(0.5,1,1,1), colorE = Couleur(1,0.5,1,1), colorF = Couleur(1,1,0.5,1);
    double alpha = 1.5, hauteur = 1;


    lireBalise();

    do
    {
        if( m_tampon == Parseur::ORIGINE )
        {
            origine = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::ORIGINE )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LONGUEUR )
        {
            longueur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LONGUEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::LARGEUR )
        {
            largeur = lireVecteur3D();
            lireBalise();
            if ( m_tampon != "/" + Parseur::LARGEUR )
            {
                throw("Erreur de balise");
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::HAUTEUR )
        {
            hauteur = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::HAUTEUR )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COEFFICIENT_REBOND )
        {
            alpha = lireDouble();
            lireBalise();
            if ( m_tampon != "/" + Parseur::COEFFICIENT_REBOND )
            {
                cerr << "Erreur de balise";
            }
            lireBalise();
        }
        else if( m_tampon == Parseur::COULEUR )
        {
            lireBalise();

            do
            {

                if( m_tampon == Parseur::FACE_A )
                {
                    colorA = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_A )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_B )
                {
                    colorB = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_B )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_C )
                {
                    colorC = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_C )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_D )
                {
                    colorD = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_D )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_E )
                {
                    colorE = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_E )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }
                else if( m_tampon == Parseur::FACE_F )
                {
                    colorF = lireCouleur();
                    lireBalise();
                    if ( m_tampon != "/" + Parseur::FACE_F )
                    {
                        cerr << "Erreur de balise";
                    }
                    lireBalise();
                }

            }
            while (m_tampon != "/" + Parseur::COULEUR);
            lireBalise();
        }
    }
    while( m_tampon != "/" + Parseur::TRAMPOLINE3D );

    m_systeme->ajoute( new Trampoline3D(origine, longueur, largeur, hauteur, colorA, colorB, colorC, colorD, colorE, colorF, alpha) );
}

void Parseur::parcourirObjetMobile()
{

    lireBalise();


    do
    {
        if(m_tampon == Parseur::BALLE )
        {
            creerBalle();
            lireBalise();
        }
        else if(m_tampon == Parseur::BALLE3D )
        {
            creerBalle3D();
            lireBalise();
        }
        else if(m_tampon == Parseur::PENDULE )
        {
            creerPendule();
            lireBalise();
        }
        else if(m_tampon == Parseur::PENDULE3D )
        {
            creerPendule3D();
            lireBalise();
        }
        else if(m_tampon == Parseur::RESSORT )
        {
            creerRessort();
            lireBalise();
        }
        else if(m_tampon == Parseur::RESSORT3D )
        {
            creerRessort3D();
            lireBalise();
        }


    }
    while (m_tampon != "/" + Parseur::OBJET_MOBILE );
}

void Parseur::parcourirObstacle()
{

    lireBalise();

    do
    {
		// On utilise le m�me principe que pour extraire,
		// On cherche quel type d'Obstacle est pr�sent, jusqu'� la fermeture de la balise Obstacle
		// M�me principe pour parcourirObjetMobile, parcourirChampForces et parcourirObjetCompose, mais on ne recommente pas, et on reprends les commentaires a setAdresse
        if(m_tampon == Parseur::PLAN )
        {
			// Si on tombe sur la Balise Plan, on creerPlan prends le relais
            creerPlan();
            lireBalise();
        }
        else if(m_tampon == Parseur::PLAN3D )
        {
            creerPlan3D();
            lireBalise();
        }
        else if(m_tampon == Parseur::PLAN_FINI )
        {
            creerPlanFini();
            lireBalise();
        }
        else if(m_tampon == Parseur::PLAN_FINI3D )
        {
            creerPlanFini3D();
            lireBalise();
        }
        else if(m_tampon == Parseur::BRIQUE )
        {
            creerBrique();
            lireBalise();
        }
        else if(m_tampon == Parseur::BRIQUE3D )
        {
            creerBrique3D();
            lireBalise();
        }


    }
    while (m_tampon != "/" + Parseur::OBSTACLE );
}

void Parseur::parcourirObjetCompose()
{

    lireBalise();

    do
    {
        if(m_tampon == Parseur::TRAMPOLINE )
        {
            creerTrampoline();
            lireBalise();
        }
        else if(m_tampon == Parseur::TRAMPOLINE3D )
        {
            creerTrampoline3D();
            lireBalise();
        }
        else if(m_tampon == Parseur::VENTILATEUR )
        {
            creerVentilateur();
            lireBalise();
        }
        else if(m_tampon == Parseur::VENTILATEUR3D )
        {
            creerVentilateur3D();
            lireBalise();
        }

    }
    while (m_tampon != "/" + Parseur::OBJET_COMPOSE );
}

void Parseur::parcourirChampForces()
{

    lireBalise();

    do
    {
        if(m_tampon == Parseur::VENT )
        {
            creerVent();

            lireBalise();
        }
        else if(m_tampon == Parseur::CHAMP_FORCE_GLOBAL )
        {
            creerChampForcesGlobal();

            lireBalise();
        }

    }
    while (m_tampon != "/" + Parseur::CHAMP_FORCE );

}


void Parseur::setAdresse(string const& adresse)
{

	// Si un flux est ouvert (ce qui ne devrait pas �tre le cas), on le ferme 

    if(m_fichier.is_open())
    {
        m_fichier.close();
    }
    m_adresse = adresse;

}

void Parseur::enregistrer()
{
	// Idem, on ferme le flux au cas o�
    if ( m_fichier.is_open()  )
    {
        m_fichier.close();

    }

	// On cr�� un flux de sortie vers m_adresse
    ofstream out(m_adresse);
    
    // Si le flux s'est bien ouvert ...
    if (out.is_open())
    {
        out << "<" + Parseur::SYSTEME + ">" << endl
            << Parseur::TABULATION + "<" + Parseur::OBJET_MOBILE + ">" << endl  ;

	// On it�re sur la m�thode enregistrer de chaque �l�ment du syst�me pour enregistrer leurs �tats dans m_adresse
for (unique_ptr<ObjetMobile>& i : m_systeme->objets_mobiles())
        {
            i->enregistrer(out);
        }


        out << Parseur::TABULATION + "</" + Parseur::OBJET_MOBILE + ">" << endl  ;
        out << Parseur::TABULATION + "<" + Parseur::OBSTACLE + ">" << endl  ;

for (unique_ptr<Obstacle>& i : m_systeme->obstacles())
        {
            i->enregistrer(out);
        }

        out << Parseur::TABULATION + "</" + Parseur::OBSTACLE + ">" << endl  ;
        out << Parseur::TABULATION + "<" + Parseur::CHAMP_FORCE + ">" << endl  ;

for (unique_ptr<ChampForces>& i : m_systeme->champs())
        {
            i->enregistrer(out);
        }
        out << Parseur::TABULATION + "</" + Parseur::CHAMP_FORCE + ">" << endl  ;
        out << "</" + Parseur::SYSTEME + ">" << endl;
    }
    
	// Et on ferme le flux :)
	out.close();
}
