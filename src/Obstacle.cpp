/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Obstacle.cpp
 * @brief D�finition des m�thodes de la classe Obstacle
 */
#include "include/Obstacle.h"
#include "include/ObjetMobile.h"
#include "include/Vecteur.h"
#include "include/Systeme.h"



using namespace std;

Obstacle::Obstacle(Vecteur const& origine, Vecteur const& normale, double const& alpha)
    : m_origine(origine), m_normale(normale.unitaire()), m_alpha(alpha)
{

}


Obstacle::~Obstacle()
{

}

double Obstacle::distance(ObjetMobile const& objet) const
{
    return (objet.position() - pointPlusProche(objet)).norme() - objet.rayon();
}

Vecteur const& Obstacle::normale() const
{
    return m_normale;
}

Vecteur const& Obstacle::origine() const
{
    return m_origine;
}

void Obstacle::agit_sur(ObjetMobile& objet)
{
	// Impl�mant� selon le compl�ment math�matique
    if(distance(objet) <= 0)
    {
        Vecteur n(( objet.position() - pointPlusProche(objet) ) .unitaire() ),  dv(3);
        double vstar(-objet.vitesse()*n), mu(0.01), lambda(1+m_alpha), F_n(objet.force()*n);
        Vecteur vc(objet.vitesse() + vstar*n);
        if (F_n < 0)
        {
            objet.ajoute_force(-F_n*n);
        }

        if ( 7*mu*(1+m_alpha)*vstar >= 2*vc.norme() )
        {
            dv = lambda*vstar*n - 2*vc/7;
        }

        else
        {
            dv = lambda*vstar*( n - mu*vc.unitaire());
        }

        objet.set_vitesse(objet.vitesse() + dv) ;
    }
}

void Obstacle::ajoute_a(Systeme *syst)
{
    syst->ajoute(this);
}

double const& Obstacle::alpha() const
{
    return m_alpha;
}
