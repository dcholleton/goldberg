/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Ressort.cpp
 * @brief D�finition des m�thodes de la classe Ressort
 */
#include <iostream>
#include <vector>
#include <string>
#include <QString>
#include "include/Parseur.h"
#include <QFormLayout>
#include <QLabel>
#include "include/Ressort.h"
#include "include/Vecteur.h"
#include "include/Couleur.h"
#include "include/ObjetMobile.h"

using namespace std;

Ressort::Ressort(Vecteur const& position, Vecteur const& vitesse, double const& rayon, double const& masse_volumique, Vecteur const& force, double const& raideur, double const& frottement, Vecteur const& direction, Vecteur const& attache, Couleur const& color)
    : ObjetMobile(position, vitesse, rayon, masse_volumique, force, color), m_raideur(raideur), m_frottement(frottement), m_direction(direction.unitaire()), m_attache(attache)
{

}


Ressort::~Ressort()
{

}

double const& Ressort::raideur() const
{
    return m_raideur;
}

double const& Ressort::frottement() const
{
    return m_frottement;
}

Vecteur Ressort::position() const
{
    return m_attache + omega()[0]*m_direction;
}

Vecteur Ressort::vitesse() const
{
    return d_omega()[0]*m_direction;
}

void Ressort::set_vitesse(Vecteur const& new_vitesse)
{
    double vitesse(new_vitesse*m_direction);

    set_d_omega( {vitesse});
}

Vecteur const& Ressort::direction() const
{
    return m_direction;
}

void Ressort::affiche(ostream& out) const
{
    out << "un ressort : "                         << endl
        <<  masse()      << " # masse ressort"     << endl
        <<  m_raideur    << " # raideur ressort"   << endl
        <<  m_frottement << " # frottement ressort"<< endl
        <<  m_attache    << "# attache ressort"    << endl
        <<  m_direction  << "# direction ressort"  << endl
        <<  omega()      << "# Omega ressort"      << endl
        <<  d_omega()    << "# Omega' ressort"     << endl
        <<  evolution()  << "# evolution ressort"  << endl
        <<  position()   << "# position ressort"   << endl
        <<  force()      << "# force ressort"      << endl
        << endl;
}

Vecteur Ressort::evolution() const
{

    return { 1/masse()*( force() * m_direction - m_raideur * omega()[0] - m_frottement * d_omega()[0] )} ;
}

Vecteur const& Ressort::attache() const
{
    return m_attache;
}

void Ressort::ajoute_force(Vecteur const& df)
{
    Vecteur force((df*m_direction)*m_direction);
    ObjetMobile::ajoute_force(force);
}

string Ressort::type() const
{
    return "Ressort";
}
QFormLayout* Ressort::proprietesLayout() const
{
    QFormLayout* mon_layout = new QFormLayout;
    QLabel* obj_type = new QLabel(QString::fromStdString(type()));
    mon_layout->addRow("Objet : ", obj_type);
    QLabel* obj_raid = new QLabel(QString::number(raideur()));
    mon_layout->addRow("Raideur : ", obj_raid);
    QLabel* obj_masse = new QLabel(QString::number(masse()));
    mon_layout->addRow("Masse de la boule : ", obj_masse);
    QLabel* obj_rayon = new QLabel(QString::number(rayon()));
    mon_layout->addRow("Rayon de la boule : ", obj_rayon);
    QLabel* obj_masse_vol = new QLabel(QString::number(masse_volumique()));
    mon_layout->addRow("Masse volumique de la boule : ", obj_masse_vol);
    QLabel* obj_frott = new QLabel(QString::number(frottement()));
    mon_layout->addRow("Coefficient de frottement : ", obj_frott);
    QLabel* obj_attache = new QLabel(QString::number(attache()[0]) + "  " + QString::number(attache()[1]) + "  " + QString::number(attache()[2]));
    mon_layout->addRow("Point d'attache : ", obj_attache);
    QLabel* obj_position = new QLabel(QString::number(position()[0]) + "  " + QString::number(position()[1]) + "  " + QString::number(position()[2]));
    mon_layout->addRow("Position de la boule : ", obj_position);
    QLabel* obj_angle = new QLabel(QString::number(omega()[0]));
    mon_layout->addRow("Position selon direction : ", obj_angle);
    QLabel* obj_vitesse = new QLabel(QString::number(vitesse()[0]) + "  " + QString::number(vitesse()[1]) + "  " + QString::number(vitesse()[2]));
    mon_layout->addRow("Vitesse de la boule : ", obj_vitesse);
    QLabel* obj_vitesse_angu = new QLabel(QString::number(d_omega()[0]));
    mon_layout->addRow("Vitesse selon la direction : ", obj_vitesse_angu);
    QLabel* obj_accel_angu = new QLabel(QString::number(evolution()[0]));
    mon_layout->addRow("Acc�l�ration selon la direction : ", obj_accel_angu);
    QLabel* obj_force = new QLabel(QString::number(force()[0]) + "  " + QString::number(force()[1]) + "  " + QString::number(force()[2]));
    mon_layout->addRow("Forces subies par la boule : ", obj_force);
    QLabel* obj_dir = new QLabel(QString::number(direction()[0]) + "  " + QString::number(direction()[1]) + "  " + QString::number(direction()[2]));
    mon_layout->addRow("Direction : ", obj_dir);
    return mon_layout;
}

void Ressort::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::RESSORT + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::POSITION + "> "
        << omega() << " </" + Parseur::POSITION + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::VITESSE + "> "
        << d_omega() << " </" + Parseur::VITESSE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FORCE + "> "
        << force() << " </" + Parseur::FORCE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::DIRECTION + "> "
        << direction() << " </" + Parseur::DIRECTION + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ATTACHE + "> "
        << attache() << " </" + Parseur::ATTACHE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::RAYON + "> "
        << rayon() << " </" + Parseur::RAYON + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::MASSE_VOLUMIQUE + "> "
        << masse_volumique() << " </" + Parseur::MASSE_VOLUMIQUE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::RAIDEUR + "> "
        << raideur() << " </" + Parseur::RAIDEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FROTTEMENT + "> "
        << frottement() << " </" + Parseur::FROTTEMENT + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> "
        << m_couleur.rouge() << " " << m_couleur.vert() << " "<< m_couleur.bleu() << " "  << m_couleur.alpha() << " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::RESSORT + ">" << endl;

}
