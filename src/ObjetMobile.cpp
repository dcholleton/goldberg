/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file ObjetMobile.cpp
 * @brief D�finition des m�thodes de la classe ObjetMobile
 */
#include <iostream>
#include <cmath>
#include "include/Couleur.h"
#include "include/ObjetMobile.h"
#include "include/Vecteur.h"
#include "include/Systeme.h"


using namespace std;

ObjetMobile::ObjetMobile(Vecteur const& omega, Vecteur const& d_omega, double const& rayon, double const& masse_volumique, Vecteur const& force, Couleur const& color)
    :m_couleur(color), m_omega(omega), m_d_omega(d_omega), m_rayon(rayon), m_masse_volumique(masse_volumique), m_force(force)
{

}

ObjetMobile::ObjetMobile(size_t const& taille)
    : m_omega(taille), m_d_omega(taille), m_rayon(1), m_masse_volumique(1), m_force(taille)
{

}

ObjetMobile::~ObjetMobile()
{

}

double ObjetMobile::distance(ObjetMobile const& objet) const
{
    return ( position() - objet.position() ).norme() - m_rayon - objet.rayon();
}

void ObjetMobile::agit_sur(ObjetMobile& objet)
{
	// Impl�mant� selon le compl�ment math�matique
    if (distance(objet) <= 0)
    {
        Vecteur n(( objet.position() - position() ).unitaire() ),  dv(3);
        double vstar((vitesse() - objet.vitesse())*n), alpha(0.8), mu(0.01), lambda((1+alpha)*(masse()/(objet.masse()+masse()))), F_n_1(objet.force()*n), F_n_2(m_force*n);
        Vecteur vc( objet.vitesse() - vitesse() + vstar*n);

        if (F_n_1 < 0)
        {
            objet.ajoute_force(-F_n_1*n);
            ajoute_force(F_n_1*n);
        }
        if (F_n_2 > 0)
        {
            objet.ajoute_force(F_n_2*n);
            ajoute_force(-F_n_2*n);
        }
        if ( 7*mu*(1+alpha)*vstar >= 2*vc.norme() )
        {
            dv = lambda*vstar*n - 2/7*masse()/(masse()+objet.masse())*vc;
        }

        else
        {
            dv = lambda*vstar*( n - mu*vc.unitaire());
        }

        objet.set_vitesse(objet.vitesse() + dv );
        set_vitesse(vitesse() - objet.masse()/masse()*dv);
    }
}

void ObjetMobile::ajoute_a(Systeme *syst)
{
    syst->ajoute(this);
}


Vecteur const& ObjetMobile::omega() const
{
    return m_omega;
}

Vecteur const& ObjetMobile::d_omega() const
{
    return m_d_omega;
}

void ObjetMobile::set_omega(Vecteur const& new_omega)
{
    m_omega = new_omega;
}

void ObjetMobile::set_d_omega(Vecteur const& new_d_omega)
{
    m_d_omega = new_d_omega;
}

Vecteur const& ObjetMobile::force() const
{
    return m_force;
}

double const& ObjetMobile::rayon() const
{
    return m_rayon;
}

double const& ObjetMobile::masse_volumique() const
{
    return m_masse_volumique;
}

double ObjetMobile::masse() const
{
    return 4*M_PI*m_rayon*m_rayon*m_rayon/3*m_masse_volumique;
}

void ObjetMobile::ajoute_force(Vecteur const& df)
{
    m_force += df;
}

void ObjetMobile::reset_force()
{
    m_force = Vecteur(3);
}
