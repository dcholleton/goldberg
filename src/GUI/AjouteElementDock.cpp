/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file AjouteElementDock.cpp
 * @brief D�finition des m�thodes de la classe AjouteElementDock
 */

#include <QWidget>
#include <QLayout>
#include <QColorDialog>
#include "include/GUI/AjouteElementDock.h"
#include "include/Systeme.h"
#include "include/GUI/gui.h"
#include "include/Balle3d.h"
#include "include/Pendule3d.h"
#include "include/Ressort3d.h"
#include "include/PlanFini3d.h"
#include "include/Plan3d.h"
#include "include/Ventilateur3d.h"
#include "include/ChampForcesGlobal.h"
#include "include/Brique3d.h"
#include "include/Vent.h"
#include "include/Trampoline3d.h"

using namespace std;

AjouteElementDock::AjouteElementDock(Systeme* systeme, GUI* simulation, char* name, QWidget* parent)
    :QDockWidget(name, parent), m_systeme(systeme), m_simulation(simulation)
{
    connect(m_simulation, SIGNAL(simulationStarted(bool)), this, SLOT(setDisabled(bool)));
    connect(m_simulation, SIGNAL(simulationStopped(bool)), this, SLOT(setEnabled(bool)));
    setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);
    QWidget* contenuAjoutEl = new QWidget;
    setWindowOpacity(0.7);
    setWidget(contenuAjoutEl);
    QVBoxLayout* LayoutAjoutEl = new QVBoxLayout;

    // QGroupBox de choix d'Element
    QWidget* addElem1 = new QWidget;
    addElem1->setObjectName("addElem1");
    QVBoxLayout* addElem1Layout = new QVBoxLayout;
    addElem1->setLayout(addElem1Layout);

    // QGroupBox de choix d'ObjetMobile
    QGroupBox* addObj = new QGroupBox("Objets Mobiles");
    addElem1Layout->addWidget(addObj);
    QGridLayout* addObjLayout = new QGridLayout;


    QPushButton* addBalle = new QPushButton("Balle");
    connect(addBalle, SIGNAL(clicked(bool)), addElem1, SLOT(hide()));
    addObjLayout->addWidget(addBalle);

    // Cr�ation de Balle ... Ainsi de suite pour tout les QGroupBox. 
    m_CreationBalle = new QGroupBox;
    m_CreationBalle->hide();
    LayoutAjoutEl->addWidget(m_CreationBalle);
    connect(addBalle, SIGNAL(clicked(bool)), this, SLOT(genereCreationBalle()));

    QPushButton* addPendule = new QPushButton("Pendule");
    connect(addPendule, SIGNAL(clicked(bool)), addElem1, SLOT(hide()));
    addObjLayout->addWidget(addPendule);

    m_CreationPendule = new QGroupBox;
    m_CreationPendule->hide();
    LayoutAjoutEl->addWidget(m_CreationPendule);
    connect(addPendule, SIGNAL(clicked(bool)), this, SLOT(genereCreationPendule()));

    QPushButton* addRessort = new QPushButton("Ressort");
    connect(addRessort, SIGNAL(clicked(bool)), addElem1, SLOT(hide()));
    addObjLayout->addWidget(addRessort);

    m_CreationRessort = new QGroupBox;
    m_CreationRessort->hide();
    LayoutAjoutEl->addWidget(m_CreationRessort);
    connect(addRessort, SIGNAL(clicked(bool)), this, SLOT(genereCreationRessort()));

    addObj->setLayout(addObjLayout);


	// QGroupBox de choix d'Obstacle
    QGroupBox* addObst = new QGroupBox("Obstacles");
    addElem1Layout->addWidget(addObst);
    QGridLayout* addObstLayout = new QGridLayout;

    QPushButton* addPlan = new QPushButton("Plan infini");
    connect(addPlan, SIGNAL(clicked(bool)), addElem1, SLOT(hide()));
    addObstLayout->addWidget(addPlan);

    m_CreationPlan = new QGroupBox;
    m_CreationPlan->hide();
    LayoutAjoutEl->addWidget(m_CreationPlan);
    connect(addPlan, SIGNAL(clicked(bool)), this, SLOT(genereCreationPlan()));

    QPushButton* addPlanFini = new QPushButton("Plan fini");
    connect(addPlanFini, SIGNAL(clicked(bool)), addElem1, SLOT(hide()));
    addObstLayout->addWidget(addPlanFini);

    m_CreationPlanFini = new QGroupBox;
    m_CreationPlanFini->hide();
    LayoutAjoutEl->addWidget(m_CreationPlanFini);
    connect(addPlanFini, SIGNAL(clicked(bool)), this, SLOT(genereCreationPlanFini()));

    QPushButton* addBrique = new QPushButton("Brique");
    connect(addBrique, SIGNAL(clicked(bool)), addElem1, SLOT(hide()));
    addObstLayout->addWidget(addBrique);

    m_CreationBrique = new QGroupBox;
    m_CreationBrique->hide();
    LayoutAjoutEl->addWidget(m_CreationBrique);
    connect(addBrique, SIGNAL(clicked(bool)), this, SLOT(genereCreationBrique()));

    addObst->setLayout(addObstLayout);

	// QGroupBox de choix de ChampForces
    QGroupBox* addChamps = new QGroupBox("Champs de Forces");
    addElem1Layout->addWidget(addChamps);
    QGridLayout* addChampsLayout = new QGridLayout;


    QPushButton* addChampForcesGlobal = new QPushButton("Champ de Forces Global");
    connect(addChampForcesGlobal, SIGNAL(clicked(bool)), addElem1, SLOT(hide()));
    addChampsLayout->addWidget(addChampForcesGlobal);

    m_CreationChampForcesGlobal = new QGroupBox;
    m_CreationChampForcesGlobal->hide();
    LayoutAjoutEl->addWidget(m_CreationChampForcesGlobal);
    connect(addChampForcesGlobal, SIGNAL(clicked(bool)), this, SLOT(genereCreationChampForcesGlobal()));

    QPushButton* addVent = new QPushButton("Vent");
    connect(addVent, SIGNAL(clicked(bool)), addElem1, SLOT(hide()));
    addChampsLayout->addWidget(addVent);

    m_CreationVent = new QGroupBox;
    m_CreationVent->hide();
    LayoutAjoutEl->addWidget(m_CreationVent);
    connect(addVent, SIGNAL(clicked(bool)), this, SLOT(genereCreationVent()));

    addChamps->setLayout(addChampsLayout);

	// QGroupBox de choix d'ObjetCompose
    QGroupBox* addObjetCompose = new QGroupBox("Objets Compos�s");
    addElem1Layout->addWidget(addObjetCompose);
    QGridLayout* addObjetComposeLayout = new QGridLayout;

    QPushButton* addVentilateur = new QPushButton("Ventilateur");
    connect(addVentilateur, SIGNAL(clicked(bool)), addElem1, SLOT(hide()));
    addObjetComposeLayout->addWidget(addVentilateur);

    m_CreationVentilateur = new QGroupBox;
    m_CreationVentilateur->hide();
    LayoutAjoutEl->addWidget(m_CreationVentilateur);
    connect(addVentilateur, SIGNAL(clicked(bool)), this, SLOT(genereCreationVentilateur()));


    QPushButton* addTrampoline = new QPushButton("Trampoline");
    connect(addTrampoline, SIGNAL(clicked(bool)), addElem1, SLOT(hide()));
    addObjetComposeLayout->addWidget(addTrampoline);

    m_CreationTrampoline = new QGroupBox;
    m_CreationTrampoline->hide();
    LayoutAjoutEl->addWidget(m_CreationTrampoline);
    connect(addTrampoline, SIGNAL(clicked(bool)), this, SLOT(genereCreationTrampoline()));




    addObjetCompose->setLayout(addObjetComposeLayout);

    LayoutAjoutEl->addWidget(addElem1);

    contenuAjoutEl->setLayout(LayoutAjoutEl);

}

void AjouteElementDock::genereCreationBalle()
{

    QLayout* par = m_CreationBalle->parentWidget()->layout();
    delete m_CreationBalle;

    m_CreationBalle = new QGroupBox("Cr�ation d'une Balle");

    par->addWidget(m_CreationBalle);

    QFormLayout* CreationBalleLayout = new QFormLayout;

	// On va cr�er toutes les QDoubleSpinBox n�cessaire pour r�cup�r� tout ce dont a besoin le constructeur. Idem pour toutes les m�thodes genereCreation
    QWidget* CreationBallePosition = new QWidget;
    QHBoxLayout* CreationBallePositionLayout = new QHBoxLayout;
    CreationBallePosition->setLayout(CreationBallePositionLayout);

    QDoubleSpinBox* CreationBallePositionX = new QDoubleSpinBox;
    CreationBallePositionX->setObjectName("CreationBallePositionX");
    CreationBallePositionX->setValue(0);
    CreationBallePositionX->setDecimals(4);
    CreationBallePositionX->setMaximum(1000);
    CreationBallePositionX->setMinimum(-1000);
    CreationBallePositionX->setSingleStep(0.05);
    CreationBallePositionX->setAccelerated(true);
    CreationBallePositionLayout->addWidget(CreationBallePositionX);

    QDoubleSpinBox* CreationBallePositionY = new QDoubleSpinBox;
    CreationBallePositionY->setObjectName("CreationBallePositionY");
    CreationBallePositionY->setValue(0);
    CreationBallePositionY->setMaximum(1000);
    CreationBallePositionY->setMinimum(-1000);
    CreationBallePositionY->setDecimals(4);
    CreationBallePositionY->setSingleStep(0.05);
    CreationBallePositionY->setAccelerated(true);
    CreationBallePositionLayout->addWidget(CreationBallePositionY);

    QDoubleSpinBox* CreationBallePositionZ = new QDoubleSpinBox;
    CreationBallePositionZ->setObjectName("CreationBallePositionZ");
    CreationBallePositionZ->setValue(0);
    CreationBallePositionZ->setDecimals(4);
    CreationBallePositionZ->setMaximum(1000);
    CreationBallePositionZ->setMinimum(-1000);
    CreationBallePositionZ->setSingleStep(0.05);
    CreationBallePositionZ->setAccelerated(true);
    CreationBallePositionLayout->addWidget(CreationBallePositionZ);

    CreationBalleLayout->addRow("Position : ", CreationBallePosition);

    QWidget* CreationBalleVitesse = new QWidget;
    QHBoxLayout* CreationBalleVitesseLayout = new QHBoxLayout;
    CreationBalleVitesse->setLayout(CreationBalleVitesseLayout);

    QDoubleSpinBox* CreationBalleVitesseX = new QDoubleSpinBox;
    CreationBalleVitesseX->setObjectName("CreationBalleVitesseX");
    CreationBalleVitesseX->setValue(0);
    CreationBalleVitesseX->setDecimals(4);
    CreationBalleVitesseX->setMaximum(1000);
    CreationBalleVitesseX->setMinimum(-1000);
    CreationBalleVitesseX->setSingleStep(0.05);
    CreationBalleVitesseX->setAccelerated(true);
    CreationBalleVitesseLayout->addWidget(CreationBalleVitesseX);

    QDoubleSpinBox* CreationBalleVitesseY = new QDoubleSpinBox;
    CreationBalleVitesseY->setObjectName("CreationBalleVitesseY");
    CreationBalleVitesseY->setValue(0);
    CreationBalleVitesseY->setDecimals(4);
    CreationBalleVitesseY->setMaximum(1000);
    CreationBalleVitesseY->setMinimum(-1000);
    CreationBalleVitesseY->setSingleStep(0.05);
    CreationBalleVitesseY->setAccelerated(true);
    CreationBalleVitesseLayout->addWidget(CreationBalleVitesseY);

    QDoubleSpinBox* CreationBalleVitesseZ = new QDoubleSpinBox;
    CreationBalleVitesseZ->setObjectName("CreationBalleVitesseZ");
    CreationBalleVitesseZ->setValue(0);
    CreationBalleVitesseZ->setMaximum(1000);
    CreationBalleVitesseZ->setMinimum(-1000);
    CreationBalleVitesseZ->setDecimals(4);
    CreationBalleVitesseZ->setSingleStep(0.05);
    CreationBalleVitesseZ->setAccelerated(true);
    CreationBalleVitesseLayout->addWidget(CreationBalleVitesseZ);

    CreationBalleLayout->addRow("Vitesse : ", CreationBalleVitesse);

    QDoubleSpinBox* CreationBalleRayon = new QDoubleSpinBox;
    CreationBalleRayon->setObjectName("CreationBalleRayon");
    CreationBalleRayon->setValue(0.1);
    CreationBalleRayon->setMaximum(1000);
    CreationBalleRayon->setMinimum(0);
    CreationBalleRayon->setDecimals(4);
    CreationBalleRayon->setSingleStep(0.05);
    CreationBalleRayon->setAccelerated(true);
    CreationBalleLayout->addRow("Rayon : ", CreationBalleRayon);

    QDoubleSpinBox* CreationBalleMasseVolumique = new QDoubleSpinBox;
    CreationBalleMasseVolumique->setObjectName("CreationBalleMasseVolumique");
    CreationBalleMasseVolumique->setValue(3);
    CreationBalleMasseVolumique->setMaximum(1000);
    CreationBalleMasseVolumique->setMinimum(0);
    CreationBalleMasseVolumique->setDecimals(4);
    CreationBalleMasseVolumique->setSingleStep(0.05);
    CreationBalleMasseVolumique->setAccelerated(true);
    CreationBalleLayout->addRow("Masse Volumique : ", CreationBalleMasseVolumique);

    QPushButton* CreationBalleAfficherCouleur = new QPushButton("Choisir la couleur");
    CreationBalleLayout->addRow(CreationBalleAfficherCouleur);

    QColorDialog* CreationBalleCouleur = new QColorDialog(Qt::blue, m_CreationBalle);
    CreationBalleCouleur->hide();
    CreationBalleCouleur->setObjectName("CreationBalleCouleur");
    connect(CreationBalleAfficherCouleur, SIGNAL(clicked()), CreationBalleCouleur, SLOT(show()));

    QPushButton* CreationBalleButton = new QPushButton("Cr�er la Balle !");
    CreationBalleLayout->addRow(CreationBalleButton);

    QPushButton* CreationBalleAnnuler = new QPushButton("Annuler");
    CreationBalleLayout->addRow(CreationBalleAnnuler);

    m_CreationBalle->setLayout(CreationBalleLayout);

    connect(CreationBalleButton, SIGNAL(clicked()), m_CreationBalle, SLOT(hide()));
    connect(CreationBalleButton, SIGNAL(clicked()), m_CreationBalle->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));

    connect(CreationBalleAnnuler, SIGNAL(clicked()), m_CreationBalle, SLOT(hide()));
    connect(CreationBalleAnnuler, SIGNAL(clicked()), m_CreationBalle->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));


    connect(CreationBalleButton, SIGNAL(clicked()), this, SLOT(creeBalle()));

}

void AjouteElementDock::genereCreationPendule()
{
    QLayout* par = m_CreationPendule->parentWidget()->layout();
    delete m_CreationPendule;

    m_CreationPendule = new QGroupBox("Cr�ation d'un Pendule");

    par->addWidget(m_CreationPendule);

    QFormLayout* CreationPenduleLayout = new QFormLayout;


    QWidget* CreationPenduleAttache = new QWidget;
    QHBoxLayout* CreationPenduleAttacheLayout = new QHBoxLayout;
    CreationPenduleAttache->setLayout(CreationPenduleAttacheLayout);

    QDoubleSpinBox* CreationPenduleAttacheX = new QDoubleSpinBox;
    CreationPenduleAttacheX->setObjectName("CreationPenduleAttacheX");
    CreationPenduleAttacheX->setValue(0);
    CreationPenduleAttacheX->setDecimals(4);
    CreationPenduleAttacheX->setMaximum(1000);
    CreationPenduleAttacheX->setMinimum(-1000);
    CreationPenduleAttacheX->setSingleStep(0.05);
    CreationPenduleAttacheX->setAccelerated(true);
    CreationPenduleAttacheLayout->addWidget(CreationPenduleAttacheX);

    QDoubleSpinBox* CreationPenduleAttacheY = new QDoubleSpinBox;
    CreationPenduleAttacheY->setObjectName("CreationPenduleAttacheY");
    CreationPenduleAttacheY->setValue(0);
    CreationPenduleAttacheY->setDecimals(4);
    CreationPenduleAttacheY->setMaximum(1000);
    CreationPenduleAttacheY->setMinimum(-1000);
    CreationPenduleAttacheY->setSingleStep(0.05);
    CreationPenduleAttacheY->setAccelerated(true);
    CreationPenduleAttacheLayout->addWidget(CreationPenduleAttacheY);

    QDoubleSpinBox* CreationPenduleAttacheZ = new QDoubleSpinBox;
    CreationPenduleAttacheZ->setObjectName("CreationPenduleAttacheZ");
    CreationPenduleAttacheZ->setValue(0);
    CreationPenduleAttacheZ->setDecimals(4);
    CreationPenduleAttacheZ->setMaximum(1000);
    CreationPenduleAttacheZ->setMinimum(-1000);
    CreationPenduleAttacheZ->setSingleStep(0.05);
    CreationPenduleAttacheZ->setAccelerated(true);
    CreationPenduleAttacheLayout->addWidget(CreationPenduleAttacheZ);

    CreationPenduleLayout->addRow("Point d'attache : ", CreationPenduleAttache);

    QDoubleSpinBox* CreationPenduleAngle = new QDoubleSpinBox;
    CreationPenduleAngle->setObjectName("CreationPenduleAngle");
    CreationPenduleAngle->setValue(0);
    CreationPenduleAngle->setMaximum(1000);
    CreationPenduleAngle->setMinimum(-1000);
    CreationPenduleAngle->setDecimals(4);
    CreationPenduleAngle->setSingleStep(0.05);
    CreationPenduleAngle->setAccelerated(true);
    CreationPenduleLayout->addRow("Angle : ", CreationPenduleAngle);

    QDoubleSpinBox* CreationPenduleVitesseAngulaire = new QDoubleSpinBox;
    CreationPenduleVitesseAngulaire->setObjectName("CreationPenduleVitesseAngulaire");
    CreationPenduleVitesseAngulaire->setValue(0);
    CreationPenduleVitesseAngulaire->setMaximum(1000);
    CreationPenduleVitesseAngulaire->setMinimum(-1000);
    CreationPenduleVitesseAngulaire->setDecimals(4);
    CreationPenduleVitesseAngulaire->setSingleStep(0.05);
    CreationPenduleVitesseAngulaire->setAccelerated(true);
    CreationPenduleLayout->addRow("Vitesse angulaire : ", CreationPenduleVitesseAngulaire);

    QDoubleSpinBox* CreationPenduleLongueur = new QDoubleSpinBox;
    CreationPenduleLongueur->setObjectName("CreationPenduleLongueur");
    CreationPenduleLongueur->setValue(0.5);
    CreationPenduleLongueur->setMaximum(1000);
    CreationPenduleLongueur->setMinimum(0);
    CreationPenduleLongueur->setDecimals(4);
    CreationPenduleLongueur->setSingleStep(0.05);
    CreationPenduleLongueur->setAccelerated(true);
    CreationPenduleLayout->addRow("Longueur : ", CreationPenduleLongueur);

    QDoubleSpinBox* CreationPenduleRayon = new QDoubleSpinBox;
    CreationPenduleRayon->setObjectName("CreationPenduleRayon");
    CreationPenduleRayon->setValue(0.1);
    CreationPenduleRayon->setMaximum(1000);
    CreationPenduleRayon->setMinimum(0);
    CreationPenduleRayon->setDecimals(4);
    CreationPenduleRayon->setSingleStep(0.05);
    CreationPenduleRayon->setAccelerated(true);
    CreationPenduleLayout->addRow("Rayon de la Boule : ", CreationPenduleRayon);

    QDoubleSpinBox* CreationPenduleMasseVolumique = new QDoubleSpinBox;
    CreationPenduleMasseVolumique->setObjectName("CreationPenduleMasseVolumique");
    CreationPenduleMasseVolumique->setMaximum(1000);
    CreationPenduleMasseVolumique->setMinimum(0);
    CreationPenduleMasseVolumique->setValue(200);
    CreationPenduleMasseVolumique->setDecimals(4);
    CreationPenduleMasseVolumique->setSingleStep(0.05);
    CreationPenduleMasseVolumique->setAccelerated(true);
    CreationPenduleLayout->addRow("Masse Volumique de la Boule : ", CreationPenduleMasseVolumique);

    QDoubleSpinBox* CreationPenduleFrottement = new QDoubleSpinBox;
    CreationPenduleFrottement->setObjectName("CreationPenduleFrottement");
    CreationPenduleFrottement->setValue(0.05);
    CreationPenduleFrottement->setMaximum(1000);
    CreationPenduleFrottement->setMinimum(0);
    CreationPenduleFrottement->setDecimals(4);
    CreationPenduleFrottement->setSingleStep(0.05);
    CreationPenduleFrottement->setAccelerated(true);
    CreationPenduleLayout->addRow("Coefficient de frottement : ", CreationPenduleFrottement);

    QWidget* CreationPenduleNormale = new QWidget;
    QHBoxLayout* CreationPenduleNormaleLayout = new QHBoxLayout;
    CreationPenduleNormale->setLayout(CreationPenduleNormaleLayout);

    QDoubleSpinBox* CreationPenduleNormaleX = new QDoubleSpinBox;
    CreationPenduleNormaleX->setObjectName("CreationPenduleNormaleX");
    CreationPenduleNormaleX->setValue(0);
    CreationPenduleNormaleX->setDecimals(4);
    CreationPenduleNormaleX->setMaximum(1000);
    CreationPenduleNormaleX->setMinimum(-1000);
    CreationPenduleNormaleX->setSingleStep(0.05);
    CreationPenduleNormaleX->setAccelerated(true);
    CreationPenduleNormaleLayout->addWidget(CreationPenduleNormaleX);

    QDoubleSpinBox* CreationPenduleNormaleY = new QDoubleSpinBox;
    CreationPenduleNormaleY->setObjectName("CreationPenduleNormaleY");
    CreationPenduleNormaleY->setValue(1);
    CreationPenduleNormaleY->setDecimals(4);
    CreationPenduleNormaleY->setMaximum(1000);
    CreationPenduleNormaleY->setMinimum(-1000);
    CreationPenduleNormaleY->setSingleStep(0.05);
    CreationPenduleNormaleY->setAccelerated(true);
    CreationPenduleNormaleLayout->addWidget(CreationPenduleNormaleY);

    QDoubleSpinBox* CreationPenduleNormaleZ = new QDoubleSpinBox;
    CreationPenduleNormaleZ->setObjectName("CreationPenduleNormaleZ");
    CreationPenduleNormaleZ->setValue(0);
    CreationPenduleNormaleZ->setDecimals(4);
    CreationPenduleNormaleZ->setMaximum(1000);
    CreationPenduleNormaleZ->setMinimum(-1000);
    CreationPenduleNormaleZ->setSingleStep(0.05);
    CreationPenduleNormaleZ->setAccelerated(true);
    CreationPenduleNormaleLayout->addWidget(CreationPenduleNormaleZ);

    CreationPenduleLayout->addRow("Direction : ", CreationPenduleNormale);

    QPushButton* CreationPenduleAfficherCouleur = new QPushButton("Choisir la couleur");
    CreationPenduleLayout->addRow(CreationPenduleAfficherCouleur);

    QColorDialog* CreationPenduleCouleur = new QColorDialog(Qt::red, m_CreationPendule);
    CreationPenduleCouleur->hide();
    CreationPenduleCouleur->setObjectName("CreationPenduleCouleur");
    connect(CreationPenduleAfficherCouleur, SIGNAL(clicked()), CreationPenduleCouleur, SLOT(show()));

    QPushButton* CreationPenduleButton = new QPushButton("Cr�er le Pendule !");
    CreationPenduleLayout->addRow(CreationPenduleButton);

    QPushButton* CreationPenduleAnnuler = new QPushButton("Annuler");
    CreationPenduleLayout->addRow(CreationPenduleAnnuler);


    m_CreationPendule->setLayout(CreationPenduleLayout);

    connect(CreationPenduleButton, SIGNAL(clicked()), m_CreationPendule, SLOT(hide()));
    connect(CreationPenduleButton, SIGNAL(clicked()), m_CreationPendule->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));

    connect(CreationPenduleAnnuler, SIGNAL(clicked()), m_CreationPendule, SLOT(hide()));
    connect(CreationPenduleAnnuler, SIGNAL(clicked()), m_CreationPendule->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));



    connect(CreationPenduleButton, SIGNAL(clicked()), this, SLOT(creePendule()));
}

void AjouteElementDock::genereCreationRessort()
{
    QLayout* par = m_CreationRessort->parentWidget()->layout();
    delete m_CreationRessort;

    m_CreationRessort = new QGroupBox("Cr�ation d'un Ressort");

    par->addWidget(m_CreationRessort);

    QFormLayout* CreationRessortLayout = new QFormLayout;


    QWidget* CreationRessortAttache = new QWidget;
    QHBoxLayout* CreationRessortAttacheLayout = new QHBoxLayout;
    CreationRessortAttache->setLayout(CreationRessortAttacheLayout);

    QDoubleSpinBox* CreationRessortAttacheX = new QDoubleSpinBox;
    CreationRessortAttacheX->setObjectName("CreationRessortAttacheX");
    CreationRessortAttacheX->setValue(0);
    CreationRessortAttacheX->setDecimals(4);
    CreationRessortAttacheX->setMaximum(1000);
    CreationRessortAttacheX->setMinimum(-1000);
    CreationRessortAttacheX->setSingleStep(0.05);
    CreationRessortAttacheX->setAccelerated(true);
    CreationRessortAttacheLayout->addWidget(CreationRessortAttacheX);

    QDoubleSpinBox* CreationRessortAttacheY = new QDoubleSpinBox;
    CreationRessortAttacheY->setObjectName("CreationRessortAttacheY");
    CreationRessortAttacheY->setValue(0);
    CreationRessortAttacheY->setDecimals(4);
    CreationRessortAttacheY->setMaximum(1000);
    CreationRessortAttacheY->setMinimum(-1000);
    CreationRessortAttacheY->setSingleStep(0.05);
    CreationRessortAttacheY->setAccelerated(true);
    CreationRessortAttacheLayout->addWidget(CreationRessortAttacheY);

    QDoubleSpinBox* CreationRessortAttacheZ = new QDoubleSpinBox;
    CreationRessortAttacheZ->setObjectName("CreationRessortAttacheZ");
    CreationRessortAttacheZ->setValue(0);
    CreationRessortAttacheZ->setDecimals(4);
    CreationRessortAttacheZ->setMaximum(1000);
    CreationRessortAttacheZ->setMinimum(-1000);
    CreationRessortAttacheZ->setSingleStep(0.05);
    CreationRessortAttacheZ->setAccelerated(true);
    CreationRessortAttacheLayout->addWidget(CreationRessortAttacheZ);

    CreationRessortLayout->addRow("Point d'attache : ", CreationRessortAttache);

    QDoubleSpinBox* CreationRessortOmega = new QDoubleSpinBox;
    CreationRessortOmega->setObjectName("CreationRessortOmega");
    CreationRessortOmega->setValue(0);
    CreationRessortOmega->setMaximum(1000);
    CreationRessortOmega->setMinimum(-1000);
    CreationRessortOmega->setDecimals(4);
    CreationRessortOmega->setSingleStep(0.05);
    CreationRessortOmega->setAccelerated(true);
    CreationRessortLayout->addRow("Position selon la direction : ", CreationRessortOmega);

    QDoubleSpinBox* CreationRessortOmegaPoint = new QDoubleSpinBox;
    CreationRessortOmegaPoint->setObjectName("CreationRessortOmegaPoint");
    CreationRessortOmegaPoint->setValue(0);
    CreationRessortOmegaPoint->setMaximum(1000);
    CreationRessortOmegaPoint->setMinimum(-1000);
    CreationRessortOmegaPoint->setDecimals(4);
    CreationRessortOmegaPoint->setSingleStep(0.05);
    CreationRessortOmegaPoint->setAccelerated(true);
    CreationRessortLayout->addRow("Vitesse selon la direction : ", CreationRessortOmegaPoint);

    QDoubleSpinBox* CreationRessortRaideur = new QDoubleSpinBox;
    CreationRessortRaideur->setObjectName("CreationRessortRaideur");
    CreationRessortRaideur->setValue(10);
    CreationRessortRaideur->setMaximum(1000);
    CreationRessortRaideur->setMinimum(0);
    CreationRessortRaideur->setDecimals(4);
    CreationRessortRaideur->setSingleStep(0.05);
    CreationRessortRaideur->setAccelerated(true);
    CreationRessortLayout->addRow("Raideur : ", CreationRessortRaideur);

    QDoubleSpinBox* CreationRessortRayon = new QDoubleSpinBox;
    CreationRessortRayon->setObjectName("CreationRessortRayon");
    CreationRessortRayon->setValue(0.1);
    CreationRessortRayon->setMaximum(1000);
    CreationRessortRayon->setMinimum(0);
    CreationRessortRayon->setDecimals(4);
    CreationRessortRayon->setSingleStep(0.05);
    CreationRessortRayon->setAccelerated(true);
    CreationRessortLayout->addRow("Rayon de la Boule : ", CreationRessortRayon);

    QDoubleSpinBox* CreationRessortMasseVolumique = new QDoubleSpinBox;
    CreationRessortMasseVolumique->setObjectName("CreationRessortMasseVolumique");
    CreationRessortMasseVolumique->setMaximum(1000);
    CreationRessortMasseVolumique->setMinimum(0);
    CreationRessortMasseVolumique->setValue(200);
    CreationRessortMasseVolumique->setDecimals(4);
    CreationRessortMasseVolumique->setSingleStep(0.05);
    CreationRessortMasseVolumique->setAccelerated(true);
    CreationRessortLayout->addRow("Masse Volumique de la Boule : ", CreationRessortMasseVolumique);

    QDoubleSpinBox* CreationRessortFrottement = new QDoubleSpinBox;
    CreationRessortFrottement->setObjectName("CreationRessortFrottement");
    CreationRessortFrottement->setValue(0.05);
    CreationRessortFrottement->setMaximum(1000);
    CreationRessortFrottement->setMinimum(0);
    CreationRessortFrottement->setDecimals(4);
    CreationRessortFrottement->setSingleStep(0.05);
    CreationRessortFrottement->setAccelerated(true);
    CreationRessortLayout->addRow("Coefficient de frottement : ", CreationRessortFrottement);

    QWidget* CreationRessortDirection = new QWidget;
    QHBoxLayout* CreationRessortDirectionLayout = new QHBoxLayout;
    CreationRessortDirection->setLayout(CreationRessortDirectionLayout);

    QDoubleSpinBox* CreationRessortDirectionX = new QDoubleSpinBox;
    CreationRessortDirectionX->setObjectName("CreationRessortDirectionX");
    CreationRessortDirectionX->setValue(0);
    CreationRessortDirectionX->setDecimals(4);
    CreationRessortDirectionX->setMaximum(1000);
    CreationRessortDirectionX->setMinimum(-1000);
    CreationRessortDirectionX->setSingleStep(0.05);
    CreationRessortDirectionX->setAccelerated(true);
    CreationRessortDirectionLayout->addWidget(CreationRessortDirectionX);

    QDoubleSpinBox* CreationRessortDirectionY = new QDoubleSpinBox;
    CreationRessortDirectionY->setObjectName("CreationRessortDirectionY");
    CreationRessortDirectionY->setValue(1);
    CreationRessortDirectionY->setDecimals(4);
    CreationRessortDirectionY->setMaximum(1000);
    CreationRessortDirectionY->setMinimum(-1000);
    CreationRessortDirectionY->setSingleStep(0.05);
    CreationRessortDirectionY->setAccelerated(true);
    CreationRessortDirectionLayout->addWidget(CreationRessortDirectionY);

    QDoubleSpinBox* CreationRessortDirectionZ = new QDoubleSpinBox;
    CreationRessortDirectionZ->setObjectName("CreationRessortDirectionZ");
    CreationRessortDirectionZ->setValue(0);
    CreationRessortDirectionZ->setDecimals(4);
    CreationRessortDirectionZ->setMaximum(1000);
    CreationRessortDirectionZ->setMinimum(-1000);
    CreationRessortDirectionZ->setSingleStep(0.05);
    CreationRessortDirectionZ->setAccelerated(true);
    CreationRessortDirectionLayout->addWidget(CreationRessortDirectionZ);

    CreationRessortLayout->addRow("Vecteur sur le plan du mouvement : ", CreationRessortDirection);

    QPushButton* CreationRessortAfficherCouleur = new QPushButton("Choisir la couleur");
    CreationRessortLayout->addRow(CreationRessortAfficherCouleur);

    QColorDialog* CreationRessortCouleur = new QColorDialog(Qt::green, m_CreationRessort);
    CreationRessortCouleur->hide();
    CreationRessortCouleur->setObjectName("CreationRessortCouleur");
    connect(CreationRessortAfficherCouleur, SIGNAL(clicked()), CreationRessortCouleur, SLOT(show()));

    QPushButton* CreationRessortButton = new QPushButton("Cr�er le Ressort !");
    CreationRessortLayout->addRow(CreationRessortButton);

    QPushButton* CreationRessortAnnuler = new QPushButton("Annuler");
    CreationRessortLayout->addRow(CreationRessortAnnuler);


    m_CreationRessort->setLayout(CreationRessortLayout);

    connect(CreationRessortButton, SIGNAL(clicked()), m_CreationRessort, SLOT(hide()));
    connect(CreationRessortButton, SIGNAL(clicked()), m_CreationRessort->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));

    connect(CreationRessortAnnuler, SIGNAL(clicked()), m_CreationRessort, SLOT(hide()));
    connect(CreationRessortAnnuler, SIGNAL(clicked()), m_CreationRessort->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));



    connect(CreationRessortButton, SIGNAL(clicked()), this, SLOT(creeRessort()));
}

void AjouteElementDock::genereCreationPlan()
{

    QLayout* par = m_CreationPlan->parentWidget()->layout();
    delete m_CreationPlan;

    m_CreationPlan = new QGroupBox("Cr�ation d'un Plan Infini");

    par->addWidget(m_CreationPlan);

    QFormLayout* CreationPlanLayout = new QFormLayout;


    QWidget* CreationPlanOrigine = new QWidget;
    QHBoxLayout* CreationPlanOrigineLayout = new QHBoxLayout;
    CreationPlanOrigine->setLayout(CreationPlanOrigineLayout);

    QDoubleSpinBox* CreationPlanOrigineX = new QDoubleSpinBox;
    CreationPlanOrigineX->setObjectName("CreationPlanOrigineX");
    CreationPlanOrigineX->setValue(0);
    CreationPlanOrigineX->setDecimals(4);
    CreationPlanOrigineX->setMaximum(1000);
    CreationPlanOrigineX->setMinimum(-1000);
    CreationPlanOrigineX->setSingleStep(0.05);
    CreationPlanOrigineX->setAccelerated(true);
    CreationPlanOrigineLayout->addWidget(CreationPlanOrigineX);

    QDoubleSpinBox* CreationPlanOrigineY = new QDoubleSpinBox;
    CreationPlanOrigineY->setObjectName("CreationPlanOrigineY");
    CreationPlanOrigineY->setValue(0);
    CreationPlanOrigineY->setMaximum(1000);
    CreationPlanOrigineY->setMinimum(-1000);
    CreationPlanOrigineY->setDecimals(4);
    CreationPlanOrigineY->setSingleStep(0.05);
    CreationPlanOrigineY->setAccelerated(true);
    CreationPlanOrigineLayout->addWidget(CreationPlanOrigineY);

    QDoubleSpinBox* CreationPlanOrigineZ = new QDoubleSpinBox;
    CreationPlanOrigineZ->setObjectName("CreationPlanOrigineZ");
    CreationPlanOrigineZ->setValue(0);
    CreationPlanOrigineZ->setDecimals(4);
    CreationPlanOrigineZ->setMaximum(1000);
    CreationPlanOrigineZ->setMinimum(-1000);
    CreationPlanOrigineZ->setSingleStep(0.05);
    CreationPlanOrigineZ->setAccelerated(true);
    CreationPlanOrigineLayout->addWidget(CreationPlanOrigineZ);

    CreationPlanLayout->addRow("Origine : ", CreationPlanOrigine);

    QWidget* CreationPlanNormale = new QWidget;
    QHBoxLayout* CreationPlanNormaleLayout = new QHBoxLayout;
    CreationPlanNormale->setLayout(CreationPlanNormaleLayout);

    QDoubleSpinBox* CreationPlanNormaleX = new QDoubleSpinBox;
    CreationPlanNormaleX->setObjectName("CreationPlanNormaleX");
    CreationPlanNormaleX->setValue(0);
    CreationPlanNormaleX->setDecimals(4);
    CreationPlanNormaleX->setMaximum(1000);
    CreationPlanNormaleX->setMinimum(-1000);
    CreationPlanNormaleX->setSingleStep(0.05);
    CreationPlanNormaleX->setAccelerated(true);
    CreationPlanNormaleLayout->addWidget(CreationPlanNormaleX);

    QDoubleSpinBox* CreationPlanNormaleY = new QDoubleSpinBox;
    CreationPlanNormaleY->setObjectName("CreationPlanNormaleY");
    CreationPlanNormaleY->setValue(0);
    CreationPlanNormaleY->setDecimals(4);
    CreationPlanNormaleY->setMaximum(1000);
    CreationPlanNormaleY->setMinimum(-1000);
    CreationPlanNormaleY->setSingleStep(0.05);
    CreationPlanNormaleY->setAccelerated(true);
    CreationPlanNormaleLayout->addWidget(CreationPlanNormaleY);

    QDoubleSpinBox* CreationPlanNormaleZ = new QDoubleSpinBox;
    CreationPlanNormaleZ->setObjectName("CreationPlanNormaleZ");
    CreationPlanNormaleZ->setValue(1);
    CreationPlanNormaleZ->setMaximum(1000);
    CreationPlanNormaleZ->setMinimum(-1000);
    CreationPlanNormaleZ->setDecimals(4);
    CreationPlanNormaleZ->setSingleStep(0.05);
    CreationPlanNormaleZ->setAccelerated(true);
    CreationPlanNormaleLayout->addWidget(CreationPlanNormaleZ);

    CreationPlanLayout->addRow("Normale : ", CreationPlanNormale);

    QDoubleSpinBox* CreationPlanRebond = new QDoubleSpinBox;
    CreationPlanRebond->setObjectName("CreationPlanRebond");
    CreationPlanRebond->setValue(0.8);
    CreationPlanRebond->setMaximum(1);
    CreationPlanRebond->setMinimum(0);
    CreationPlanRebond->setDecimals(4);
    CreationPlanRebond->setSingleStep(0.05);
    CreationPlanRebond->setAccelerated(true);
    CreationPlanLayout->addRow("Coefficient de rebonds : ", CreationPlanRebond);

    QPushButton* CreationPlanAfficherCouleur = new QPushButton("Choisir la couleur");
    CreationPlanLayout->addRow(CreationPlanAfficherCouleur);

    QColorDialog* CreationPlanCouleur = new QColorDialog(Qt::white, m_CreationPlan);
    CreationPlanCouleur->hide();
    CreationPlanCouleur->setObjectName("CreationPlanCouleur");
    connect(CreationPlanAfficherCouleur, SIGNAL(clicked()), CreationPlanCouleur, SLOT(show()));

    QPushButton* CreationPlanButton = new QPushButton("Cr�er le Plan Infini !");
    CreationPlanLayout->addRow(CreationPlanButton);

    QPushButton* CreationPlanAnnuler = new QPushButton("Annuler");
    CreationPlanLayout->addRow(CreationPlanAnnuler);

    m_CreationPlan->setLayout(CreationPlanLayout);

    connect(CreationPlanButton, SIGNAL(clicked()), m_CreationPlan, SLOT(hide()));
    connect(CreationPlanButton, SIGNAL(clicked()), m_CreationPlan->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));

    connect(CreationPlanAnnuler, SIGNAL(clicked()), m_CreationPlan, SLOT(hide()));
    connect(CreationPlanAnnuler, SIGNAL(clicked()), m_CreationPlan->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));


    connect(CreationPlanButton, SIGNAL(clicked()), this, SLOT(creePlan()));

}

void AjouteElementDock::genereCreationPlanFini()
{

    QLayout* par = m_CreationPlanFini->parentWidget()->layout();
    delete m_CreationPlanFini;

    m_CreationPlanFini = new QGroupBox("Cr�ation d'un Plan Fini");

    par->addWidget(m_CreationPlanFini);

    QFormLayout* CreationPlanFiniLayout = new QFormLayout;


    QWidget* CreationPlanFiniOrigine = new QWidget;
    QHBoxLayout* CreationPlanFiniOrigineLayout = new QHBoxLayout;
    CreationPlanFiniOrigine->setLayout(CreationPlanFiniOrigineLayout);

    QDoubleSpinBox* CreationPlanFiniOrigineX = new QDoubleSpinBox;
    CreationPlanFiniOrigineX->setObjectName("CreationPlanFiniOrigineX");
    CreationPlanFiniOrigineX->setValue(0);
    CreationPlanFiniOrigineX->setDecimals(4);
    CreationPlanFiniOrigineX->setMaximum(1000);
    CreationPlanFiniOrigineX->setMinimum(-1000);
    CreationPlanFiniOrigineX->setSingleStep(0.05);
    CreationPlanFiniOrigineX->setAccelerated(true);
    CreationPlanFiniOrigineLayout->addWidget(CreationPlanFiniOrigineX);

    QDoubleSpinBox* CreationPlanFiniOrigineY = new QDoubleSpinBox;
    CreationPlanFiniOrigineY->setObjectName("CreationPlanFiniOrigineY");
    CreationPlanFiniOrigineY->setValue(0);
    CreationPlanFiniOrigineY->setMaximum(1000);
    CreationPlanFiniOrigineY->setMinimum(-1000);
    CreationPlanFiniOrigineY->setDecimals(4);
    CreationPlanFiniOrigineY->setSingleStep(0.05);
    CreationPlanFiniOrigineY->setAccelerated(true);
    CreationPlanFiniOrigineLayout->addWidget(CreationPlanFiniOrigineY);

    QDoubleSpinBox* CreationPlanFiniOrigineZ = new QDoubleSpinBox;
    CreationPlanFiniOrigineZ->setObjectName("CreationPlanFiniOrigineZ");
    CreationPlanFiniOrigineZ->setValue(0);
    CreationPlanFiniOrigineZ->setDecimals(4);
    CreationPlanFiniOrigineZ->setMaximum(1000);
    CreationPlanFiniOrigineZ->setMinimum(-1000);
    CreationPlanFiniOrigineZ->setSingleStep(0.05);
    CreationPlanFiniOrigineZ->setAccelerated(true);
    CreationPlanFiniOrigineLayout->addWidget(CreationPlanFiniOrigineZ);

    CreationPlanFiniLayout->addRow("Origine : ", CreationPlanFiniOrigine);

    QWidget* CreationPlanFiniNormale = new QWidget;
    QHBoxLayout* CreationPlanFiniNormaleLayout = new QHBoxLayout;
    CreationPlanFiniNormale->setLayout(CreationPlanFiniNormaleLayout);

    QDoubleSpinBox* CreationPlanFiniNormaleX = new QDoubleSpinBox;
    CreationPlanFiniNormaleX->setObjectName("CreationPlanFiniNormaleX");
    CreationPlanFiniNormaleX->setValue(0);
    CreationPlanFiniNormaleX->setDecimals(4);
    CreationPlanFiniNormaleX->setMaximum(1000);
    CreationPlanFiniNormaleX->setMinimum(-1000);
    CreationPlanFiniNormaleX->setSingleStep(0.05);
    CreationPlanFiniNormaleX->setAccelerated(true);
    CreationPlanFiniNormaleLayout->addWidget(CreationPlanFiniNormaleX);

    QDoubleSpinBox* CreationPlanFiniNormaleY = new QDoubleSpinBox;
    CreationPlanFiniNormaleY->setObjectName("CreationPlanFiniNormaleY");
    CreationPlanFiniNormaleY->setValue(0);
    CreationPlanFiniNormaleY->setDecimals(4);
    CreationPlanFiniNormaleY->setMaximum(1000);
    CreationPlanFiniNormaleY->setMinimum(-1000);
    CreationPlanFiniNormaleY->setSingleStep(0.05);
    CreationPlanFiniNormaleY->setAccelerated(true);
    CreationPlanFiniNormaleLayout->addWidget(CreationPlanFiniNormaleY);

    QDoubleSpinBox* CreationPlanFiniNormaleZ = new QDoubleSpinBox;
    CreationPlanFiniNormaleZ->setObjectName("CreationPlanFiniNormaleZ");
    CreationPlanFiniNormaleZ->setValue(1);
    CreationPlanFiniNormaleZ->setMaximum(1000);
    CreationPlanFiniNormaleZ->setMinimum(-1000);
    CreationPlanFiniNormaleZ->setDecimals(4);
    CreationPlanFiniNormaleZ->setSingleStep(0.05);
    CreationPlanFiniNormaleZ->setAccelerated(true);
    CreationPlanFiniNormaleLayout->addWidget(CreationPlanFiniNormaleZ);

    CreationPlanFiniLayout->addRow("Normale : ", CreationPlanFiniNormale);

    QWidget* CreationPlanFiniLongueur = new QWidget;
    QHBoxLayout* CreationPlanFiniLongueurLayout = new QHBoxLayout;
    CreationPlanFiniLongueur->setLayout(CreationPlanFiniLongueurLayout);

    QDoubleSpinBox* CreationPlanFiniLongueurX = new QDoubleSpinBox;
    CreationPlanFiniLongueurX->setObjectName("CreationPlanFiniLongueurX");
    CreationPlanFiniLongueurX->setValue(0);
    CreationPlanFiniLongueurX->setDecimals(4);
    CreationPlanFiniLongueurX->setMaximum(1000);
    CreationPlanFiniLongueurX->setMinimum(-1000);
    CreationPlanFiniLongueurX->setSingleStep(0.05);
    CreationPlanFiniLongueurX->setAccelerated(true);
    CreationPlanFiniLongueurLayout->addWidget(CreationPlanFiniLongueurX);

    QDoubleSpinBox* CreationPlanFiniLongueurY = new QDoubleSpinBox;
    CreationPlanFiniLongueurY->setObjectName("CreationPlanFiniLongueurY");
    CreationPlanFiniLongueurY->setValue(1);
    CreationPlanFiniLongueurY->setDecimals(4);
    CreationPlanFiniLongueurY->setMaximum(1000);
    CreationPlanFiniLongueurY->setMinimum(-1000);
    CreationPlanFiniLongueurY->setSingleStep(0.05);
    CreationPlanFiniLongueurY->setAccelerated(true);
    CreationPlanFiniLongueurLayout->addWidget(CreationPlanFiniLongueurY);

    QDoubleSpinBox* CreationPlanFiniLongueurZ = new QDoubleSpinBox;
    CreationPlanFiniLongueurZ->setObjectName("CreationPlanFiniLongueurZ");
    CreationPlanFiniLongueurZ->setValue(0);
    CreationPlanFiniLongueurZ->setMaximum(1000);
    CreationPlanFiniLongueurZ->setMinimum(-1000);
    CreationPlanFiniLongueurZ->setDecimals(4);
    CreationPlanFiniLongueurZ->setSingleStep(0.05);
    CreationPlanFiniLongueurZ->setAccelerated(true);
    CreationPlanFiniLongueurLayout->addWidget(CreationPlanFiniLongueurZ);

    CreationPlanFiniLayout->addRow("Longueur : ", CreationPlanFiniLongueur);

    QWidget* CreationPlanFiniLargeur = new QWidget;
    QHBoxLayout* CreationPlanFiniLargeurLayout = new QHBoxLayout;
    CreationPlanFiniLargeur->setLayout(CreationPlanFiniLargeurLayout);

    QDoubleSpinBox* CreationPlanFiniLargeurX = new QDoubleSpinBox;
    CreationPlanFiniLargeurX->setObjectName("CreationPlanFiniLargeurX");
    CreationPlanFiniLargeurX->setValue(1);
    CreationPlanFiniLargeurX->setDecimals(4);
    CreationPlanFiniLargeurX->setMaximum(1000);
    CreationPlanFiniLargeurX->setMinimum(-1000);
    CreationPlanFiniLargeurX->setSingleStep(0.05);
    CreationPlanFiniLargeurX->setAccelerated(true);
    CreationPlanFiniLargeurLayout->addWidget(CreationPlanFiniLargeurX);

    QDoubleSpinBox* CreationPlanFiniLargeurY = new QDoubleSpinBox;
    CreationPlanFiniLargeurY->setObjectName("CreationPlanFiniLargeurY");
    CreationPlanFiniLargeurY->setValue(0);
    CreationPlanFiniLargeurY->setDecimals(4);
    CreationPlanFiniLargeurY->setMaximum(1000);
    CreationPlanFiniLargeurY->setMinimum(-1000);
    CreationPlanFiniLargeurY->setSingleStep(0.05);
    CreationPlanFiniLargeurY->setAccelerated(true);
    CreationPlanFiniLargeurLayout->addWidget(CreationPlanFiniLargeurY);

    QDoubleSpinBox* CreationPlanFiniLargeurZ = new QDoubleSpinBox;
    CreationPlanFiniLargeurZ->setObjectName("CreationPlanFiniLargeurZ");
    CreationPlanFiniLargeurZ->setValue(0);
    CreationPlanFiniLargeurZ->setMaximum(1000);
    CreationPlanFiniLargeurZ->setMinimum(-1000);
    CreationPlanFiniLargeurZ->setDecimals(4);
    CreationPlanFiniLargeurZ->setSingleStep(0.05);
    CreationPlanFiniLargeurZ->setAccelerated(true);
    CreationPlanFiniLargeurLayout->addWidget(CreationPlanFiniLargeurZ);

    CreationPlanFiniLayout->addRow("Largeur : ", CreationPlanFiniLargeur);

    QDoubleSpinBox* CreationPlanFiniRebond = new QDoubleSpinBox;
    CreationPlanFiniRebond->setObjectName("CreationPlanFiniRebond");
    CreationPlanFiniRebond->setValue(0.8);
    CreationPlanFiniRebond->setMaximum(1);
    CreationPlanFiniRebond->setMinimum(0);
    CreationPlanFiniRebond->setDecimals(4);
    CreationPlanFiniRebond->setSingleStep(0.05);
    CreationPlanFiniRebond->setAccelerated(true);
    CreationPlanFiniLayout->addRow("Coefficient de rebonds : ", CreationPlanFiniRebond);

    QPushButton* CreationPlanFiniAfficherCouleur = new QPushButton("Choisir la couleur");
    CreationPlanFiniLayout->addRow(CreationPlanFiniAfficherCouleur);

    QColorDialog* CreationPlanFiniCouleur = new QColorDialog(Qt::white, m_CreationPlanFini);
    CreationPlanFiniCouleur->hide();
    CreationPlanFiniCouleur->setObjectName("CreationPlanFiniCouleur");
    connect(CreationPlanFiniAfficherCouleur, SIGNAL(clicked()), CreationPlanFiniCouleur, SLOT(show()));

    QPushButton* CreationPlanFiniButton = new QPushButton("Cr�er le PlanFini !");
    CreationPlanFiniLayout->addRow(CreationPlanFiniButton);

    QPushButton* CreationPlanFiniAnnuler = new QPushButton("Annuler");
    CreationPlanFiniLayout->addRow(CreationPlanFiniAnnuler);

    m_CreationPlanFini->setLayout(CreationPlanFiniLayout);

    connect(CreationPlanFiniButton, SIGNAL(clicked()), m_CreationPlanFini, SLOT(hide()));
    connect(CreationPlanFiniButton, SIGNAL(clicked()), m_CreationPlanFini->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));

    connect(CreationPlanFiniAnnuler, SIGNAL(clicked()), m_CreationPlanFini, SLOT(hide()));
    connect(CreationPlanFiniAnnuler, SIGNAL(clicked()), m_CreationPlanFini->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));


    connect(CreationPlanFiniButton, SIGNAL(clicked()), this, SLOT(creePlanFini()));

}

void AjouteElementDock::genereCreationBrique()
{

    QLayout* par = m_CreationBrique->parentWidget()->layout();
    delete m_CreationBrique;

    m_CreationBrique = new QGroupBox("Cr�ation d'une Brique");

    par->addWidget(m_CreationBrique);

    QFormLayout* CreationBriqueLayout = new QFormLayout;


    QWidget* CreationBriqueOrigine = new QWidget;
    QHBoxLayout* CreationBriqueOrigineLayout = new QHBoxLayout;
    CreationBriqueOrigine->setLayout(CreationBriqueOrigineLayout);

    QDoubleSpinBox* CreationBriqueOrigineX = new QDoubleSpinBox;
    CreationBriqueOrigineX->setObjectName("CreationBriqueOrigineX");
    CreationBriqueOrigineX->setValue(0);
    CreationBriqueOrigineX->setDecimals(4);
    CreationBriqueOrigineX->setMaximum(1000);
    CreationBriqueOrigineX->setMinimum(-1000);
    CreationBriqueOrigineX->setSingleStep(0.05);
    CreationBriqueOrigineX->setAccelerated(true);
    CreationBriqueOrigineLayout->addWidget(CreationBriqueOrigineX);

    QDoubleSpinBox* CreationBriqueOrigineY = new QDoubleSpinBox;
    CreationBriqueOrigineY->setObjectName("CreationBriqueOrigineY");
    CreationBriqueOrigineY->setValue(0);
    CreationBriqueOrigineY->setMaximum(1000);
    CreationBriqueOrigineY->setMinimum(-1000);
    CreationBriqueOrigineY->setDecimals(4);
    CreationBriqueOrigineY->setSingleStep(0.05);
    CreationBriqueOrigineY->setAccelerated(true);
    CreationBriqueOrigineLayout->addWidget(CreationBriqueOrigineY);

    QDoubleSpinBox* CreationBriqueOrigineZ = new QDoubleSpinBox;
    CreationBriqueOrigineZ->setObjectName("CreationBriqueOrigineZ");
    CreationBriqueOrigineZ->setValue(0);
    CreationBriqueOrigineZ->setDecimals(4);
    CreationBriqueOrigineZ->setMaximum(1000);
    CreationBriqueOrigineZ->setMinimum(-1000);
    CreationBriqueOrigineZ->setSingleStep(0.05);
    CreationBriqueOrigineZ->setAccelerated(true);
    CreationBriqueOrigineLayout->addWidget(CreationBriqueOrigineZ);

    CreationBriqueLayout->addRow("Origine : ", CreationBriqueOrigine);

    QWidget* CreationBriqueLongueur = new QWidget;
    QHBoxLayout* CreationBriqueLongueurLayout = new QHBoxLayout;
    CreationBriqueLongueur->setLayout(CreationBriqueLongueurLayout);

    QDoubleSpinBox* CreationBriqueLongueurX = new QDoubleSpinBox;
    CreationBriqueLongueurX->setObjectName("CreationBriqueLongueurX");
    CreationBriqueLongueurX->setValue(0);
    CreationBriqueLongueurX->setDecimals(4);
    CreationBriqueLongueurX->setMaximum(1000);
    CreationBriqueLongueurX->setMinimum(-1000);
    CreationBriqueLongueurX->setSingleStep(0.05);
    CreationBriqueLongueurX->setAccelerated(true);
    CreationBriqueLongueurLayout->addWidget(CreationBriqueLongueurX);

    QDoubleSpinBox* CreationBriqueLongueurY = new QDoubleSpinBox;
    CreationBriqueLongueurY->setObjectName("CreationBriqueLongueurY");
    CreationBriqueLongueurY->setValue(1);
    CreationBriqueLongueurY->setDecimals(4);
    CreationBriqueLongueurY->setMaximum(1000);
    CreationBriqueLongueurY->setMinimum(-1000);
    CreationBriqueLongueurY->setSingleStep(0.05);
    CreationBriqueLongueurY->setAccelerated(true);
    CreationBriqueLongueurLayout->addWidget(CreationBriqueLongueurY);

    QDoubleSpinBox* CreationBriqueLongueurZ = new QDoubleSpinBox;
    CreationBriqueLongueurZ->setObjectName("CreationBriqueLongueurZ");
    CreationBriqueLongueurZ->setValue(0);
    CreationBriqueLongueurZ->setMaximum(1000);
    CreationBriqueLongueurZ->setMinimum(-1000);
    CreationBriqueLongueurZ->setDecimals(4);
    CreationBriqueLongueurZ->setSingleStep(0.05);
    CreationBriqueLongueurZ->setAccelerated(true);
    CreationBriqueLongueurLayout->addWidget(CreationBriqueLongueurZ);

    CreationBriqueLayout->addRow("Longueur : ", CreationBriqueLongueur);

    QWidget* CreationBriqueLargeur = new QWidget;
    QHBoxLayout* CreationBriqueLargeurLayout = new QHBoxLayout;
    CreationBriqueLargeur->setLayout(CreationBriqueLargeurLayout);

    QDoubleSpinBox* CreationBriqueLargeurX = new QDoubleSpinBox;
    CreationBriqueLargeurX->setObjectName("CreationBriqueLargeurX");
    CreationBriqueLargeurX->setValue(1);
    CreationBriqueLargeurX->setDecimals(4);
    CreationBriqueLargeurX->setMaximum(1000);
    CreationBriqueLargeurX->setMinimum(-1000);
    CreationBriqueLargeurX->setSingleStep(0.05);
    CreationBriqueLargeurX->setAccelerated(true);
    CreationBriqueLargeurLayout->addWidget(CreationBriqueLargeurX);

    QDoubleSpinBox* CreationBriqueLargeurY = new QDoubleSpinBox;
    CreationBriqueLargeurY->setObjectName("CreationBriqueLargeurY");
    CreationBriqueLargeurY->setValue(0);
    CreationBriqueLargeurY->setDecimals(4);
    CreationBriqueLargeurY->setMaximum(1000);
    CreationBriqueLargeurY->setMinimum(-1000);
    CreationBriqueLargeurY->setSingleStep(0.05);
    CreationBriqueLargeurY->setAccelerated(true);
    CreationBriqueLargeurLayout->addWidget(CreationBriqueLargeurY);

    QDoubleSpinBox* CreationBriqueLargeurZ = new QDoubleSpinBox;
    CreationBriqueLargeurZ->setObjectName("CreationBriqueLargeurZ");
    CreationBriqueLargeurZ->setValue(0);
    CreationBriqueLargeurZ->setMaximum(1000);
    CreationBriqueLargeurZ->setMinimum(-1000);
    CreationBriqueLargeurZ->setDecimals(4);
    CreationBriqueLargeurZ->setSingleStep(0.05);
    CreationBriqueLargeurZ->setAccelerated(true);
    CreationBriqueLargeurLayout->addWidget(CreationBriqueLargeurZ);

    CreationBriqueLayout->addRow("Largeur : ", CreationBriqueLargeur);



    QDoubleSpinBox* CreationBriqueHauteur = new QDoubleSpinBox;
    CreationBriqueHauteur->setObjectName("CreationBriqueHauteur");
    CreationBriqueHauteur->setValue(1);
    CreationBriqueHauteur->setMaximum(1000);
    CreationBriqueHauteur->setMinimum(0);
    CreationBriqueHauteur->setDecimals(4);
    CreationBriqueHauteur->setSingleStep(0.05);
    CreationBriqueHauteur->setAccelerated(true);
    CreationBriqueLayout->addRow("Hauteur : ", CreationBriqueHauteur);

    QDoubleSpinBox* CreationBriqueRebond = new QDoubleSpinBox;
    CreationBriqueRebond->setObjectName("CreationBriqueRebond");
    CreationBriqueRebond->setValue(0.8);
    CreationBriqueRebond->setMaximum(1);
    CreationBriqueRebond->setMinimum(0);
    CreationBriqueRebond->setDecimals(4);
    CreationBriqueRebond->setSingleStep(0.05);
    CreationBriqueRebond->setAccelerated(true);
    CreationBriqueLayout->addRow("Coefficient de rebonds : ", CreationBriqueRebond);

    QPushButton* CreationBriqueAfficherCouleurB = new QPushButton("Choisir la couleur de la face sup�rieure");
    CreationBriqueLayout->addRow(CreationBriqueAfficherCouleurB);
    QColorDialog* CreationBriqueCouleurB = new QColorDialog(Qt::yellow, m_CreationBrique);
    CreationBriqueCouleurB->hide();
    CreationBriqueCouleurB->setObjectName("CreationBriqueCouleurB");
    connect(CreationBriqueAfficherCouleurB, SIGNAL(clicked()), CreationBriqueCouleurB, SLOT(show()));

    QPushButton* CreationBriqueAfficherCouleurA = new QPushButton("Choisir la couleur de la face inf�rieure");
    CreationBriqueLayout->addRow(CreationBriqueAfficherCouleurA);
    QColorDialog* CreationBriqueCouleurA = new QColorDialog(Qt::cyan, m_CreationBrique);
    CreationBriqueCouleurA->hide();
    CreationBriqueCouleurA->setObjectName("CreationBriqueCouleurA");
    connect(CreationBriqueAfficherCouleurA, SIGNAL(clicked()), CreationBriqueCouleurA, SLOT(show()));

    QPushButton* CreationBriqueAfficherCouleurC = new QPushButton("Choisir la couleur de la face de gauche");
    CreationBriqueLayout->addRow(CreationBriqueAfficherCouleurC);
    QColorDialog* CreationBriqueCouleurC = new QColorDialog(Qt::magenta, m_CreationBrique);
    CreationBriqueCouleurC->hide();
    CreationBriqueCouleurC->setObjectName("CreationBriqueCouleurC");
    connect(CreationBriqueAfficherCouleurC, SIGNAL(clicked()), CreationBriqueCouleurC, SLOT(show()));

    QPushButton* CreationBriqueAfficherCouleurD = new QPushButton("Choisir la couleur de la face de droite");
    CreationBriqueLayout->addRow(CreationBriqueAfficherCouleurD);
    QColorDialog* CreationBriqueCouleurD = new QColorDialog(Qt::red, m_CreationBrique);
    CreationBriqueCouleurD->hide();
    CreationBriqueCouleurD->setObjectName("CreationBriqueCouleurD");
    connect(CreationBriqueAfficherCouleurD, SIGNAL(clicked()), CreationBriqueCouleurD, SLOT(show()));

    QPushButton* CreationBriqueAfficherCouleurE = new QPushButton("Choisir la couleur de la face du fond");
    CreationBriqueLayout->addRow(CreationBriqueAfficherCouleurE);
    QColorDialog* CreationBriqueCouleurE = new QColorDialog(Qt::green, m_CreationBrique);
    CreationBriqueCouleurE->hide();
    CreationBriqueCouleurE->setObjectName("CreationBriqueCouleurE");
    connect(CreationBriqueAfficherCouleurE, SIGNAL(clicked()), CreationBriqueCouleurE, SLOT(show()));

    QPushButton* CreationBriqueAfficherCouleurF = new QPushButton("Choisir la couleur de la face de devant");
    CreationBriqueLayout->addRow(CreationBriqueAfficherCouleurF);
    QColorDialog* CreationBriqueCouleurF = new QColorDialog(Qt::blue, m_CreationBrique);
    CreationBriqueCouleurF->hide();
    CreationBriqueCouleurF->setObjectName("CreationBriqueCouleurF");
    connect(CreationBriqueAfficherCouleurF, SIGNAL(clicked()), CreationBriqueCouleurF, SLOT(show()));

    QPushButton* CreationBriqueButton = new QPushButton("Cr�er la Brique !");
    CreationBriqueLayout->addRow(CreationBriqueButton);

    QPushButton* CreationBriqueAnnuler = new QPushButton("Annuler");
    CreationBriqueLayout->addRow(CreationBriqueAnnuler);

    m_CreationBrique->setLayout(CreationBriqueLayout);

    connect(CreationBriqueButton, SIGNAL(clicked()), m_CreationBrique, SLOT(hide()));
    connect(CreationBriqueButton, SIGNAL(clicked()), m_CreationBrique->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));

    connect(CreationBriqueAnnuler, SIGNAL(clicked()), m_CreationBrique, SLOT(hide()));
    connect(CreationBriqueAnnuler, SIGNAL(clicked()), m_CreationBrique->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));


    connect(CreationBriqueButton, SIGNAL(clicked()), this, SLOT(creeBrique()));

}

void AjouteElementDock::genereCreationVent()
{

    QLayout* par = m_CreationVent->parentWidget()->layout();
    delete m_CreationVent;

    m_CreationVent = new QGroupBox("Cr�ation d'un Vent");

    par->addWidget(m_CreationVent);

    QFormLayout* CreationVentLayout = new QFormLayout;


    QWidget* CreationVentOrigine = new QWidget;
    QHBoxLayout* CreationVentOrigineLayout = new QHBoxLayout;
    CreationVentOrigine->setLayout(CreationVentOrigineLayout);

    QDoubleSpinBox* CreationVentOrigineX = new QDoubleSpinBox;
    CreationVentOrigineX->setObjectName("CreationVentOrigineX");
    CreationVentOrigineX->setValue(0);
    CreationVentOrigineX->setDecimals(4);
    CreationVentOrigineX->setMaximum(1000);
    CreationVentOrigineX->setMinimum(-1000);
    CreationVentOrigineX->setSingleStep(0.05);
    CreationVentOrigineX->setAccelerated(true);
    CreationVentOrigineLayout->addWidget(CreationVentOrigineX);

    QDoubleSpinBox* CreationVentOrigineY = new QDoubleSpinBox;
    CreationVentOrigineY->setObjectName("CreationVentOrigineY");
    CreationVentOrigineY->setValue(0);
    CreationVentOrigineY->setMaximum(1000);
    CreationVentOrigineY->setMinimum(-1000);
    CreationVentOrigineY->setDecimals(4);
    CreationVentOrigineY->setSingleStep(0.05);
    CreationVentOrigineY->setAccelerated(true);
    CreationVentOrigineLayout->addWidget(CreationVentOrigineY);

    QDoubleSpinBox* CreationVentOrigineZ = new QDoubleSpinBox;
    CreationVentOrigineZ->setObjectName("CreationVentOrigineZ");
    CreationVentOrigineZ->setValue(0);
    CreationVentOrigineZ->setDecimals(4);
    CreationVentOrigineZ->setMaximum(1000);
    CreationVentOrigineZ->setMinimum(-1000);
    CreationVentOrigineZ->setSingleStep(0.05);
    CreationVentOrigineZ->setAccelerated(true);
    CreationVentOrigineLayout->addWidget(CreationVentOrigineZ);

    CreationVentLayout->addRow("Origine : ", CreationVentOrigine);

    QWidget* CreationVentNormale = new QWidget;
    QHBoxLayout* CreationVentNormaleLayout = new QHBoxLayout;
    CreationVentNormale->setLayout(CreationVentNormaleLayout);

    QDoubleSpinBox* CreationVentNormaleX = new QDoubleSpinBox;
    CreationVentNormaleX->setObjectName("CreationVentNormaleX");
    CreationVentNormaleX->setValue(0);
    CreationVentNormaleX->setDecimals(4);
    CreationVentNormaleX->setMaximum(1000);
    CreationVentNormaleX->setMinimum(-1000);
    CreationVentNormaleX->setSingleStep(0.05);
    CreationVentNormaleX->setAccelerated(true);
    CreationVentNormaleLayout->addWidget(CreationVentNormaleX);

    QDoubleSpinBox* CreationVentNormaleY = new QDoubleSpinBox;
    CreationVentNormaleY->setObjectName("CreationVentNormaleY");
    CreationVentNormaleY->setValue(0);
    CreationVentNormaleY->setDecimals(4);
    CreationVentNormaleY->setMaximum(1000);
    CreationVentNormaleY->setMinimum(-1000);
    CreationVentNormaleY->setSingleStep(0.05);
    CreationVentNormaleY->setAccelerated(true);
    CreationVentNormaleLayout->addWidget(CreationVentNormaleY);

    QDoubleSpinBox* CreationVentNormaleZ = new QDoubleSpinBox;
    CreationVentNormaleZ->setObjectName("CreationVentNormaleZ");
    CreationVentNormaleZ->setValue(1);
    CreationVentNormaleZ->setMaximum(1000);
    CreationVentNormaleZ->setMinimum(-1000);
    CreationVentNormaleZ->setDecimals(4);
    CreationVentNormaleZ->setSingleStep(0.05);
    CreationVentNormaleZ->setAccelerated(true);
    CreationVentNormaleLayout->addWidget(CreationVentNormaleZ);

    CreationVentLayout->addRow("Normale : ", CreationVentNormale);

    QWidget* CreationVentLongueur = new QWidget;
    QHBoxLayout* CreationVentLongueurLayout = new QHBoxLayout;
    CreationVentLongueur->setLayout(CreationVentLongueurLayout);

    QDoubleSpinBox* CreationVentLongueurX = new QDoubleSpinBox;
    CreationVentLongueurX->setObjectName("CreationVentLongueurX");
    CreationVentLongueurX->setValue(0);
    CreationVentLongueurX->setDecimals(4);
    CreationVentLongueurX->setMaximum(1000);
    CreationVentLongueurX->setMinimum(-1000);
    CreationVentLongueurX->setSingleStep(0.05);
    CreationVentLongueurX->setAccelerated(true);
    CreationVentLongueurLayout->addWidget(CreationVentLongueurX);

    QDoubleSpinBox* CreationVentLongueurY = new QDoubleSpinBox;
    CreationVentLongueurY->setObjectName("CreationVentLongueurY");
    CreationVentLongueurY->setValue(1);
    CreationVentLongueurY->setDecimals(4);
    CreationVentLongueurY->setMaximum(1000);
    CreationVentLongueurY->setMinimum(-1000);
    CreationVentLongueurY->setSingleStep(0.05);
    CreationVentLongueurY->setAccelerated(true);
    CreationVentLongueurLayout->addWidget(CreationVentLongueurY);

    QDoubleSpinBox* CreationVentLongueurZ = new QDoubleSpinBox;
    CreationVentLongueurZ->setObjectName("CreationVentLongueurZ");
    CreationVentLongueurZ->setValue(0);
    CreationVentLongueurZ->setMaximum(1000);
    CreationVentLongueurZ->setMinimum(-1000);
    CreationVentLongueurZ->setDecimals(4);
    CreationVentLongueurZ->setSingleStep(0.05);
    CreationVentLongueurZ->setAccelerated(true);
    CreationVentLongueurLayout->addWidget(CreationVentLongueurZ);

    CreationVentLayout->addRow("Longueur : ", CreationVentLongueur);

    QWidget* CreationVentLargeur = new QWidget;
    QHBoxLayout* CreationVentLargeurLayout = new QHBoxLayout;
    CreationVentLargeur->setLayout(CreationVentLargeurLayout);

    QDoubleSpinBox* CreationVentLargeurX = new QDoubleSpinBox;
    CreationVentLargeurX->setObjectName("CreationVentLargeurX");
    CreationVentLargeurX->setValue(1);
    CreationVentLargeurX->setDecimals(4);
    CreationVentLargeurX->setMaximum(1000);
    CreationVentLargeurX->setMinimum(-1000);
    CreationVentLargeurX->setSingleStep(0.05);
    CreationVentLargeurX->setAccelerated(true);
    CreationVentLargeurLayout->addWidget(CreationVentLargeurX);

    QDoubleSpinBox* CreationVentLargeurY = new QDoubleSpinBox;
    CreationVentLargeurY->setObjectName("CreationVentLargeurY");
    CreationVentLargeurY->setValue(0);
    CreationVentLargeurY->setDecimals(4);
    CreationVentLargeurY->setMaximum(1000);
    CreationVentLargeurY->setMinimum(-1000);
    CreationVentLargeurY->setSingleStep(0.05);
    CreationVentLargeurY->setAccelerated(true);
    CreationVentLargeurLayout->addWidget(CreationVentLargeurY);

    QDoubleSpinBox* CreationVentLargeurZ = new QDoubleSpinBox;
    CreationVentLargeurZ->setObjectName("CreationVentLargeurZ");
    CreationVentLargeurZ->setValue(0);
    CreationVentLargeurZ->setMaximum(1000);
    CreationVentLargeurZ->setMinimum(-1000);
    CreationVentLargeurZ->setDecimals(4);
    CreationVentLargeurZ->setSingleStep(0.05);
    CreationVentLargeurZ->setAccelerated(true);
    CreationVentLargeurLayout->addWidget(CreationVentLargeurZ);

    CreationVentLayout->addRow("Largeur : ", CreationVentLargeur);

    QDoubleSpinBox* CreationVentProfondeur = new QDoubleSpinBox;
    CreationVentProfondeur->setObjectName("CreationVentProfondeur");
    CreationVentProfondeur->setValue(1);
    CreationVentProfondeur->setMaximum(1000);
    CreationVentProfondeur->setMinimum(0);
    CreationVentProfondeur->setDecimals(4);
    CreationVentProfondeur->setSingleStep(0.05);
    CreationVentProfondeur->setAccelerated(true);
    CreationVentLayout->addRow("Profondeur : ", CreationVentProfondeur);

    QDoubleSpinBox* CreationVentIntensite = new QDoubleSpinBox;
    CreationVentIntensite->setObjectName("CreationVentIntensite");
    CreationVentIntensite->setValue(10);
    CreationVentIntensite->setMaximum(1000);
    CreationVentIntensite->setMinimum(0);
    CreationVentIntensite->setDecimals(4);
    CreationVentIntensite->setSingleStep(0.05);
    CreationVentIntensite->setAccelerated(true);
    CreationVentLayout->addRow("Intensit� : ", CreationVentIntensite);

    QPushButton* CreationVentButton = new QPushButton("Cr�er le Vent !");
    CreationVentLayout->addRow(CreationVentButton);

    QPushButton* CreationVentAnnuler = new QPushButton("Annuler");
    CreationVentLayout->addRow(CreationVentAnnuler);

    m_CreationVent->setLayout(CreationVentLayout);

    connect(CreationVentButton, SIGNAL(clicked()), m_CreationVent, SLOT(hide()));
    connect(CreationVentButton, SIGNAL(clicked()), m_CreationVent->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));

    connect(CreationVentAnnuler, SIGNAL(clicked()), m_CreationVent, SLOT(hide()));
    connect(CreationVentAnnuler, SIGNAL(clicked()), m_CreationVent->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));


    connect(CreationVentButton, SIGNAL(clicked()), this, SLOT(creeVent()));
}

void AjouteElementDock::genereCreationChampForcesGlobal()
{
    QLayout* par = m_CreationChampForcesGlobal->parentWidget()->layout();
    delete m_CreationChampForcesGlobal;

    m_CreationChampForcesGlobal = new QGroupBox("Cr�ation d'un Champ de Forces Global");

    par->addWidget(m_CreationChampForcesGlobal);

    QFormLayout* CreationChampForcesGlobalLayout = new QFormLayout;


    QWidget* CreationChampForcesGlobalIntensite = new QWidget;
    QHBoxLayout* CreationChampForcesGlobalIntensiteLayout = new QHBoxLayout;
    CreationChampForcesGlobalIntensite->setLayout(CreationChampForcesGlobalIntensiteLayout);

    QDoubleSpinBox* CreationChampForcesGlobalIntensiteX = new QDoubleSpinBox;
    CreationChampForcesGlobalIntensiteX->setObjectName("CreationChampForcesGlobalIntensiteX");
    CreationChampForcesGlobalIntensiteX->setValue(0);
    CreationChampForcesGlobalIntensiteX->setDecimals(4);
    CreationChampForcesGlobalIntensiteX->setMaximum(1000);
    CreationChampForcesGlobalIntensiteX->setMinimum(-1000);
    CreationChampForcesGlobalIntensiteX->setSingleStep(0.05);
    CreationChampForcesGlobalIntensiteX->setAccelerated(true);
    CreationChampForcesGlobalIntensiteLayout->addWidget(CreationChampForcesGlobalIntensiteX);

    QDoubleSpinBox* CreationChampForcesGlobalIntensiteY = new QDoubleSpinBox;
    CreationChampForcesGlobalIntensiteY->setObjectName("CreationChampForcesGlobalIntensiteY");
    CreationChampForcesGlobalIntensiteY->setValue(0);
    CreationChampForcesGlobalIntensiteY->setMaximum(1000);
    CreationChampForcesGlobalIntensiteY->setMinimum(-1000);
    CreationChampForcesGlobalIntensiteY->setDecimals(4);
    CreationChampForcesGlobalIntensiteY->setSingleStep(0.05);
    CreationChampForcesGlobalIntensiteY->setAccelerated(true);
    CreationChampForcesGlobalIntensiteLayout->addWidget(CreationChampForcesGlobalIntensiteY);

    QDoubleSpinBox* CreationChampForcesGlobalIntensiteZ = new QDoubleSpinBox;
    CreationChampForcesGlobalIntensiteZ->setObjectName("CreationChampForcesGlobalIntensiteZ");
    CreationChampForcesGlobalIntensiteZ->setMinimum(-1000);
    CreationChampForcesGlobalIntensiteZ->setValue(-9.81);
    CreationChampForcesGlobalIntensiteZ->setDecimals(4);
    CreationChampForcesGlobalIntensiteZ->setMaximum(1000);
    CreationChampForcesGlobalIntensiteZ->setSingleStep(0.05);
    CreationChampForcesGlobalIntensiteZ->setAccelerated(true);
    CreationChampForcesGlobalIntensiteLayout->addWidget(CreationChampForcesGlobalIntensiteZ);

    CreationChampForcesGlobalLayout->addRow("Intensit� : ", CreationChampForcesGlobalIntensite);



    QPushButton* CreationChampForcesGlobalButton = new QPushButton("Cr�er le Champ de Forces Global !");
    CreationChampForcesGlobalLayout->addRow(CreationChampForcesGlobalButton);

    QPushButton* CreationChampForcesGlobalAnnuler = new QPushButton("Annuler");
    CreationChampForcesGlobalLayout->addRow(CreationChampForcesGlobalAnnuler);

    m_CreationChampForcesGlobal->setLayout(CreationChampForcesGlobalLayout);

    connect(CreationChampForcesGlobalButton, SIGNAL(clicked()), m_CreationChampForcesGlobal, SLOT(hide()));
    connect(CreationChampForcesGlobalButton, SIGNAL(clicked()), m_CreationChampForcesGlobal->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));

    connect(CreationChampForcesGlobalAnnuler, SIGNAL(clicked()), m_CreationChampForcesGlobal, SLOT(hide()));
    connect(CreationChampForcesGlobalAnnuler, SIGNAL(clicked()), m_CreationChampForcesGlobal->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));


    connect(CreationChampForcesGlobalButton, SIGNAL(clicked()), this, SLOT(creeChampForcesGlobal()));
}


void AjouteElementDock::genereCreationVentilateur()
{
    QLayout* par = m_CreationVentilateur->parentWidget()->layout();
    delete m_CreationVentilateur;

    m_CreationVentilateur = new QGroupBox("Cr�ation d'un Ventilateur");

    par->addWidget(m_CreationVentilateur);

    QFormLayout* CreationVentilateurLayout = new QFormLayout;


    QWidget* CreationVentilateurOrigine = new QWidget;
    QHBoxLayout* CreationVentilateurOrigineLayout = new QHBoxLayout;
    CreationVentilateurOrigine->setLayout(CreationVentilateurOrigineLayout);

    QDoubleSpinBox* CreationVentilateurOrigineX = new QDoubleSpinBox;
    CreationVentilateurOrigineX->setObjectName("CreationVentilateurOrigineX");
    CreationVentilateurOrigineX->setValue(0);
    CreationVentilateurOrigineX->setDecimals(4);
    CreationVentilateurOrigineX->setMaximum(1000);
    CreationVentilateurOrigineX->setMinimum(-1000);
    CreationVentilateurOrigineX->setSingleStep(0.05);
    CreationVentilateurOrigineX->setAccelerated(true);
    CreationVentilateurOrigineLayout->addWidget(CreationVentilateurOrigineX);

    QDoubleSpinBox* CreationVentilateurOrigineY = new QDoubleSpinBox;
    CreationVentilateurOrigineY->setObjectName("CreationVentilateurOrigineY");
    CreationVentilateurOrigineY->setValue(0);
    CreationVentilateurOrigineY->setMaximum(1000);
    CreationVentilateurOrigineY->setMinimum(-1000);
    CreationVentilateurOrigineY->setDecimals(4);
    CreationVentilateurOrigineY->setSingleStep(0.05);
    CreationVentilateurOrigineY->setAccelerated(true);
    CreationVentilateurOrigineLayout->addWidget(CreationVentilateurOrigineY);

    QDoubleSpinBox* CreationVentilateurOrigineZ = new QDoubleSpinBox;
    CreationVentilateurOrigineZ->setObjectName("CreationVentilateurOrigineZ");
    CreationVentilateurOrigineZ->setValue(0);
    CreationVentilateurOrigineZ->setDecimals(4);
    CreationVentilateurOrigineZ->setMaximum(1000);
    CreationVentilateurOrigineZ->setMinimum(-1000);
    CreationVentilateurOrigineZ->setSingleStep(0.05);
    CreationVentilateurOrigineZ->setAccelerated(true);
    CreationVentilateurOrigineLayout->addWidget(CreationVentilateurOrigineZ);

    CreationVentilateurLayout->addRow("Origine : ", CreationVentilateurOrigine);

    QWidget* CreationVentilateurNormale = new QWidget;
    QHBoxLayout* CreationVentilateurNormaleLayout = new QHBoxLayout;
    CreationVentilateurNormale->setLayout(CreationVentilateurNormaleLayout);

    QDoubleSpinBox* CreationVentilateurNormaleX = new QDoubleSpinBox;
    CreationVentilateurNormaleX->setObjectName("CreationVentilateurNormaleX");
    CreationVentilateurNormaleX->setValue(0);
    CreationVentilateurNormaleX->setDecimals(4);
    CreationVentilateurNormaleX->setMaximum(1000);
    CreationVentilateurNormaleX->setMinimum(-1000);
    CreationVentilateurNormaleX->setSingleStep(0.05);
    CreationVentilateurNormaleX->setAccelerated(true);
    CreationVentilateurNormaleLayout->addWidget(CreationVentilateurNormaleX);

    QDoubleSpinBox* CreationVentilateurNormaleY = new QDoubleSpinBox;
    CreationVentilateurNormaleY->setObjectName("CreationVentilateurNormaleY");
    CreationVentilateurNormaleY->setValue(0);
    CreationVentilateurNormaleY->setMaximum(1000);
    CreationVentilateurNormaleY->setMinimum(-1000);
    CreationVentilateurNormaleY->setDecimals(4);
    CreationVentilateurNormaleY->setSingleStep(0.05);
    CreationVentilateurNormaleY->setAccelerated(true);
    CreationVentilateurNormaleLayout->addWidget(CreationVentilateurNormaleY);

    QDoubleSpinBox* CreationVentilateurNormaleZ = new QDoubleSpinBox;
    CreationVentilateurNormaleZ->setObjectName("CreationVentilateurNormaleZ");
    CreationVentilateurNormaleZ->setValue(1);
    CreationVentilateurNormaleZ->setDecimals(4);
    CreationVentilateurNormaleZ->setMaximum(1000);
    CreationVentilateurNormaleZ->setMinimum(-1000);
    CreationVentilateurNormaleZ->setSingleStep(0.05);
    CreationVentilateurNormaleZ->setAccelerated(true);
    CreationVentilateurNormaleLayout->addWidget(CreationVentilateurNormaleZ);

    CreationVentilateurLayout->addRow("Normale : ", CreationVentilateurNormale);

    QWidget* CreationVentilateurLongueur = new QWidget;
    QHBoxLayout* CreationVentilateurLongueurLayout = new QHBoxLayout;
    CreationVentilateurLongueur->setLayout(CreationVentilateurLongueurLayout);

    QDoubleSpinBox* CreationVentilateurLongueurX = new QDoubleSpinBox;
    CreationVentilateurLongueurX->setObjectName("CreationVentilateurLongueurX");
    CreationVentilateurLongueurX->setValue(0);
    CreationVentilateurLongueurX->setDecimals(4);
    CreationVentilateurLongueurX->setMaximum(1000);
    CreationVentilateurLongueurX->setMinimum(-1000);
    CreationVentilateurLongueurX->setSingleStep(0.05);
    CreationVentilateurLongueurX->setAccelerated(true);
    CreationVentilateurLongueurLayout->addWidget(CreationVentilateurLongueurX);

    QDoubleSpinBox* CreationVentilateurLongueurY = new QDoubleSpinBox;
    CreationVentilateurLongueurY->setObjectName("CreationVentilateurLongueurY");
    CreationVentilateurLongueurY->setValue(1);
    CreationVentilateurLongueurY->setDecimals(4);
    CreationVentilateurLongueurY->setMaximum(1000);
    CreationVentilateurLongueurY->setMinimum(-1000);
    CreationVentilateurLongueurY->setSingleStep(0.05);
    CreationVentilateurLongueurY->setAccelerated(true);
    CreationVentilateurLongueurLayout->addWidget(CreationVentilateurLongueurY);

    QDoubleSpinBox* CreationVentilateurLongueurZ = new QDoubleSpinBox;
    CreationVentilateurLongueurZ->setObjectName("CreationVentilateurLongueurZ");
    CreationVentilateurLongueurZ->setValue(0);
    CreationVentilateurLongueurZ->setMaximum(1000);
    CreationVentilateurLongueurZ->setMinimum(-1000);
    CreationVentilateurLongueurZ->setDecimals(4);
    CreationVentilateurLongueurZ->setSingleStep(0.05);
    CreationVentilateurLongueurZ->setAccelerated(true);
    CreationVentilateurLongueurLayout->addWidget(CreationVentilateurLongueurZ);

    CreationVentilateurLayout->addRow("Longueur : ", CreationVentilateurLongueur);

    QWidget* CreationVentilateurLargeur = new QWidget;
    QHBoxLayout* CreationVentilateurLargeurLayout = new QHBoxLayout;
    CreationVentilateurLargeur->setLayout(CreationVentilateurLargeurLayout);

    QDoubleSpinBox* CreationVentilateurLargeurX = new QDoubleSpinBox;
    CreationVentilateurLargeurX->setObjectName("CreationVentilateurLargeurX");
    CreationVentilateurLargeurX->setValue(1);
    CreationVentilateurLargeurX->setDecimals(4);
    CreationVentilateurLargeurX->setMaximum(1000);
    CreationVentilateurLargeurX->setMinimum(-1000);
    CreationVentilateurLargeurX->setSingleStep(0.05);
    CreationVentilateurLargeurX->setAccelerated(true);
    CreationVentilateurLargeurLayout->addWidget(CreationVentilateurLargeurX);

    QDoubleSpinBox* CreationVentilateurLargeurY = new QDoubleSpinBox;
    CreationVentilateurLargeurY->setObjectName("CreationVentilateurLargeurY");
    CreationVentilateurLargeurY->setValue(0);
    CreationVentilateurLargeurY->setDecimals(4);
    CreationVentilateurLargeurY->setMaximum(1000);
    CreationVentilateurLargeurY->setMinimum(-1000);
    CreationVentilateurLargeurY->setSingleStep(0.05);
    CreationVentilateurLargeurY->setAccelerated(true);
    CreationVentilateurLargeurLayout->addWidget(CreationVentilateurLargeurY);

    QDoubleSpinBox* CreationVentilateurLargeurZ = new QDoubleSpinBox;
    CreationVentilateurLargeurZ->setObjectName("CreationVentilateurLargeurZ");
    CreationVentilateurLargeurZ->setValue(0);
    CreationVentilateurLargeurZ->setMaximum(1000);
    CreationVentilateurLargeurZ->setMinimum(-1000);
    CreationVentilateurLargeurZ->setDecimals(4);
    CreationVentilateurLargeurZ->setSingleStep(0.05);
    CreationVentilateurLargeurZ->setAccelerated(true);
    CreationVentilateurLargeurLayout->addWidget(CreationVentilateurLargeurZ);

    CreationVentilateurLayout->addRow("Largeur : ", CreationVentilateurLargeur);



    QDoubleSpinBox* CreationVentilateurHauteur = new QDoubleSpinBox;
    CreationVentilateurHauteur->setObjectName("CreationVentilateurHauteur");
    CreationVentilateurHauteur->setValue(0.1);
    CreationVentilateurHauteur->setMaximum(1000);
    CreationVentilateurHauteur->setMinimum(0);
    CreationVentilateurHauteur->setDecimals(4);
    CreationVentilateurHauteur->setSingleStep(0.05);
    CreationVentilateurHauteur->setAccelerated(true);
    CreationVentilateurLayout->addRow("Hauteur : ", CreationVentilateurHauteur);

    QDoubleSpinBox* CreationVentilateurProfondeur = new QDoubleSpinBox;
    CreationVentilateurProfondeur->setObjectName("CreationVentilateurProfondeur");
    CreationVentilateurProfondeur->setValue(1);
    CreationVentilateurProfondeur->setMaximum(1000);
    CreationVentilateurProfondeur->setMinimum(0);
    CreationVentilateurProfondeur->setDecimals(4);
    CreationVentilateurProfondeur->setSingleStep(0.05);
    CreationVentilateurProfondeur->setAccelerated(true);
    CreationVentilateurLayout->addRow("Profondeur : ", CreationVentilateurProfondeur);


    QPushButton* CreationVentilateurAfficherCouleurB = new QPushButton("Choisir la couleur de la face sup�rieure (soufflerie)");
    CreationVentilateurLayout->addRow(CreationVentilateurAfficherCouleurB);
    QColorDialog* CreationVentilateurCouleurB = new QColorDialog(Qt::yellow, m_CreationVentilateur);
    CreationVentilateurCouleurB->hide();
    CreationVentilateurCouleurB->setObjectName("CreationVentilateurCouleurB");
    connect(CreationVentilateurAfficherCouleurB, SIGNAL(clicked()), CreationVentilateurCouleurB, SLOT(show()));

    QPushButton* CreationVentilateurAfficherCouleurA = new QPushButton("Choisir la couleur de la face inf�rieure");
    CreationVentilateurLayout->addRow(CreationVentilateurAfficherCouleurA);
    QColorDialog* CreationVentilateurCouleurA = new QColorDialog(Qt::cyan, m_CreationVentilateur);
    CreationVentilateurCouleurA->hide();
    CreationVentilateurCouleurA->setObjectName("CreationVentilateurCouleurA");
    connect(CreationVentilateurAfficherCouleurA, SIGNAL(clicked()), CreationVentilateurCouleurA, SLOT(show()));

    QPushButton* CreationVentilateurAfficherCouleurC = new QPushButton("Choisir la couleur de la face de gauche");
    CreationVentilateurLayout->addRow(CreationVentilateurAfficherCouleurC);
    QColorDialog* CreationVentilateurCouleurC = new QColorDialog(Qt::magenta, m_CreationVentilateur);
    CreationVentilateurCouleurC->hide();
    CreationVentilateurCouleurC->setObjectName("CreationVentilateurCouleurC");
    connect(CreationVentilateurAfficherCouleurC, SIGNAL(clicked()), CreationVentilateurCouleurC, SLOT(show()));

    QPushButton* CreationVentilateurAfficherCouleurD = new QPushButton("Choisir la couleur de la face de droite");
    CreationVentilateurLayout->addRow(CreationVentilateurAfficherCouleurD);
    QColorDialog* CreationVentilateurCouleurD = new QColorDialog(Qt::red, m_CreationVentilateur);
    CreationVentilateurCouleurD->hide();
    CreationVentilateurCouleurD->setObjectName("CreationVentilateurCouleurD");
    connect(CreationVentilateurAfficherCouleurD, SIGNAL(clicked()), CreationVentilateurCouleurD, SLOT(show()));

    QPushButton* CreationVentilateurAfficherCouleurE = new QPushButton("Choisir la couleur de la face du fond");
    CreationVentilateurLayout->addRow(CreationVentilateurAfficherCouleurE);
    QColorDialog* CreationVentilateurCouleurE = new QColorDialog(Qt::green, m_CreationVentilateur);
    CreationVentilateurCouleurE->hide();
    CreationVentilateurCouleurE->setObjectName("CreationVentilateurCouleurE");
    connect(CreationVentilateurAfficherCouleurE, SIGNAL(clicked()), CreationVentilateurCouleurE, SLOT(show()));

    QPushButton* CreationVentilateurAfficherCouleurF = new QPushButton("Choisir la couleur de la face de devant");
    CreationVentilateurLayout->addRow(CreationVentilateurAfficherCouleurF);
    QColorDialog* CreationVentilateurCouleurF = new QColorDialog(Qt::blue, m_CreationVentilateur);
    CreationVentilateurCouleurF->hide();
    CreationVentilateurCouleurF->setObjectName("CreationVentilateurCouleurF");
    connect(CreationVentilateurAfficherCouleurF, SIGNAL(clicked()), CreationVentilateurCouleurF, SLOT(show()));

    QDoubleSpinBox* CreationVentilateurRebond = new QDoubleSpinBox;
    CreationVentilateurRebond->setObjectName("CreationVentilateurRebond");
    CreationVentilateurRebond->setValue(0.8);
    CreationVentilateurRebond->setMaximum(1);
    CreationVentilateurRebond->setMinimum(0);
    CreationVentilateurRebond->setDecimals(4);
    CreationVentilateurRebond->setSingleStep(0.05);
    CreationVentilateurRebond->setAccelerated(true);
    CreationVentilateurLayout->addRow("Coefficient de rebonds : ", CreationVentilateurRebond);

    QDoubleSpinBox* CreationVentilateurIntensite = new QDoubleSpinBox;
    CreationVentilateurIntensite->setObjectName("CreationVentilateurIntensite");
    CreationVentilateurIntensite->setValue(3.5);
    CreationVentilateurIntensite->setMaximum(5);
    CreationVentilateurIntensite->setMinimum(0);
    CreationVentilateurIntensite->setDecimals(4);
    CreationVentilateurIntensite->setSingleStep(0.05);
    CreationVentilateurIntensite->setAccelerated(true);
    CreationVentilateurLayout->addRow("Intensit� : ", CreationVentilateurIntensite);

    QPushButton* CreationVentilateurButton = new QPushButton("Cr�er le Ventilateur !");
    CreationVentilateurLayout->addRow(CreationVentilateurButton);

    QPushButton* CreationVentilateurAnnuler = new QPushButton("Annuler");
    CreationVentilateurLayout->addRow(CreationVentilateurAnnuler);

    m_CreationVentilateur->setLayout(CreationVentilateurLayout);

    connect(CreationVentilateurButton, SIGNAL(clicked()), m_CreationVentilateur, SLOT(hide()));
    connect(CreationVentilateurButton, SIGNAL(clicked()), m_CreationVentilateur->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));

    connect(CreationVentilateurAnnuler, SIGNAL(clicked()), m_CreationVentilateur, SLOT(hide()));
    connect(CreationVentilateurAnnuler, SIGNAL(clicked()), m_CreationVentilateur->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));


    connect(CreationVentilateurButton, SIGNAL(clicked()), this, SLOT(creeVentilateur()));
}


void AjouteElementDock::genereCreationTrampoline()
{
    QLayout* par = m_CreationTrampoline->parentWidget()->layout();
    delete m_CreationTrampoline;

    m_CreationTrampoline = new QGroupBox("Cr�ation d'un Trampoline");

    par->addWidget(m_CreationTrampoline);

    QFormLayout* CreationTrampolineLayout = new QFormLayout;


    QWidget* CreationTrampolineOrigine = new QWidget;
    QHBoxLayout* CreationTrampolineOrigineLayout = new QHBoxLayout;
    CreationTrampolineOrigine->setLayout(CreationTrampolineOrigineLayout);

    QDoubleSpinBox* CreationTrampolineOrigineX = new QDoubleSpinBox;
    CreationTrampolineOrigineX->setObjectName("CreationTrampolineOrigineX");
    CreationTrampolineOrigineX->setValue(0);
    CreationTrampolineOrigineX->setDecimals(4);
    CreationTrampolineOrigineX->setMaximum(1000);
    CreationTrampolineOrigineX->setMinimum(-1000);
    CreationTrampolineOrigineX->setSingleStep(0.05);
    CreationTrampolineOrigineX->setAccelerated(true);
    CreationTrampolineOrigineLayout->addWidget(CreationTrampolineOrigineX);

    QDoubleSpinBox* CreationTrampolineOrigineY = new QDoubleSpinBox;
    CreationTrampolineOrigineY->setObjectName("CreationTrampolineOrigineY");
    CreationTrampolineOrigineY->setValue(0);
    CreationTrampolineOrigineY->setMaximum(1000);
    CreationTrampolineOrigineY->setMinimum(-1000);
    CreationTrampolineOrigineY->setDecimals(4);
    CreationTrampolineOrigineY->setSingleStep(0.05);
    CreationTrampolineOrigineY->setAccelerated(true);
    CreationTrampolineOrigineLayout->addWidget(CreationTrampolineOrigineY);

    QDoubleSpinBox* CreationTrampolineOrigineZ = new QDoubleSpinBox;
    CreationTrampolineOrigineZ->setObjectName("CreationTrampolineOrigineZ");
    CreationTrampolineOrigineZ->setValue(0);
    CreationTrampolineOrigineZ->setDecimals(4);
    CreationTrampolineOrigineZ->setMaximum(1000);
    CreationTrampolineOrigineZ->setMinimum(-1000);
    CreationTrampolineOrigineZ->setSingleStep(0.05);
    CreationTrampolineOrigineZ->setAccelerated(true);
    CreationTrampolineOrigineLayout->addWidget(CreationTrampolineOrigineZ);

    CreationTrampolineLayout->addRow("Origine : ", CreationTrampolineOrigine);


    QWidget* CreationTrampolineLongueur = new QWidget;
    QHBoxLayout* CreationTrampolineLongueurLayout = new QHBoxLayout;
    CreationTrampolineLongueur->setLayout(CreationTrampolineLongueurLayout);

    QDoubleSpinBox* CreationTrampolineLongueurX = new QDoubleSpinBox;
    CreationTrampolineLongueurX->setObjectName("CreationTrampolineLongueurX");
    CreationTrampolineLongueurX->setValue(1);
    CreationTrampolineLongueurX->setDecimals(4);
    CreationTrampolineLongueurX->setMaximum(1000);
    CreationTrampolineLongueurX->setMinimum(-1000);
    CreationTrampolineLongueurX->setSingleStep(0.05);
    CreationTrampolineLongueurX->setAccelerated(true);
    CreationTrampolineLongueurLayout->addWidget(CreationTrampolineLongueurX);

    QDoubleSpinBox* CreationTrampolineLongueurY = new QDoubleSpinBox;
    CreationTrampolineLongueurY->setObjectName("CreationTrampolineLongueurY");
    CreationTrampolineLongueurY->setValue(0);
    CreationTrampolineLongueurY->setDecimals(4);
    CreationTrampolineLongueurY->setMaximum(1000);
    CreationTrampolineLongueurY->setMinimum(-1000);
    CreationTrampolineLongueurY->setSingleStep(0.05);
    CreationTrampolineLongueurY->setAccelerated(true);
    CreationTrampolineLongueurLayout->addWidget(CreationTrampolineLongueurY);

    QDoubleSpinBox* CreationTrampolineLongueurZ = new QDoubleSpinBox;
    CreationTrampolineLongueurZ->setObjectName("CreationTrampolineLongueurZ");
    CreationTrampolineLongueurZ->setValue(0);
    CreationTrampolineLongueurZ->setMaximum(1000);
    CreationTrampolineLongueurZ->setMinimum(-1000);
    CreationTrampolineLongueurZ->setDecimals(4);
    CreationTrampolineLongueurZ->setSingleStep(0.05);
    CreationTrampolineLongueurZ->setAccelerated(true);
    CreationTrampolineLongueurLayout->addWidget(CreationTrampolineLongueurZ);

    CreationTrampolineLayout->addRow("Longueur : ", CreationTrampolineLongueur);

    QWidget* CreationTrampolineLargeur = new QWidget;
    QHBoxLayout* CreationTrampolineLargeurLayout = new QHBoxLayout;
    CreationTrampolineLargeur->setLayout(CreationTrampolineLargeurLayout);

    QDoubleSpinBox* CreationTrampolineLargeurX = new QDoubleSpinBox;
    CreationTrampolineLargeurX->setObjectName("CreationTrampolineLargeurX");
    CreationTrampolineLargeurX->setValue(0);
    CreationTrampolineLargeurX->setDecimals(4);
    CreationTrampolineLargeurX->setMaximum(1000);
    CreationTrampolineLargeurX->setMinimum(-1000);
    CreationTrampolineLargeurX->setSingleStep(0.05);
    CreationTrampolineLargeurX->setAccelerated(true);
    CreationTrampolineLargeurLayout->addWidget(CreationTrampolineLargeurX);

    QDoubleSpinBox* CreationTrampolineLargeurY = new QDoubleSpinBox;
    CreationTrampolineLargeurY->setObjectName("CreationTrampolineLargeurY");
    CreationTrampolineLargeurY->setValue(1);
    CreationTrampolineLargeurY->setDecimals(4);
    CreationTrampolineLargeurY->setMaximum(1000);
    CreationTrampolineLargeurY->setMinimum(-1000);
    CreationTrampolineLargeurY->setSingleStep(0.05);
    CreationTrampolineLargeurY->setAccelerated(true);
    CreationTrampolineLargeurLayout->addWidget(CreationTrampolineLargeurY);

    QDoubleSpinBox* CreationTrampolineLargeurZ = new QDoubleSpinBox;
    CreationTrampolineLargeurZ->setObjectName("CreationTrampolineLargeurZ");
    CreationTrampolineLargeurZ->setValue(0);
    CreationTrampolineLargeurZ->setMaximum(1000);
    CreationTrampolineLargeurZ->setMinimum(-1000);
    CreationTrampolineLargeurZ->setDecimals(4);
    CreationTrampolineLargeurZ->setSingleStep(0.05);
    CreationTrampolineLargeurZ->setAccelerated(true);
    CreationTrampolineLargeurLayout->addWidget(CreationTrampolineLargeurZ);

    CreationTrampolineLayout->addRow("Largeur : ", CreationTrampolineLargeur);



    QDoubleSpinBox* CreationTrampolineHauteur = new QDoubleSpinBox;
    CreationTrampolineHauteur->setObjectName("CreationTrampolineHauteur");
    CreationTrampolineHauteur->setValue(0.1);
    CreationTrampolineHauteur->setMaximum(1000);
    CreationTrampolineHauteur->setMinimum(0);
    CreationTrampolineHauteur->setDecimals(4);
    CreationTrampolineHauteur->setSingleStep(0.05);
    CreationTrampolineHauteur->setAccelerated(true);
    CreationTrampolineLayout->addRow("Hauteur : ", CreationTrampolineHauteur);

    QPushButton* CreationTrampolineAfficherCouleurA = new QPushButton("Choisir la couleur de la face sup�rieure (rebondissante)");
    CreationTrampolineLayout->addRow(CreationTrampolineAfficherCouleurA);
    QColorDialog* CreationTrampolineCouleurA = new QColorDialog(Qt::cyan, m_CreationTrampoline);
    CreationTrampolineCouleurA->hide();
    CreationTrampolineCouleurA->setObjectName("CreationTrampolineCouleurA");
    connect(CreationTrampolineAfficherCouleurA, SIGNAL(clicked()), CreationTrampolineCouleurA, SLOT(show()));

    QPushButton* CreationTrampolineAfficherCouleurB = new QPushButton("Choisir la couleur de la face inf�rieure");
    CreationTrampolineLayout->addRow(CreationTrampolineAfficherCouleurB);
    QColorDialog* CreationTrampolineCouleurB = new QColorDialog(Qt::yellow, m_CreationTrampoline);
    CreationTrampolineCouleurB->hide();
    CreationTrampolineCouleurB->setObjectName("CreationTrampolineCouleurB");
    connect(CreationTrampolineAfficherCouleurB, SIGNAL(clicked()), CreationTrampolineCouleurB, SLOT(show()));



    QPushButton* CreationTrampolineAfficherCouleurC = new QPushButton("Choisir la couleur de la face de gauche");
    CreationTrampolineLayout->addRow(CreationTrampolineAfficherCouleurC);
    QColorDialog* CreationTrampolineCouleurC = new QColorDialog(Qt::magenta, m_CreationTrampoline);
    CreationTrampolineCouleurC->hide();
    CreationTrampolineCouleurC->setObjectName("CreationTrampolineCouleurC");
    connect(CreationTrampolineAfficherCouleurC, SIGNAL(clicked()), CreationTrampolineCouleurC, SLOT(show()));

    QPushButton* CreationTrampolineAfficherCouleurD = new QPushButton("Choisir la couleur de la face de droite");
    CreationTrampolineLayout->addRow(CreationTrampolineAfficherCouleurD);
    QColorDialog* CreationTrampolineCouleurD = new QColorDialog(Qt::red, m_CreationTrampoline);
    CreationTrampolineCouleurD->hide();
    CreationTrampolineCouleurD->setObjectName("CreationTrampolineCouleurD");
    connect(CreationTrampolineAfficherCouleurD, SIGNAL(clicked()), CreationTrampolineCouleurD, SLOT(show()));

    QPushButton* CreationTrampolineAfficherCouleurE = new QPushButton("Choisir la couleur de la face du fond");
    CreationTrampolineLayout->addRow(CreationTrampolineAfficherCouleurE);
    QColorDialog* CreationTrampolineCouleurE = new QColorDialog(Qt::green, m_CreationTrampoline);
    CreationTrampolineCouleurE->hide();
    CreationTrampolineCouleurE->setObjectName("CreationTrampolineCouleurE");
    connect(CreationTrampolineAfficherCouleurE, SIGNAL(clicked()), CreationTrampolineCouleurE, SLOT(show()));

    QPushButton* CreationTrampolineAfficherCouleurF = new QPushButton("Choisir la couleur de la face de devant");
    CreationTrampolineLayout->addRow(CreationTrampolineAfficherCouleurF);
    QColorDialog* CreationTrampolineCouleurF = new QColorDialog(Qt::blue, m_CreationTrampoline);
    CreationTrampolineCouleurF->hide();
    CreationTrampolineCouleurF->setObjectName("CreationTrampolineCouleurF");
    connect(CreationTrampolineAfficherCouleurF, SIGNAL(clicked()), CreationTrampolineCouleurF, SLOT(show()));

    QDoubleSpinBox* CreationTrampolineRebond = new QDoubleSpinBox;
    CreationTrampolineRebond->setObjectName("CreationTrampolineRebond");
    CreationTrampolineRebond->setValue(1.2);
    CreationTrampolineRebond->setMaximum(10);
    CreationTrampolineRebond->setMinimum(1);
    CreationTrampolineRebond->setDecimals(4);
    CreationTrampolineRebond->setSingleStep(0.05);
    CreationTrampolineRebond->setAccelerated(true);
    CreationTrampolineLayout->addRow("Coefficient de rebonds : ", CreationTrampolineRebond);

    QPushButton* CreationTrampolineButton = new QPushButton("Cr�er le Trampoline !");
    CreationTrampolineLayout->addRow(CreationTrampolineButton);

    QPushButton* CreationTrampolineAnnuler = new QPushButton("Annuler");
    CreationTrampolineLayout->addRow(CreationTrampolineAnnuler);

    m_CreationTrampoline->setLayout(CreationTrampolineLayout);

    connect(CreationTrampolineButton, SIGNAL(clicked()), m_CreationTrampoline, SLOT(hide()));
    connect(CreationTrampolineButton, SIGNAL(clicked()), m_CreationTrampoline->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));

    connect(CreationTrampolineAnnuler, SIGNAL(clicked()), m_CreationTrampoline, SLOT(hide()));
    connect(CreationTrampolineAnnuler, SIGNAL(clicked()), m_CreationTrampoline->parentWidget()->findChild<QWidget*>("addElem1"), SLOT(show()));


    connect(CreationTrampolineButton, SIGNAL(clicked()), this, SLOT(creeTrampoline()));
}



void AjouteElementDock::creeBalle()
{
	// On r�cup�re les infos de genereCreationBalle() et on cree la Balle3D. Idem pour toutes les m�thodes de cree##()
    QDoubleSpinBox* PX = m_CreationBalle->findChild<QDoubleSpinBox*>("CreationBallePositionX");
    QDoubleSpinBox* PY = m_CreationBalle->findChild<QDoubleSpinBox*>("CreationBallePositionY");
    QDoubleSpinBox* PZ = m_CreationBalle->findChild<QDoubleSpinBox*>("CreationBallePositionZ");

    QDoubleSpinBox* VX = m_CreationBalle->findChild<QDoubleSpinBox*>("CreationBalleVitesseX");
    QDoubleSpinBox* VY = m_CreationBalle->findChild<QDoubleSpinBox*>("CreationBalleVitesseY");
    QDoubleSpinBox* VZ = m_CreationBalle->findChild<QDoubleSpinBox*>("CreationBalleVitesseZ");

    QDoubleSpinBox* R = m_CreationBalle->findChild<QDoubleSpinBox*>("CreationBalleRayon");

    QDoubleSpinBox* MV = m_CreationBalle->findChild<QDoubleSpinBox*>("CreationBalleMasseVolumique");

    QColorDialog* C = m_CreationBalle->findChild<QColorDialog*>("CreationBalleCouleur");
    Couleur COU(C->currentColor().red()/255., C->currentColor().green()/255., C->currentColor().blue()/255., C->currentColor().alpha()/255.);

    m_systeme->ajoute(new Balle3D( {PX->value(), PY->value(), PZ->value()}, {VX->value(),VY->value(),VZ->value()},R->value(),MV->value(), Vecteur(3), COU ));

}

void AjouteElementDock::creePendule()
{

    QDoubleSpinBox* AX = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleAttacheX");
    QDoubleSpinBox* AY = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleAttacheY");
    QDoubleSpinBox* AZ = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleAttacheZ");

    QDoubleSpinBox* O = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleAngle");
    QDoubleSpinBox* OP = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleVitesseAngulaire");

    QDoubleSpinBox* L = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleLongueur");

    QDoubleSpinBox* R = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleRayon");

    QDoubleSpinBox* MV = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleMasseVolumique");

    QDoubleSpinBox* F = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleFrottement");

    QDoubleSpinBox* NX = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleNormaleX");
    QDoubleSpinBox* NY = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleNormaleY");
    QDoubleSpinBox* NZ = m_CreationPendule->findChild<QDoubleSpinBox*>("CreationPenduleNormaleZ");

    QColorDialog* C = m_CreationPendule->findChild<QColorDialog*>("CreationPenduleCouleur");
    Couleur COU(C->currentColor().red()/255., C->currentColor().green()/255., C->currentColor().blue()/255., C->currentColor().alpha()/255.);

    m_systeme->ajoute(new Pendule3D( {O->value()}, {OP->value()},R->value(),MV->value(),Vecteur(3),L->value(),F->value(), {NX->value(),NY->value(),NZ->value()}, {AX->value(),AY->value(),AZ->value()}, COU ));


}

void AjouteElementDock::creeRessort()
{

    QDoubleSpinBox* AX = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortAttacheX");
    QDoubleSpinBox* AY = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortAttacheY");
    QDoubleSpinBox* AZ = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortAttacheZ");

    QDoubleSpinBox* O = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortOmega");
    QDoubleSpinBox* OP = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortOmegaPoint");

    QDoubleSpinBox* RA = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortRaideur");

    QDoubleSpinBox* R = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortRayon");

    QDoubleSpinBox* MV = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortMasseVolumique");

    QDoubleSpinBox* F = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortFrottement");

    QDoubleSpinBox* DX = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortDirectionX");
    QDoubleSpinBox* DY = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortDirectionY");
    QDoubleSpinBox* DZ = m_CreationRessort->findChild<QDoubleSpinBox*>("CreationRessortDirectionZ");

    QColorDialog* C = m_CreationRessort->findChild<QColorDialog*>("CreationRessortCouleur");
    Couleur COU(C->currentColor().red()/255., C->currentColor().green()/255., C->currentColor().blue()/255., C->currentColor().alpha()/255.);

    m_systeme->ajoute(new Ressort3D( {O->value()}, {OP->value()},R->value(),MV->value(),Vecteur(3),RA->value(),F->value(), {DX->value(),DY->value(),DZ->value()}, {AX->value(),AY->value(),AZ->value()},COU));

}

void AjouteElementDock::creePlan()
{

    QDoubleSpinBox* OX = m_CreationPlan->findChild<QDoubleSpinBox*>("CreationPlanOrigineX");
    QDoubleSpinBox* OY = m_CreationPlan->findChild<QDoubleSpinBox*>("CreationPlanOrigineY");
    QDoubleSpinBox* OZ = m_CreationPlan->findChild<QDoubleSpinBox*>("CreationPlanOrigineZ");

    QDoubleSpinBox* NX = m_CreationPlan->findChild<QDoubleSpinBox*>("CreationPlanNormaleX");
    QDoubleSpinBox* NY = m_CreationPlan->findChild<QDoubleSpinBox*>("CreationPlanNormaleY");
    QDoubleSpinBox* NZ = m_CreationPlan->findChild<QDoubleSpinBox*>("CreationPlanNormaleZ");

    QDoubleSpinBox* R = m_CreationPlan->findChild<QDoubleSpinBox*>("CreationPlanRebond");

    QColorDialog* C = m_CreationPlan->findChild<QColorDialog*>("CreationPlanCouleur");
    Couleur COU(C->currentColor().red()/255., C->currentColor().green()/255., C->currentColor().blue()/255., C->currentColor().alpha()/255.);

    m_systeme->ajoute(new Plan3D( {OX->value(),OY->value(),OZ->value()}, {NX->value(),NY->value(),NZ->value()}, COU, R->value()));


}

void AjouteElementDock::creePlanFini()
{

    QDoubleSpinBox* OX = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniOrigineX");
    QDoubleSpinBox* OY = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniOrigineY");
    QDoubleSpinBox* OZ = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniOrigineZ");

    QDoubleSpinBox* NX = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniNormaleX");
    QDoubleSpinBox* NY = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniNormaleY");
    QDoubleSpinBox* NZ = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniNormaleZ");

    QDoubleSpinBox* LOX = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniLongueurX");
    QDoubleSpinBox* LOY = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniLongueurY");
    QDoubleSpinBox* LOZ = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniLongueurZ");

    QDoubleSpinBox* LAX = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniLargeurX");
    QDoubleSpinBox* LAY = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniLargeurY");
    QDoubleSpinBox* LAZ = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniLargeurZ");

    QDoubleSpinBox* R = m_CreationPlanFini->findChild<QDoubleSpinBox*>("CreationPlanFiniRebond");

    QColorDialog* C = m_CreationPlanFini->findChild<QColorDialog*>("CreationPlanFiniCouleur");
    Couleur COU(C->currentColor().red()/255., C->currentColor().green()/255., C->currentColor().blue()/255., C->currentColor().alpha()/255.);

    m_systeme->ajoute(new PlanFini3D( {OX->value(),OY->value(),OZ->value()}, {NX->value(),NY->value(),NZ->value()}, {LOX->value(),LOY->value(),LOZ->value()}, {LAX->value(),LAY->value(),LAZ->value()}, COU, R->value()));


}

void AjouteElementDock::creeBrique()
{

    QDoubleSpinBox* OX = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueOrigineX");
    QDoubleSpinBox* OY = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueOrigineY");
    QDoubleSpinBox* OZ = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueOrigineZ");

    QDoubleSpinBox* LOX = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueLongueurX");
    QDoubleSpinBox* LOY = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueLongueurY");
    QDoubleSpinBox* LOZ = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueLongueurZ");

    QDoubleSpinBox* LAX = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueLargeurX");
    QDoubleSpinBox* LAY = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueLargeurY");
    QDoubleSpinBox* LAZ = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueLargeurZ");

    QDoubleSpinBox* H = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueHauteur");

    QDoubleSpinBox* R = m_CreationBrique->findChild<QDoubleSpinBox*>("CreationBriqueRebond");

    QColorDialog* CA = m_CreationBrique->findChild<QColorDialog*>("CreationBriqueCouleurA");
    QColorDialog* CB = m_CreationBrique->findChild<QColorDialog*>("CreationBriqueCouleurB");
    QColorDialog* CC = m_CreationBrique->findChild<QColorDialog*>("CreationBriqueCouleurC");
    QColorDialog* CD = m_CreationBrique->findChild<QColorDialog*>("CreationBriqueCouleurD");
    QColorDialog* CE = m_CreationBrique->findChild<QColorDialog*>("CreationBriqueCouleurE");
    QColorDialog* CF = m_CreationBrique->findChild<QColorDialog*>("CreationBriqueCouleurF");

    Couleur COUA(CA->currentColor().red()/255., CA->currentColor().green()/255., CA->currentColor().blue()/255., CA->currentColor().alpha()/255.);
    Couleur COUB(CB->currentColor().red()/255., CB->currentColor().green()/255., CB->currentColor().blue()/255., CB->currentColor().alpha()/255.);
    Couleur COUC(CC->currentColor().red()/255., CC->currentColor().green()/255., CC->currentColor().blue()/255., CC->currentColor().alpha()/255.);
    Couleur COUD(CD->currentColor().red()/255., CD->currentColor().green()/255., CD->currentColor().blue()/255., CD->currentColor().alpha()/255.);
    Couleur COUE(CE->currentColor().red()/255., CE->currentColor().green()/255., CE->currentColor().blue()/255., CE->currentColor().alpha()/255.);
    Couleur COUF(CF->currentColor().red()/255., CF->currentColor().green()/255., CF->currentColor().blue()/255., CF->currentColor().alpha()/255.);

    m_systeme->ajoute(new Brique3D( {OX->value(),OY->value(),OZ->value()}, {LOX->value(),LOY->value(),LOZ->value()}, {LAX->value(),LAY->value(),LAZ->value()},H->value(), COUA, COUB, COUC, COUD, COUE, COUF, R->value()));



}

void AjouteElementDock::creeVent()
{
    QDoubleSpinBox* OX = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentOrigineX");
    QDoubleSpinBox* OY = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentOrigineY");
    QDoubleSpinBox* OZ = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentOrigineZ");

    QDoubleSpinBox* NX = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentNormaleX");
    QDoubleSpinBox* NY = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentNormaleY");
    QDoubleSpinBox* NZ = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentNormaleZ");

    QDoubleSpinBox* LOX = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentLongueurX");
    QDoubleSpinBox* LOY = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentLongueurY");
    QDoubleSpinBox* LOZ = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentLongueurZ");

    QDoubleSpinBox* LAX = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentLargeurX");
    QDoubleSpinBox* LAY = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentLargeurY");
    QDoubleSpinBox* LAZ = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentLargeurZ");

    QDoubleSpinBox* P = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentProfondeur");

    QDoubleSpinBox* I = m_CreationVent->findChild<QDoubleSpinBox*>("CreationVentIntensite");

    m_systeme->ajoute(new Vent( {OX->value(),OY->value(),OZ->value()}, {NX->value(),NY->value(),NZ->value()}, {LOX->value(),LOY->value(),LOZ->value()}, {LAX->value(),LAY->value(),LAZ->value()},P->value(),I->value()));
}

void AjouteElementDock::creeChampForcesGlobal()
{
    QDoubleSpinBox* IX = m_CreationChampForcesGlobal->findChild<QDoubleSpinBox*>("CreationChampForcesGlobalIntensiteX");
    QDoubleSpinBox* IY = m_CreationChampForcesGlobal->findChild<QDoubleSpinBox*>("CreationChampForcesGlobalIntensiteY");
    QDoubleSpinBox* IZ = m_CreationChampForcesGlobal->findChild<QDoubleSpinBox*>("CreationChampForcesGlobalIntensiteZ");


    m_systeme->ajoute(new ChampForcesGlobal( {IX->value(), IY->value(), IZ->value()}) );

}

void AjouteElementDock::creeVentilateur()
{
    QDoubleSpinBox* OX = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurOrigineX");
    QDoubleSpinBox* OY = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurOrigineY");
    QDoubleSpinBox* OZ = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurOrigineZ");

    QDoubleSpinBox* NX = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurNormaleX");
    QDoubleSpinBox* NY = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurNormaleY");
    QDoubleSpinBox* NZ = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurNormaleZ");

    QDoubleSpinBox* LOX = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurLongueurX");
    QDoubleSpinBox* LOY = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurLongueurY");
    QDoubleSpinBox* LOZ = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurLongueurZ");

    QDoubleSpinBox* LAX = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurLargeurX");
    QDoubleSpinBox* LAY = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurLargeurY");
    QDoubleSpinBox* LAZ = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurLargeurZ");

    QDoubleSpinBox* H = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurHauteur");

    QDoubleSpinBox* P = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurProfondeur");

    QDoubleSpinBox* R = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurRebond");

    QDoubleSpinBox* I = m_CreationVentilateur->findChild<QDoubleSpinBox*>("CreationVentilateurIntensite");

    QColorDialog* CA = m_CreationVentilateur->findChild<QColorDialog*>("CreationVentilateurCouleurA");
    QColorDialog* CB = m_CreationVentilateur->findChild<QColorDialog*>("CreationVentilateurCouleurB");
    QColorDialog* CC = m_CreationVentilateur->findChild<QColorDialog*>("CreationVentilateurCouleurC");
    QColorDialog* CD = m_CreationVentilateur->findChild<QColorDialog*>("CreationVentilateurCouleurD");
    QColorDialog* CE = m_CreationVentilateur->findChild<QColorDialog*>("CreationVentilateurCouleurE");
    QColorDialog* CF = m_CreationVentilateur->findChild<QColorDialog*>("CreationVentilateurCouleurF");

    Couleur COUA(CA->currentColor().red()/255., CA->currentColor().green()/255., CA->currentColor().blue()/255., CA->currentColor().alpha()/255.);
    Couleur COUB(CB->currentColor().red()/255., CB->currentColor().green()/255., CB->currentColor().blue()/255., CB->currentColor().alpha()/255.);
    Couleur COUC(CC->currentColor().red()/255., CC->currentColor().green()/255., CC->currentColor().blue()/255., CC->currentColor().alpha()/255.);
    Couleur COUD(CD->currentColor().red()/255., CD->currentColor().green()/255., CD->currentColor().blue()/255., CD->currentColor().alpha()/255.);
    Couleur COUE(CE->currentColor().red()/255., CE->currentColor().green()/255., CE->currentColor().blue()/255., CE->currentColor().alpha()/255.);
    Couleur COUF(CF->currentColor().red()/255., CF->currentColor().green()/255., CF->currentColor().blue()/255., CF->currentColor().alpha()/255.);

    m_systeme->ajoute(new Ventilateur3D( {OX->value(), OY->value(), OZ->value()}, {NX->value(),NY->value(),NZ->value()}, {LOX->value(),LOY->value(),LOZ->value()}, {LAX->value(),LAY->value(),LAZ->value()}, H->value(), P->value(), COUA, COUB, COUC, COUD, COUE, COUF, R->value(), I->value() ));

}

void AjouteElementDock::creeTrampoline()
{
    QDoubleSpinBox* OX = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineOrigineX");
    QDoubleSpinBox* OY = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineOrigineY");
    QDoubleSpinBox* OZ = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineOrigineZ");


    QDoubleSpinBox* LOX = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineLongueurX");
    QDoubleSpinBox* LOY = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineLongueurY");
    QDoubleSpinBox* LOZ = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineLongueurZ");

    QDoubleSpinBox* LAX = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineLargeurX");
    QDoubleSpinBox* LAY = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineLargeurY");
    QDoubleSpinBox* LAZ = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineLargeurZ");

    QDoubleSpinBox* H = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineHauteur");

    QDoubleSpinBox* R = m_CreationTrampoline->findChild<QDoubleSpinBox*>("CreationTrampolineRebond");


    QColorDialog* CA = m_CreationTrampoline->findChild<QColorDialog*>("CreationTrampolineCouleurA");
    QColorDialog* CB = m_CreationTrampoline->findChild<QColorDialog*>("CreationTrampolineCouleurB");
    QColorDialog* CC = m_CreationTrampoline->findChild<QColorDialog*>("CreationTrampolineCouleurC");
    QColorDialog* CD = m_CreationTrampoline->findChild<QColorDialog*>("CreationTrampolineCouleurD");
    QColorDialog* CE = m_CreationTrampoline->findChild<QColorDialog*>("CreationTrampolineCouleurE");
    QColorDialog* CF = m_CreationTrampoline->findChild<QColorDialog*>("CreationTrampolineCouleurF");

    Couleur COUA(CA->currentColor().red()/255., CA->currentColor().green()/255., CA->currentColor().blue()/255., CA->currentColor().alpha()/255.);
    Couleur COUB(CB->currentColor().red()/255., CB->currentColor().green()/255., CB->currentColor().blue()/255., CB->currentColor().alpha()/255.);
    Couleur COUC(CC->currentColor().red()/255., CC->currentColor().green()/255., CC->currentColor().blue()/255., CC->currentColor().alpha()/255.);
    Couleur COUD(CD->currentColor().red()/255., CD->currentColor().green()/255., CD->currentColor().blue()/255., CD->currentColor().alpha()/255.);
    Couleur COUE(CE->currentColor().red()/255., CE->currentColor().green()/255., CE->currentColor().blue()/255., CE->currentColor().alpha()/255.);
    Couleur COUF(CF->currentColor().red()/255., CF->currentColor().green()/255., CF->currentColor().blue()/255., CF->currentColor().alpha()/255.);

    m_systeme->ajoute(new Trampoline3D( {OX->value(), OY->value(), OZ->value()}, {LOX->value(),LOY->value(),LOZ->value()}, {LAX->value(),LAY->value(),LAZ->value()}, H->value(), COUA, COUB, COUC, COUD, COUE, COUF, R->value() ));

}


