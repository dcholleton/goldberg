/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file gui.cpp
 * @brief D�finition des m�thodes de la classe GUI
 */

#include <GL/glu.h>
#include <QWidget>
#include <QString>
#include <QTimer>
#include <QtOpenGL>
#include <cmath>
#include "include/GUI/gui.h"
#include "include/Systeme.h"
#include "include/ObjetMobile.h"
#include "include/Obstacle.h"
#include "include/ChampForces.h"

using namespace std;

GUI::GUI(Systeme* systeme,int imagesParSecondes, QWidget* parent, char* nom)
    :QGLWidget(parent), m_systeme(systeme), h(5),theta(0), z(0.1), y(0)
{
    setWindowTitle(QString::fromUtf8(nom));
    if(imagesParSecondes == 0)
        m_Timer = NULL;
    else
    {
        int seconde = 1000; // 1 seconde = 1000 ms
        int timerInterval = seconde / imagesParSecondes;
        m_Timer = new QTimer(this);
        connect(m_Timer, SIGNAL(timeout()), this, SLOT(timeOutSlot()));
        m_Timer->setInterval(timerInterval);
    }

    connect(m_systeme, SIGNAL(newObjet(ObjetMobile*)), this, SLOT(updateGL()));
    connect(m_systeme, SIGNAL(newObstacle(Obstacle*)), this, SLOT(updateGL()));
    connect(m_systeme, SIGNAL(newChamp(ChampForces*)), this, SLOT(updateGL()));
}

void GUI::keyPressEvent(QKeyEvent* keyEvent)
{

    switch(keyEvent->key())
    {
        case Qt::Key_Up:
        {
            h-=0.1;
            break;
        }
        case Qt::Key_Z:
        {
            h-=0.1;
            break;
        }
        case Qt::Key_Down:
        {
            h+=0.1;
            break;
        }
        case Qt::Key_S:
        {
            h+=0.1;
            break;
        }
        case Qt::Key_Left:
        {
            theta-=0.1;
            break;
        }
        case Qt::Key_Q:
        {
            theta-=0.1;
            break;
        }
        case Qt::Key_Right:
        {
            theta+=0.1;
            break;
        }
        case Qt::Key_D:
        {
            theta+=0.1;
            break;
        }
        case Qt::Key_PageUp:
        {
            z+=0.1;
            break;
        }
        case Qt::Key_R:
        {
            z+=0.1;
            break;
        }
        case Qt::Key_PageDown:
        {
            z-=0.1;
            break;
        }
        case Qt::Key_F:
        {
            z-=0.1;
            break;
        }
        case Qt::Key_A:
        {
            y-=0.1;
            break;
        }
        case Qt::Key_E:
        {
            y+=0.1;
            break;
        }

        case Qt::Key_G:
        {
            if(m_Timer->isActive())
            {
                stopSimulation();
            }
            else
            {
                startSimulation();
            }
            break;
        }
        case Qt::Key_B:
        {
            QString test = QInputDialog::getText(this, "Coucou", "Comment tu vas ?");
            if (test=="Bien" || test == "bien")
            {
                QMessageBox::information(this, "Pseudo", "Tu m'en voit ravi");
            }
            if (test=="Mal" || test == "mal")
            {
                QMessageBox::information(this, "Pseudo", "Tant pis pour toi !");
            }
            break;
        }
    }

    updateGL();

}

void GUI::initializeGL()
{
    glShadeModel(GL_SMOOTH);
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClearDepth(1.0);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    //  glEnable(GL_LIGHTING);
    //int LightPos[4] = {0,1,5,0};
    //glLightiv(GL_LIGHT0,GL_AMBIENT,LightPos);
    //glEnable(GL_LIGHT0);

    glEnable(GL_COLOR_MATERIAL);
}

void GUI::resizeGL(int width, int height)
{
    if(height == 0)
        height = 1;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (GLfloat)width/(GLfloat)height, 0.1f, 100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void GUI::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();


    // fixe le point de vue
    gluLookAt(cos(theta)*h, sin(theta)*h+y, z*h,  // position de l'oeil
              0, y, 0,  // point vis�
              0, 0 , 1);

    glPushMatrix();
    glColor4d(1,0,0,1);
    glBegin(GL_LINES);
    glVertex3d(0,0,0);
    glVertex3d(1,0,0);
    glEnd();

    glColor4d(0,1,0,1);
    glBegin(GL_LINES);
    glVertex3d(0,0,0);
    glVertex3d(0,1,0);
    glEnd();

    glColor4d(0,0,1,1);
    glBegin(GL_LINES);
    glVertex3d(0,0,0);
    glVertex3d(0,0,1);
    glEnd();
    glPopMatrix();

    m_systeme->dessine();


}

void GUI::mousePressEvent (QMouseEvent * mouseEvent)
{
    setFocus();
}

void GUI::mouseDoubleClickEvent(QMouseEvent * mouseEvent)
{
    if (! m_Timer->isActive())
    {
        startSimulation();
    }
    else
    {
        stopSimulation();
    }
}

void GUI::mouseMoveEvent (QMouseEvent* move_event)
{


    double dx = move_event->x()/100. - lastPos.x()/100.   ;
    double dy = move_event->y()/100. - lastPos.y()/100.   ;
    if (dx > 0.5 || dx < -0.5)
    {
        dx = 0;
    }
    if (dy > 0.5 || dy < -0.5)
    {
        dy = 0;
    }
    theta -= dx;
    z += dy;
    updateGL();
    lastPos = move_event->pos();


}

void GUI::wheelEvent (QWheelEvent * wheel_event)
{
    h -= wheel_event->delta()/100;
    updateGL();
}

void GUI::timeOutSlot()
{
    m_systeme->evolue();
    updateGL();
}

void GUI::startSimulation()
{
    m_Timer->start();
    emit simulationStarted();
}

void GUI::stopSimulation()
{
    m_Timer->stop();
    emit simulationStopped();
}

void GUI::setImagesParSecondes(int nmbrImgSec)
{
    int seconde = 1000;
    int timerInterval = seconde / nmbrImgSec;
    m_Timer->setInterval(timerInterval);
}

Systeme* GUI::systeme()
{
    return m_systeme;
}

QTimer* GUI::timer() const
{
    return m_Timer;
}
