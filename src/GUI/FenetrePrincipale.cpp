/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file FenetrePrincipale.cpp
 * @brief D�finition des m�thodes de la classe FenetrePrincipale
 */
#include <QWidget>
#include <QLayout>
#include <QStandardItemModel>
#include <QStandardItem>
#include <vector>
#include <memory>
#include <QString>
#include "include/Systeme.h"
#include "include/GUI/gui.h"
#include "include/GUI/FenetrePrincipale.h"
#include "include/GUI/AjouteElementDock.h"
#include "include/ObjetMobile.h"
#include "include/Obstacle.h"
#include "include/ChampForces.h"
#include "include/Parseur.h"


using namespace std;

FenetrePrincipale::FenetrePrincipale(Systeme* systeme, char* nom, QWidget *parent) :
    QMainWindow(parent), m_model(0)
{
    setWindowTitle(nom);
    setWindowIcon(QIcon(":/icone.png"));

    // On g�n�re l'affichage graphique
    genereSimulation(systeme);
    setCentralWidget(m_simulation);
    m_simulation->setFocus();

    // On g�n�re le mod�le
    genereModel(systeme);

    // On redimensionne la fen�tre
    resize(1000,600);

    setWindowState(windowState() ^ Qt::WindowMaximized);

    // Cr�ation du Menu

    // Onglet "Fichier"
    QMenu* menuFichier = menuBar()->addMenu("&Fichier");

    QAction* actionNew = menuFichier->addAction("&Nouveau syst�me");
    actionNew->setIcon(QIcon(":/new.png"));
    actionNew->setShortcut(QKeySequence("Ctrl+O"));
    actionNew->setIconVisibleInMenu(true);
    actionNew->setStatusTip("Cr�� un nouveau syst�me");
    connect(actionNew, SIGNAL(triggered()), this, SLOT(nouveau()));

    QAction* actionOuvrir = menuFichier->addAction("&Ouvrir");
    actionOuvrir->setIcon(QIcon(":/open.png"));
    actionOuvrir->setShortcut(QKeySequence("Ctrl+O"));
    actionOuvrir->setIconVisibleInMenu(true);
    actionOuvrir->setStatusTip("Ouvre un syst�me existant");
    connect(actionOuvrir, SIGNAL(triggered()), this, SLOT(ouvrir()));

    QAction* actionEnregistrer = menuFichier->addAction("&Enregistrer");
    actionEnregistrer->setIcon(QIcon(":/save.png"));
    actionEnregistrer->setShortcut(QKeySequence("Ctrl+S"));
    actionEnregistrer->setIconVisibleInMenu(true);
    actionEnregistrer->setStatusTip("Enregistre le syst�me courant");
    connect(actionEnregistrer, SIGNAL(triggered()), this, SLOT(enregistrer()));

    // Bouton "Quitter"
    QAction* actionQuitter = menuFichier->addAction("&Quitter");
    actionQuitter->setIcon(QIcon(":/sortir.png"));
    actionQuitter->setShortcut(QKeySequence("Ctrl+Q"));
    actionQuitter->setIconVisibleInMenu(true);
    actionQuitter->setStatusTip("Quitte le programme");
    connect(actionQuitter, SIGNAL(triggered()), qApp, SLOT(quit()));

    // Onglet "Simulation"
    QMenu* menuSimulation = menuBar()->addMenu("&Simulation");

    // Bouton "Activer"
    QAction* actionStart = menuSimulation->addAction("&Activer le simulation");
    actionStart->setIcon(QIcon(":/start.png"));
    actionStart->setShortcut(QKeySequence("Ctrl+G"));
    actionStart->setIconVisibleInMenu(true);
    actionStart->setStatusTip("Active la simulation");
    connect(actionStart, SIGNAL(triggered()), m_simulation, SLOT(startSimulation()));
    connect(m_simulation, SIGNAL(simulationStarted(bool)), actionStart, SLOT(setDisabled(bool)));
    connect(m_simulation, SIGNAL(simulationStopped(bool)), actionStart, SLOT(setEnabled(bool)));

    // Bouton "Suspendre"
    QAction* actionStop = menuSimulation->addAction("&Suspendre la simulation");
    actionStop->setEnabled(false);
    actionStop->setIcon(QIcon(":/pause.png"));
    actionStop->setShortcut(QKeySequence("Ctrl+G"));
    actionStop->setIconVisibleInMenu(true);
    actionStop->setStatusTip("Suspends la simulation");
    connect(actionStop, SIGNAL(triggered()), m_simulation, SLOT(stopSimulation()));
    connect(m_simulation, SIGNAL(simulationStarted(bool)), actionStop, SLOT(setEnabled(bool)));
    connect(m_simulation, SIGNAL(simulationStopped(bool)), actionStop, SLOT(setDisabled(bool)));

    // Onglet "A Propos"
    QMenu* menuPropos = menuBar()->addMenu("� &Propos");

    QAction* actionQtinfo = menuPropos->addAction("� propos de Qt");
    actionQtinfo->setIcon(QIcon(":/qt.png"));
    actionQtinfo->setIconVisibleInMenu(true);
    actionQtinfo->setStatusTip("Cette interface � �t� cr��e avec Qt 4.8");
    connect(actionQtinfo, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

    // Cr�ation de la Barre d'Outils

    // Barre d'Outils "Fichier"
    QToolBar* barreOutilsFichier = addToolBar("Fichier");

    barreOutilsFichier->addAction(actionNew);
    barreOutilsFichier->addAction(actionOuvrir);
    barreOutilsFichier->addAction(actionEnregistrer);
    barreOutilsFichier->addAction(actionQuitter);


    // Barre d'Outils "Simulation"
    QToolBar* barreOutilsSimulation = addToolBar("Simulation");

    barreOutilsSimulation->addAction(actionStart);

    barreOutilsSimulation->addAction(actionStop);


    // Barre d'Outils "Temps"
    QToolBar* barreOutilsTemps = addToolBar("Temps");

    QLabel* imgSec = new QLabel(" Nombre d'int�grations par secondes : ");
    barreOutilsTemps->addWidget(imgSec);

    QSpinBox* nmbrImgSec = new QSpinBox();
    nmbrImgSec->setValue(50);
    nmbrImgSec->setAccelerated(true);
    nmbrImgSec->setMinimum(1);
    nmbrImgSec->setMaximum(1000);
    barreOutilsTemps->addWidget(nmbrImgSec);
    connect(nmbrImgSec, SIGNAL(valueChanged(int)), m_simulation, SLOT(setImagesParSecondes(int)));

    QLabel* integr = new QLabel("    Pas de temps de l'int�gration : ");
    barreOutilsTemps->addWidget(integr);

    QDoubleSpinBox* nmbrIntegr = new QDoubleSpinBox();
    nmbrIntegr->setValue(m_simulation->systeme()->dt());
    nmbrIntegr->setAccelerated(true);
    nmbrIntegr->setDecimals(4);
    nmbrIntegr->setMaximum(0.05);
    nmbrIntegr->setMinimum(0.001);
    nmbrIntegr->setSingleStep(0.001);
    barreOutilsTemps->addWidget(nmbrIntegr);
    connect(nmbrIntegr, SIGNAL(valueChanged(double)), m_simulation->systeme(), SLOT(set_dt(double)));

    QLabel* choixIntegr = new QLabel("    Int�grateur : ");
    barreOutilsTemps->addWidget(choixIntegr);

    QComboBox* typeIntegr = new QComboBox();
    typeIntegr->addItem("Euler");
    typeIntegr->addItem("Newmark");
    typeIntegr->addItem("Runge-Kutta");
    barreOutilsTemps->addWidget(typeIntegr);

    connect(typeIntegr, SIGNAL(currentIndexChanged(int)), m_simulation->systeme(), SLOT(set_integrateur(int)));

    // Cr�ation des Docks

    // Dock "Ajouter un �l�ment"

    AjouteElementDock* ajoutElement = new AjouteElementDock(m_simulation->systeme(), m_simulation, "Ajouter des �l�ments");
    addDockWidget(Qt::LeftDockWidgetArea, ajoutElement);

    // Dock "�tat du syst�me"
    QDockWidget* infoSyst = new QDockWidget("�tat du syst�me", this);
    infoSyst->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);
    addDockWidget(Qt::RightDockWidgetArea, infoSyst);
    QWidget* contenuInfoSyst = new QWidget;
    infoSyst->setWidget(contenuInfoSyst);
    QVBoxLayout* LayoutInfoSyst = new QVBoxLayout;

    // GroupBox "�l�ments du syst�me"
    QGroupBox* elements = new QGroupBox("�l�ments du syst�me");
    LayoutInfoSyst->addWidget(elements);

    QTreeView* vue = new QTreeView;
    vue->setObjectName("ElementsVue");
    vue->setModel( m_model);
    vue->setHeaderHidden(1);
    vue->setAnimated(true);
    vue->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QVBoxLayout* vueLayout = new QVBoxLayout;
    vueLayout->addWidget(vue);


    elements->setLayout(vueLayout);


    // GroupBox "Propri�t�s"
    m_proprietes = new QGroupBox("Propri�t�s");
    LayoutInfoSyst->addWidget(m_proprietes);

    connect(vue, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(generePropriete(QModelIndex)));


    contenuInfoSyst->setLayout(LayoutInfoSyst);



    // Cr�ation de la Barre d'Etat

    QStatusBar* barreEtat = statusBar();
    barreEtat->showMessage("Simulation initialis�e");



}

FenetrePrincipale::~FenetrePrincipale()
{
    delete m_model;
}

void FenetrePrincipale::ajouteObjet(ObjetMobile* obj)
{
    QStandardItem* objets = m_model->item(1);

    objets->appendRow(new QStandardItem( QString::fromStdString(obj->type()) ));
}

void FenetrePrincipale::ajouteObstacle(Obstacle* obst)
{
    QStandardItem* obstacles = m_model->item(2);

    obstacles->appendRow(new QStandardItem( QString::fromStdString(obst->type()) ));
}

void FenetrePrincipale::ajouteChamp(ChampForces* champ)
{
    QStandardItem* champs = m_model->item(3);

    champs->appendRow(new QStandardItem( QString::fromStdString(champ->type()) ));
}

void FenetrePrincipale::genereModel(Systeme* systeme)
{
    if(m_model == 0)
    {
        m_model = new QStandardItemModel(3,1);
    }


    QStandardItem* objets = new QStandardItem("Objets Mobiles");
    QStandardItem* obstacles = new QStandardItem("Obstacles");
    QStandardItem* champs = new QStandardItem("Champs de Forces");

    m_model->setItem(1, objets);
    m_model->setItem(2, obstacles);
    m_model->setItem(3, champs);

for (unique_ptr<ObjetMobile> const& obj : systeme->objets_mobiles())
    {
        objets->appendRow(new QStandardItem( QString::fromStdString(obj->type()) ));
    }
for (unique_ptr<Obstacle> const& obst : systeme->obstacles())
    {
        obstacles->appendRow(new QStandardItem( QString::fromStdString(obst->type()) ));
    }
for (unique_ptr<ChampForces> const& champ : systeme->champs())
    {
        champs->appendRow(new QStandardItem( QString::fromStdString(champ->type()) ));
    }

    connect(m_simulation->systeme(), SIGNAL(newObjet(ObjetMobile*)), this, SLOT(ajouteObjet(ObjetMobile*)));
    connect(m_simulation->systeme(), SIGNAL(newObstacle(Obstacle*)), this, SLOT(ajouteObstacle(Obstacle*)));
    connect(m_simulation->systeme(), SIGNAL(newChamp(ChampForces*)), this, SLOT(ajouteChamp(ChampForces*)));


}

void FenetrePrincipale::genereSimulation(Systeme *systeme)
{
    m_simulation = new GUI(systeme, 50, this);
}

void FenetrePrincipale::generePropriete(QModelIndex const& index)
{
    if(m_proprietes->layout())
    {
        QLayout* par = m_proprietes->parentWidget()->layout();
        delete m_proprietes;
        m_proprietes = new QGroupBox("Propri�t�s");
        par->addWidget(m_proprietes);
    }

    QFormLayout* proprietesLayout;

    QPushButton* suppr = new QPushButton("Supprimer");
    connect(suppr, SIGNAL(clicked()), this, SLOT(supprimeElement()));

    if(index.parent() != QModelIndex() )
    {
        switch (index.parent().row())
        {
            case 1:
            {
                proprietesLayout = m_simulation->systeme()->objets_mobiles()[index.row()]->proprietesLayout();
                proprietesLayout->addRow(suppr);
                break;
            }
            case 2:
            {
                proprietesLayout = m_simulation->systeme()->obstacles()[index.row()]->proprietesLayout();
                proprietesLayout->addRow(suppr);
                break;
            }
            case 3:
            {
                proprietesLayout = m_simulation->systeme()->champs()[index.row()]->proprietesLayout();
                proprietesLayout->addRow(suppr);
                break;
            }
        }

    }
    else
    {
        proprietesLayout = new QFormLayout;
        delete suppr;
    }




    m_proprietes->setLayout(proprietesLayout);
}

void FenetrePrincipale::supprimeElement()
{
    QModelIndex index = findChild<QTreeView*>("ElementsVue")->currentIndex();
    if(index.parent() != QModelIndex() )
    {
        switch (index.parent().row())
        {
            case 1:
            {
                m_simulation->systeme()->objets_mobiles().erase(m_simulation->systeme()->objets_mobiles().begin() + index.row());
                m_model->item(index.parent().row())->removeRow(index.row());
                break;
            }
            case 2:
            {
                m_simulation->systeme()->obstacles().erase(m_simulation->systeme()->obstacles().begin() + index.row());
                m_model->item(index.parent().row())->removeRow(index.row());
                break;
            }
            case 3:
            {
                m_simulation->systeme()->champs().erase(m_simulation->systeme()->champs().begin() + index.row());
                m_model->item(index.parent().row())->removeRow(index.row());
                break;
            }
        }
    }

    generePropriete();
    m_simulation->updateGL();

}


void FenetrePrincipale::ouvrir()
{


    if(m_simulation->timer()->isActive())
    {
        m_simulation->stopSimulation();
    }



    QString fichier = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Fichier XML (*.xml)");

    m_simulation->systeme()->reset();
    clearModel();



    Parseur mon_parseur(fichier.toStdString(), m_simulation->systeme());

    mon_parseur.extraire();

}

void FenetrePrincipale::enregistrer()
{
    QString fichier = QFileDialog::getSaveFileName(this, "Enregistrer un fichier", QString(), "Fichier XML (*.xml)");
    Parseur mon_parseur(fichier.toStdString(), m_simulation->systeme());
    mon_parseur.enregistrer();
}

void FenetrePrincipale::clearModel()
{
    m_model->clear();

    QStandardItem* objets = new QStandardItem("Objets Mobiles");
    QStandardItem* obstacles = new QStandardItem("Obstacles");
    QStandardItem* champs = new QStandardItem("Champs de Forces");

    m_model->setItem(1, objets);
    m_model->setItem(2, obstacles);
    m_model->setItem(3, champs);

}

void FenetrePrincipale::nouveau()
{
    if(m_simulation->timer()->isActive())
    {
        m_simulation->stopSimulation();
    }
    m_simulation->systeme()->reset();
    clearModel();
    m_simulation->updateGL();
}
