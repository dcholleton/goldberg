/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Balle3d.cpp
 * @brief D�finition des m�thodes de la classe Balle3D
 */
#include <GL/glu.h>
#include <QLineEdit>
#include <iostream>
#include <string>
#include "include/Vecteur.h"
#include "include/Balle3d.h"
#include "include/Balle.h"
#include "include/Couleur.h"
#include "include/Parseur.h"


using namespace std;

Balle3D::Balle3D(Vecteur const& position, Vecteur const& vitesse, double const& rayon, double const& masse_volumique, Vecteur const& force, Couleur const& color)
    :Balle(position, vitesse, rayon, masse_volumique, force, color), m_sphere(gluNewQuadric())
{

}

Balle3D::~Balle3D()
{
    gluDeleteQuadric(m_sphere);
}

void Balle3D::dessine() const
{
    glPushMatrix();
	glColor4d(m_couleur.rouge(), m_couleur.vert(), m_couleur.bleu(), m_couleur.alpha());
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glTranslated(position()[0], position()[1], position()[2]);
	gluSphere(m_sphere, rayon(), 30, 30);
    glPopMatrix();
}

string Balle3D::type() const
{
    return "Balle3D";
}


void Balle3D::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::BALLE3D + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::POSITION + "> "
        << position() << " </" + Parseur::POSITION + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::VITESSE + "> "
        << vitesse() << " </" + Parseur::VITESSE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FORCE + "> "
        << force() << " </" + Parseur::FORCE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::RAYON + "> "
        << rayon() << " </" + Parseur::RAYON + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::MASSE_VOLUMIQUE + "> "
        << masse_volumique() << " </" + Parseur::MASSE_VOLUMIQUE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> "
        << m_couleur.rouge() << " " << m_couleur.vert() << " "<< m_couleur.bleu() << " "  << m_couleur.alpha() << " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::BALLE3D + ">" << endl;

}
