/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file PlanFini3d.cpp
 * @brief D�finition des m�thodes de la classe PlanFini3D
 */
#include <GL/glu.h>
#include <string>
#include "include/PlanFini3d.h"
#include "include/Couleur.h"
#include "include/Vecteur.h"
#include "include/PlanFini.h"
#include "include/Parseur.h"

using namespace std;

PlanFini3D::PlanFini3D(Vecteur const& origine, Vecteur const& normale, Vecteur const& longueur, Vecteur const& largeur, Couleur const& color, double const& alpha)
    :PlanFini(origine, normale, longueur, largeur, color, alpha)
{
}

PlanFini3D::~PlanFini3D()
{

}

void PlanFini3D::dessine() const
{

    glPushMatrix();
    glColor4d(m_couleur.rouge(), m_couleur.vert(), m_couleur.bleu(), m_couleur.alpha());
    glBegin(GL_QUADS); // On place les 4 points a l'aide de origine, longueur et largeur (plus simple que pour Plan3D) 
    glVertex3d(origine()[0],origine()[1],origine()[2]);
    glVertex3d((origine()+longueur())[0],(origine()+longueur())[1],(origine()+longueur())[2]);
    glVertex3d((origine()+longueur()+largeur())[0],(origine()+longueur()+largeur())[1],(origine()+longueur()+largeur())[2]);
    glVertex3d((origine()+largeur())[0],(origine()+largeur())[1],(origine()+largeur())[2]);
    glEnd();
    glPopMatrix();
}

string PlanFini3D::type() const
{
    return "PlanFini3D";
}

void PlanFini3D::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::PLAN_FINI3D + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ORIGINE + "> "
        << origine() << " </" + Parseur::ORIGINE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::NORMALE + "> "
        << normale() << " </" + Parseur::NORMALE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LONGUEUR + "> "
        << longueur() << " </" + Parseur::LONGUEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LARGEUR + "> "
        << largeur() << " </" + Parseur::LARGEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COEFFICIENT_REBOND + "> "
        << alpha() << " </" + Parseur::COEFFICIENT_REBOND + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> "
        << m_couleur.rouge() << " " << m_couleur.vert() << " "<< m_couleur.bleu() << " "  << m_couleur.alpha() << " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::PLAN_FINI3D + ">" << endl;

}
