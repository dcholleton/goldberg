/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file PlanFini.cpp
 * @brief D�finition des m�thodes de la classe PlanFini
 */
#include <iostream>
#include <string>
#include <QString>
#include <QFormLayout>
#include <QLabel>
#include "include/PlanFini.h"
#include "include/Vecteur.h"
#include "include/Plan.h"
#include "include/Couleur.h"
#include "include/ObjetMobile.h"
#include "include/Parseur.h"

using namespace std;

PlanFini::PlanFini(Vecteur const& origine, Vecteur const& normale, Vecteur const& longueur, Vecteur const& largeur, Couleur const& color, double const& alpha)
    : Plan(origine, normale, color, alpha), m_longueur(longueur - (longueur * this->normale()) * this->normale()), m_largeur(largeur - (largeur * longueur.unitaire()) * longueur.unitaire() - (largeur * this->normale()) * this->normale())
{

}


PlanFini::~PlanFini()
{

}

Vecteur PlanFini::pointPlusProche(ObjetMobile const& objet) const
{
	// On a suivi ici les indications du compl�ments math�matique
    double L = m_longueur.norme();
    double l = m_largeur.norme();
    Vecteur e_L = m_longueur.unitaire();
    Vecteur e_l = m_largeur.unitaire();
    Vecteur X_2 = Plan::pointPlusProche(objet);
    Vecteur O = origine();

    double X_kL = ( X_2 - O ) * e_L ;
    double X_kl = ( X_2 - O ) * e_l ;

    if (X_kL > L)
    {
        X_2 -= ( X_kL - L ) * e_L;
    }
    else if (X_kL < 0)
    {
        X_2 -= X_kL * e_L;
    }

    if (X_kl > l)
    {
        X_2 -= ( X_kl - l ) * e_l;
    }
    else if (X_kl < 0)
    {
        X_2 -= X_kl * e_l;
    }

    return X_2;
}

Vecteur const& PlanFini::longueur() const
{
    return m_longueur;
}

Vecteur const& PlanFini::largeur() const
{
    return m_largeur;
}

void PlanFini::affiche(ostream& out) const
{
    out << "un plan fini :"						 << endl
        << origine()  << "# origine plan"  << endl
        << normale()  << "# normale plan"  << endl
        << m_longueur       << "# longueur plan" << endl
        << m_largeur        << "# largeur plan"  << endl
        << endl;
}

string PlanFini::type() const
{
    return "PlanFini";
}
QFormLayout* PlanFini::proprietesLayout() const
{
    QFormLayout* mon_layout = new QFormLayout;
    QLabel* obj_type = new QLabel(QString::fromStdString(type()));
    mon_layout->addRow("Objet : ", obj_type);
    QLabel* obj_orig = new QLabel(QString::number(origine()[0]) + "  " + QString::number(origine()[1]) + "  " + QString::number(origine()[2]));
    mon_layout->addRow("Origine : ", obj_orig);
    QLabel* obj_norm = new QLabel(QString::number(normale()[0]) + "  " + QString::number(normale()[1]) + "  " + QString::number(normale()[2]));
    mon_layout->addRow("Normale au plan : ", obj_norm);
    QLabel* obj_long = new QLabel(QString::number(longueur()[0]) + "  " + QString::number(longueur()[1]) + "  " + QString::number(longueur()[2]));
    mon_layout->addRow("Longueur : ", obj_long);
    QLabel* obj_larg = new QLabel(QString::number(largeur()[0]) + "  " + QString::number(largeur()[1]) + "  " + QString::number(largeur()[2]));
    mon_layout->addRow("Largeur : ", obj_larg);
    QLabel* obj_alpha = new QLabel(QString::number(alpha()));
    mon_layout->addRow("Coefficient de rebonds : ", obj_alpha);
    return mon_layout;
}


void PlanFini::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::PLAN_FINI + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ORIGINE + "> "
        << origine() << " </" + Parseur::ORIGINE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::NORMALE + "> "
        << normale() << " </" + Parseur::NORMALE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LONGUEUR + "> "
        << longueur() << " </" + Parseur::LONGUEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LARGEUR + "> "
        << largeur() << " </" + Parseur::LARGEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COEFFICIENT_REBOND + "> "
        << alpha() << " </" + Parseur::COEFFICIENT_REBOND + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> "
        << m_couleur.rouge() << " " << m_couleur.vert() << " "<< m_couleur.bleu() << " "  << m_couleur.alpha() << " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::PLAN_FINI + ">" << endl;

}
