/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */




/**
 * @file Goldberg.cpp
 * @brief Programme final
 */
#include <QApplication>
#include <string>
#include "include/Systeme.h"
#include "include/Element.h"
#include "include/Vecteur.h"
#include "include/Balle.h"
#include "include/Balle3d.h"
#include "include/Pendule3d.h"
#include "include/Ressort3d.h"
#include "include/Brique.h"
#include "include/Brique3d.h"
#include "include/Pendule.h"
#include "include/Ressort.h"
#include "include/Plan.h"
#include "include/Plan3d.h"
#include "include/PlanFini3d.h"
#include "include/PlanFini.h"
#include "include/ChampForces.h"
#include "include/GUI/gui.h"
#include "include/GUI/FenetrePrincipale.h"
#include "include/Ventilateur.h"
#include "include/Trampoline.h"
#include "include/Ventilateur3d.h"
#include "include/ChampForcesGlobal.h"
#include "include/Trampoline3d.h"
#include "include/Parseur.h"
#include <string>

using namespace std;

string traiteArguments(int &nb, char* argv[]);

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Systeme mon_sys;
    string adresse = traiteArguments(argc, argv);
    Parseur parseur(adresse, &mon_sys);
    parseur.extraire();
    FenetrePrincipale ma_fen(&mon_sys, "Projet d'Informatique 2012 - Dana�l Cholleton - Joackim De Figueiredo - Machines de Goldberg", 0);
    ma_fen.show();
    return app.exec();
}

string traiteArguments(int& nb, char* argv[])
{


    if (nb == 3 )
    {
        string option = argv[1];
        if (option == "-s")
        {
            return argv[2];
        }
    }
    else
    {
        return "";
    }

}
