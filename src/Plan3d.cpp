/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Plan3d.cpp
 * @brief D�finition des m�thodes de la classe Plan3D
 */

#include <GL/glu.h>
#include <cmath>
#include <string>
#include "include/Couleur.h"
#include "include/Vecteur.h"
#include "include/Plan3d.h"
#include "include/Plan.h"
#include "include/Parseur.h"

using namespace std;

Plan3D::Plan3D(Vecteur const& origine, Vecteur const& normale, Couleur const& color, double const& alpha)
    :Plan(origine, normale, color, alpha)
{
}

Plan3D::~Plan3D()
{

}

void Plan3D::dessine() const
{
	
    double phi = acos(normale()*Vecteur(0,0,1))/2/M_PI*360;
    Vecteur normale_plan = normale() - (normale()*Vecteur(0,0,1))*Vecteur(0,0,1);
    double cosinus = Vecteur(1,0,0) * normale_plan;
    double sinus = Vecteur(0,1,0) * normale_plan;
    double theta = acos(cosinus)/2/M_PI*360;

	// A cause de je ne sait pas trop quel probl�me math�matique ou d'utilisation d'OpenGL, je dois faire quelques modifications en fonction de l'angle et de la normale ...
    if (cosinus <=  0 && sinus >= 0)
    {
        phi *= -1;
    }
    else if (cosinus > 0 && sinus <  0)
    {
        phi *= -1;
    }

    if (normale()[0] == 0 && normale()[1] < 0)
    {
        sinus *= -1;
    }
    else if (normale()[0] > 0 && normale()[1] > 0)
    {
        sinus *= -1;
    }
    else if (normale()[0] > 0 && normale()[1] < 0)
    {
        cosinus *= -1;
    }
    else if (normale()[0] < 0 && normale()[1] == 0)
    {
        cosinus *= -1;
    }
    else if (normale()[0] < 0 && normale()[1] > 0 )
    {
        cosinus *= -1;
    }
    else if (normale()[0] < 0 && normale()[1] < 0)
    {
        sinus *= -1;
    }

    glPushMatrix();
    glColor4d(m_couleur.rouge(), m_couleur.vert(), m_couleur.bleu(), m_couleur.alpha());
    // On se met dans le r�f�rentiel du Plan 
    glTranslated(origine()[0],origine()[1],origine()[2]);
    glRotated(phi,sinus,cosinus,0);
    glRotated(theta,0,0,1);

    glBegin(GL_QUADS); // Et on place les 4 points tr�s loin :D
    glVertex3d(1000,1000,0);
    glVertex3d(-1000,1000,0);
    glVertex3d(-1000,-1000,0);
    glVertex3d(1000,-1000,0);
    glEnd();
    glPopMatrix();

}

string Plan3D::type() const
{
    return "Plan3D";
}

void Plan3D::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::PLAN3D + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ORIGINE + "> "
        << origine() << " </" + Parseur::ORIGINE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::NORMALE + "> "
        << normale() << " </" + Parseur::NORMALE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COEFFICIENT_REBOND + "> "
        << alpha() << " </" + Parseur::COEFFICIENT_REBOND + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> "
        << m_couleur.rouge() << " " << m_couleur.vert() << " "<< m_couleur.bleu() << " "  << m_couleur.alpha() << " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::PLAN3D + ">" << endl;

}
