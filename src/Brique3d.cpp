/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Brique3d.cpp
 * @brief D�finition des m�thodes de la classe Brique3D
 */
#include <string>
#include <GL/glu.h>
#include "include/Brique3d.h"
#include "include/Vecteur.h"
#include "include/Brique.h"
#include "include/Couleur.h"
#include "include/Parseur.h"


using namespace std;
Brique3D::Brique3D(Vecteur const& origine, Vecteur const& longueur, Vecteur const& largeur, double const& hauteur, Couleur const& colorA, Couleur const& colorB, Couleur const& colorC, Couleur const& colorD, Couleur const& colorE, Couleur const& colorF, double const& alpha)
    :Brique(origine, longueur, largeur, hauteur, colorA, colorB, colorC, colorD, colorE, colorF, alpha)
{
}

Brique3D::~Brique3D()
{

}

Brique3D::Brique3D(Brique const& brique)
    :Brique(brique)
{


}

void Brique3D::dessine() const
{
    glPushMatrix();
    
    // On construit les 6 faces avec leurs Couleurs respectives
    glBegin(GL_QUADS);

    glColor4d(m_couleurA.rouge(),  m_couleurA.vert(), m_couleurA.bleu(), m_couleurA.alpha());

    glVertex3d(origine()[0],origine()[1],origine()[2]);
    glVertex3d((origine()+longueur())[0],(origine()+longueur())[1],(origine()+longueur())[2]);
    glVertex3d((origine()+longueur()+largeur())[0],(origine()+longueur()+largeur())[1],(origine()+longueur()+largeur())[2]);
    glVertex3d((origine()+largeur())[0],(origine()+largeur())[1],(origine()+largeur())[2]);

    glColor4d(m_couleurF.rouge(),  m_couleurF.vert(), m_couleurF.bleu(), m_couleurF.alpha());

    glVertex3d(origine()[0],origine()[1],origine()[2]);
    glVertex3d((origine()+longueur())[0],(origine()+longueur())[1],(origine()+longueur())[2]);
    glVertex3d((origine()+longueur()-(normale()*hauteur()))[0],(origine()+longueur()-(normale()*hauteur()))[1],(origine()+longueur()-(normale()*hauteur()))[2]);
    glVertex3d((origine()-(normale()*hauteur()))[0],(origine()-(normale()*hauteur()))[1],(origine()-(normale()*hauteur()))[2]);

    glColor4d(m_couleurC.rouge(),  m_couleurC.vert(), m_couleurC.bleu(), m_couleurC.alpha());

    glVertex3d(origine()[0],origine()[1],origine()[2]);
    glVertex3d((origine()+largeur())[0],(origine()+largeur())[1],(origine()+largeur())[2]);
    glVertex3d((origine()+largeur()-(normale()*hauteur()))[0],(origine()+largeur()-(normale()*hauteur()))[1],(origine()+largeur()-(normale()*hauteur()))[2]);
    glVertex3d((origine()-(normale()*hauteur()))[0],(origine()-(normale()*hauteur()))[1],(origine()-(normale()*hauteur()))[2]);

    glColor4d(m_couleurE.rouge(),  m_couleurE.vert(), m_couleurE.bleu(), m_couleurE.alpha());

    glVertex3d((origine()+largeur())[0],(origine()+largeur())[1],(origine()+largeur())[2]);
    glVertex3d((origine()+longueur()+largeur())[0],(origine()+longueur()+largeur())[1],(origine()+longueur()+largeur())[2]);
    glVertex3d((origine()+longueur()-(normale()*hauteur())+largeur())[0],(origine()+longueur()-(normale()*hauteur())+largeur())[1],(origine()+longueur()-(normale()*hauteur())+largeur())[2]);
    glVertex3d((origine()-(normale()*hauteur())+largeur())[0],(origine()-(normale()*hauteur())+largeur())[1],(origine()-(normale()*hauteur())+largeur())[2]);

    glColor4d(m_couleurB.rouge(),  m_couleurB.vert(), m_couleurB.bleu(), m_couleurB.alpha());

    glVertex3d((origine()-(normale()*hauteur()))[0],(origine()-(normale()*hauteur()))[1],(origine()-(normale()*hauteur()))[2]);
    glVertex3d((origine()+longueur()-(normale()*hauteur()))[0],(origine()+longueur()-(normale()*hauteur()))[1],(origine()+longueur()-(normale()*hauteur()))[2]);
    glVertex3d((origine()+longueur()+largeur()-(normale()*hauteur()))[0],(origine()+longueur()+largeur()-(normale()*hauteur()))[1],(origine()+longueur()+largeur()-(normale()*hauteur()))[2]);
    glVertex3d((origine()+largeur()-(normale()*hauteur()))[0],(origine()+largeur()-(normale()*hauteur()))[1],(origine()+largeur()-(normale()*hauteur()))[2]);

    glColor4d(m_couleurD.rouge(),  m_couleurD.vert(), m_couleurD.bleu(), m_couleurD.alpha());

    glVertex3d((origine()+longueur())[0],(origine()+longueur())[1],(origine()+longueur())[2]);
    glVertex3d((origine()+largeur()+longueur())[0],(origine()+largeur()+longueur())[1],(origine()+largeur()+longueur())[2]);
    glVertex3d((origine()+largeur()-(normale()*hauteur())+longueur())[0],(origine()+largeur()-(normale()*hauteur())+longueur())[1],(origine()+largeur()-(normale()*hauteur())+longueur())[2]);
    glVertex3d((origine()-(normale()*hauteur())+longueur())[0],(origine()-(normale()*hauteur())+longueur())[1],(origine()-(normale()*hauteur())+longueur())[2]);


    glEnd();
    glPopMatrix();
}

string Brique3D::type() const
{
    return "Brique3D";
}

void Brique3D::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::BRIQUE3D + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ORIGINE + "> "
        << origine() << " </" + Parseur::ORIGINE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LONGUEUR + "> "
        << longueur() << " </" + Parseur::LONGUEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LARGEUR + "> "
        << largeur() << " </" + Parseur::LARGEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COEFFICIENT_REBOND + "> "
        << alpha() << " </" + Parseur::COEFFICIENT_REBOND + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::HAUTEUR + "> "
        << hauteur() << " </" + Parseur::HAUTEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> " << endl

        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_A + "> "
        << m_couleurA.rouge() << " " << m_couleurA.vert() << " " << m_couleurA.bleu() << " " << m_couleurA.alpha() << " </" + Parseur::FACE_A + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_B + "> "
        << m_couleurB.rouge() << " " << m_couleurB.vert() << " " << m_couleurB.bleu() << " " << m_couleurB.alpha() << " </" + Parseur::FACE_B + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_C + "> "
        << m_couleurC.rouge() << " " << m_couleurC.vert() << " " << m_couleurC.bleu() << " " << m_couleurC.alpha() << " </" + Parseur::FACE_C + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_D + "> "
        << m_couleurD.rouge() << " " << m_couleurD.vert() << " " << m_couleurD.bleu() << " " << m_couleurD.alpha() << " </" + Parseur::FACE_D + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_E + "> "
        << m_couleurE.rouge() << " " << m_couleurE.vert() << " " << m_couleurE.bleu() << " " << m_couleurE.alpha() << " </" + Parseur::FACE_E + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_F + "> "
        << m_couleurF.rouge() << " " << m_couleurF.vert() << " " << m_couleurF.bleu() << " " << m_couleurF.alpha() << " </" + Parseur::FACE_F + ">" << endl

        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::BRIQUE3D + ">" << endl;

}
