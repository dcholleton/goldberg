/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Brique.cpp
 * @brief D�finition des m�thodes de la classe Brique
 */

#include <iostream>
#include <QFormLayout>
#include <QString>
#include <QLabel>
#include "include/Brique.h"
#include "include/PlanFini.h"
#include "include/Vecteur.h"
#include "include/Obstacle.h"
#include "include/Couleur.h"
#include "include/ObjetMobile.h"
#include "include/Parseur.h"

using namespace std;

Brique::Brique(Vecteur const& origine, Vecteur const& longueur, Vecteur const& largeur, double const& hauteur, Couleur const& colorA, Couleur const& colorB, Couleur const& colorC, Couleur const& colorD, Couleur const& colorE, Couleur const& colorF, double const& alpha)
    : Obstacle(origine, (longueur^(largeur - (largeur * longueur.unitaire()) * longueur.unitaire())), alpha ), m_couleurA(colorA), m_couleurB(colorB), m_couleurC(colorC), m_couleurD(colorD), m_couleurE(colorE), m_couleurF(colorF), m_longueur(longueur), m_largeur(largeur - (largeur * longueur.unitaire()) * longueur.unitaire()), m_hauteur(hauteur)
{

}


Brique::~Brique()
{

}

Vecteur Brique::pointPlusProche(ObjetMobile const& objet) const
{
    Vecteur O = origine();
    Vecteur n = normale();
    double L = m_longueur.norme();
    double l = m_largeur.norme();
    double h = m_hauteur;
    Vecteur e_L = m_longueur.unitaire();
    Vecteur e_l = m_largeur.unitaire();

	// On consrtuit des PlanFini qui repr�sentent les 6 faces de la Brique
    PlanFini face1(O, n, L*e_L, l*e_l);
    PlanFini face2(O, -e_l, h*-n, L*e_L);
    PlanFini face3(O, -e_L, l*e_l, h*-n);
    PlanFini face4(O + L*e_L, e_L, h*-n, l*e_l);
    PlanFini face5(O + l*e_l , e_l, L*e_L, h*-n);
    PlanFini face6(O - h*n, -n, l*e_l, L*e_L);

	// On calcule pour chaque PlanFini le point le plus proche de l'objet mobile
    Vecteur x1 = face1.pointPlusProche(objet);
    Vecteur x2 = face2.pointPlusProche(objet);
    Vecteur x3 = face3.pointPlusProche(objet);
    Vecteur x4 = face4.pointPlusProche(objet);
    Vecteur x5 = face5.pointPlusProche(objet);
    Vecteur x6 = face6.pointPlusProche(objet);

	// On cherche ensuite la distance la plus courte
    Vecteur x = x1;

    if (( x - objet.position() ).norme() > ( x2 - objet.position() ).norme())
    {
        x = x2;
    }
    if (( x - objet.position() ).norme() > ( x3 - objet.position() ).norme())
    {
        x = x3;
    }
    if (( x - objet.position() ).norme() > ( x4 - objet.position() ).norme())
    {
        x = x4;
    }
    if (( x - objet.position() ).norme() > ( x5 - objet.position() ).norme())
    {
        x = x5;
    }
    if (( x - objet.position() ).norme() > ( x6 - objet.position() ).norme())
    {
        x = x6;
    }

	// Et on renvoi cette distance
    return x;
}

Vecteur const& Brique::longueur() const
{
    return m_longueur;
}

Vecteur const& Brique::largeur() const
{
    return m_largeur;
}

double const& Brique::hauteur() const
{
    return m_hauteur;
}

void Brique::affiche(ostream& out) const
{
    out << "une brique :"                    << endl
        << origine()  << "# origine brique"  << endl
        << m_longueur << "# longueur brique" << endl
        << m_largeur  << "# largeur brique"  << endl
        << m_hauteur  << " # hauteur brique" << endl
        << endl;
}

string Brique::type() const
{
    return "Brique";
}

QFormLayout* Brique::proprietesLayout() const
{
    QFormLayout* mon_layout = new QFormLayout;
    QLabel* obj_type = new QLabel(QString::fromStdString(type()));
    mon_layout->addRow("Objet : ", obj_type);
    QLabel* obj_orig = new QLabel(QString::number(origine()[0]) + "  " + QString::number(origine()[1]) + "  " + QString::number(origine()[2]));
    mon_layout->addRow("Origine : ", obj_orig);
    QLabel* obj_norm = new QLabel(QString::number(normale()[0]) + "  " + QString::number(normale()[1]) + "  " + QString::number(normale()[2]));
    mon_layout->addRow("Normale : ", obj_norm);
    QLabel* obj_long = new QLabel(QString::number(longueur()[0]) + "  " + QString::number(longueur()[1]) + "  " + QString::number(longueur()[2]));
    mon_layout->addRow("Longueur : ", obj_long);
    QLabel* obj_larg = new QLabel(QString::number(largeur()[0]) + "  " + QString::number(largeur()[1]) + "  " + QString::number(largeur()[2]));
    mon_layout->addRow("Largeur : ", obj_larg);
    QLabel* obj_haut = new QLabel(QString::number(hauteur()));
    mon_layout->addRow("Hauteur : ", obj_haut);
    QLabel* obj_alpha = new QLabel(QString::number(alpha()));
    mon_layout->addRow("Coefficient de rebonds : ", obj_alpha);
    return mon_layout;
}


void Brique::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::BRIQUE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ORIGINE + "> "
        << origine() << " </" + Parseur::ORIGINE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LONGUEUR + "> "
        << longueur() << " </" + Parseur::LONGUEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::LARGEUR + "> "
        << largeur() << " </" + Parseur::LARGEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COEFFICIENT_REBOND + "> "
        << alpha() << " </" + Parseur::COEFFICIENT_REBOND + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::HAUTEUR + "> "
        << hauteur() << " </" + Parseur::HAUTEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> " << endl

        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_A + "> "
        << m_couleurA.rouge() << " " << m_couleurA.vert() << " " << m_couleurA.bleu() << " " << m_couleurA.alpha() << " </" + Parseur::FACE_A + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_B + "> "
        << m_couleurB.rouge() << " " << m_couleurB.vert() << " " << m_couleurB.bleu() << " " << m_couleurB.alpha() << " </" + Parseur::FACE_B + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_C + "> "
        << m_couleurC.rouge() << " " << m_couleurC.vert() << " " << m_couleurC.bleu() << " " << m_couleurC.alpha() << " </" + Parseur::FACE_C + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_D + "> "
        << m_couleurD.rouge() << " " << m_couleurD.vert() << " " << m_couleurD.bleu() << " " << m_couleurD.alpha() << " </" + Parseur::FACE_D + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_E + "> "
        << m_couleurE.rouge() << " " << m_couleurE.vert() << " " << m_couleurE.bleu() << " " << m_couleurE.alpha() << " </" + Parseur::FACE_E + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::FACE_F + "> "
        << m_couleurF.rouge() << " " << m_couleurF.vert() << " " << m_couleurF.bleu() << " " << m_couleurF.alpha() << " </" + Parseur::FACE_F + ">" << endl

        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::BRIQUE + ">" << endl;

}
