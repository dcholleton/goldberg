/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file IntegrateurEuler.cpp
 * @brief D�finition des m�thodes de la classe IntegrateurEuler
 */
#include "include/Integrateur.h"
#include "../include/IntegrateurEuler.h"
#include "../include/Vecteur.h"
#include "../include/ObjetMobile.h"


using namespace std;

IntegrateurEuler::IntegrateurEuler(double const& dt)
    : Integrateur(dt)
{

}

IntegrateurEuler::~IntegrateurEuler()
{

}

void IntegrateurEuler::integre(ObjetMobile& objet) const
{
	// Impl�mant� selon le compl�ment math�matique
    objet.set_d_omega( objet.d_omega() + dt() * objet.evolution() );
    objet.set_omega( objet.omega() + dt() * objet.d_omega() );
}
