/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file ChampForces.cpp
 * @brief D�finition des m�thodes de la classe ChampForces
 */
#include <cmath>
#include <iostream>
#include <string>
#include <QFormLayout>
#include <QString>
#include <QLabel>
#include "include/ChampForces.h"
#include "include/Vecteur.h"
#include "include/ObjetMobile.h"
#include "include/Systeme.h"

using namespace std;

ChampForces::ChampForces(Vecteur intensite)
    : m_intensite(intensite)
{


}

ChampForces::~ChampForces()
{
	
}


void ChampForces::ajoute_a(Systeme *syst)
{
    syst->ajoute(this);
}

Vecteur const& ChampForces::intensite() const
{
    return m_intensite;
}

double ChampForces::distance(ObjetMobile const& objet) const
{
    return 0;
}


