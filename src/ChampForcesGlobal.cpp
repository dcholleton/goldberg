/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file ChampForcesGlobal.cpp
 * @brief D�finition des m�thodes de la classe ChampForcesGlobal
 */
#include "include/ChampForcesGlobal.h"
#include <QLabel>
#include <cmath>
#include "include/Parseur.h"

using namespace std;

ChampForcesGlobal::ChampForcesGlobal(Vecteur intensite)
    :ChampForces(intensite)
{
}

ChampForcesGlobal::~ChampForcesGlobal()
{
	
}

string ChampForcesGlobal::type() const
{
    return "Champ de Force Global";
}

QFormLayout* ChampForcesGlobal::proprietesLayout() const
{
    QFormLayout* mon_layout = new QFormLayout;
    QLabel* obj_type = new QLabel(QString::fromStdString(type()));
    mon_layout->addRow("�l�ment : ", obj_type);
    QLabel* obj_intensite = new QLabel(QString::number(intensite()[0]) + "  " + QString::number(intensite()[1]) + "  " + QString::number(intensite()[2]));
    mon_layout->addRow("Intensit� : ", obj_intensite);
    return mon_layout;
}

void ChampForcesGlobal::affiche(ostream& out) const
{
    out     << "un champ de forces global : "                      << endl
            << intensite() << "# intensite champ de forces global" << endl
            << endl;

}

void ChampForcesGlobal::agit_sur(ObjetMobile& objet)
{
    objet.ajoute_force( (objet.masse()  - 4/3*M_PI*objet.rayon()*objet.rayon()*objet.rayon() * Constantes::RHO_AIR) * intensite() );

}

void ChampForcesGlobal::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::CHAMP_FORCE_GLOBAL + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::INTENSITE + "> "
        << intensite() << " </" + Parseur::INTENSITE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::CHAMP_FORCE_GLOBAL + ">" << endl;

}
