/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Vecteur.cpp
 * @brief D�finition des m�thodes de la classe Vecteur
 */
#include "include/Vecteur.h"
#include <iostream>
#include <vector>
#include <cmath>
#include <initializer_list>

using namespace std;

Vecteur::Vecteur(double dimension)
{
    m_composantes.resize(dimension, 0.);
}

Vecteur::Vecteur(double x, double y, double z)
    : m_composantes( {x,y,z})
{

}

Vecteur::Vecteur(initializer_list<double> const& coordonnees)
    : m_composantes(coordonnees)
{

}

void Vecteur::augmente(double const& valeur)
{
    m_composantes.push_back(valeur);
}

vector<double> const& Vecteur::composantes() const
{
    return m_composantes;
}

size_t Vecteur::size() const
{
    return m_composantes.size();
}

const double& Vecteur::operator[](size_t const& i) const
{
    return m_composantes[i];
}

double& Vecteur::operator()(size_t const& i)
{
    return m_composantes[i];
}

void Vecteur::affiche(ostream& out) const
{
    if (m_composantes.empty())
    {
        out << "(Vecteur vide)";
    }
    else
    {
for (double i : m_composantes)
        {
            out << i << ' ';
        }
    }
}

ostream& operator<<(ostream& out, Vecteur const& a_afficher)
{
    a_afficher.affiche(out);
    return out;
}

bool Vecteur::operator==(Vecteur const& a_comparer) const
{
    return m_composantes == a_comparer.m_composantes;
}

bool Vecteur::operator!=(Vecteur const& a_comparer) const
{
    return !(*this == a_comparer);
}

Vecteur& Vecteur::operator+=(Vecteur const& a_ajouter)
{
    if (a_ajouter.size() <= size())
    {
        for (size_t i(0) ; i < a_ajouter.size() ; ++i)
        {
            (*this)(i) += a_ajouter[i];
        }
    }
    else
    {
        for (size_t i(0); i < size() ; ++i)
        {
            (*this)(i) += a_ajouter[i];
        }
        for (size_t i(size()) ; i < a_ajouter.size() ; ++i)
        {
            augmente(a_ajouter[i]);
        }
    }
    return *this;
}

Vecteur Vecteur::operator+(Vecteur const& a_ajouter) const
{
    Vecteur resultat(*this);
    return (resultat += a_ajouter);
}

Vecteur Vecteur::operator-() const
{
    Vecteur resultat(0);
for (double i : m_composantes)
    {
        resultat.augmente(-i);
    }
    return resultat;
}

Vecteur& Vecteur::operator-=(Vecteur const& a_soustraire)
{
    *this += -a_soustraire;
    return *this;
}

Vecteur Vecteur::operator-(Vecteur const& a_soustraire) const
{
    Vecteur resultat(*this);
    return (resultat -= a_soustraire);
}

Vecteur& Vecteur::operator*=(double const& scalaire)
{
    for (size_t i(0) ; i < size() ; ++i )
    {
        (*this)(i) *= scalaire;
    }
    return *this;
}

Vecteur operator*(double scalaire, Vecteur const& a_multiplier)
{
    Vecteur resultat(a_multiplier);
    resultat *= scalaire;
    return resultat;
}

Vecteur& Vecteur::operator/=(double const& scalaire)
{
    *this *= 1/scalaire;
    return *this;
}

Vecteur operator/(Vecteur const& a_diviser, double scalaire)
{
    Vecteur resultat(a_diviser);
    resultat /= scalaire;
    return resultat;
}

Vecteur operator*(Vecteur const& a_multiplier, double scalaire)
{
    return scalaire*a_multiplier;
}

double Vecteur::operator*(Vecteur const& a_multiplier_scal) const
{
    double resultat(0);
    size_t mini = min (size(), a_multiplier_scal.size());
    for (size_t i(0) ; i < mini ; ++i)
    {
        resultat += (*this)[i]*a_multiplier_scal[i];
    }
    return resultat;
}

Vecteur& Vecteur::operator^=(Vecteur const& a_multiplier_vect)
{
    Vecteur temporaire(0);
    if ( size() == 3 && a_multiplier_vect.size() == 3 )
    {
        temporaire.augmente((*this)[1]*a_multiplier_vect[2] - (*this)[2]*a_multiplier_vect[1]);
        temporaire.augmente((*this)[2]*a_multiplier_vect[0] - (*this)[0]*a_multiplier_vect[2]);
        temporaire.augmente((*this)[0]*a_multiplier_vect[1] - (*this)[1]*a_multiplier_vect[0]);
    }
    *this = temporaire;
    return *this;
}

Vecteur Vecteur::operator^(Vecteur const& a_multiplier_vect) const
{
    Vecteur resultat(*this);
    return (resultat ^= a_multiplier_vect);
}

double Vecteur::norme() const
{
    double norme(0);
for (double i : m_composantes)
    {
        norme += i*i;
    }
    return sqrt(norme);
}

double Vecteur::norme_carre() const
{
    double norme(0);
for (double i : m_composantes)
    {
        norme += i*i;
    }
    return norme;
}

Vecteur Vecteur::unitaire() const
{
    Vecteur resultat(size());
    if (norme() != 0)
    {
        resultat = *this / norme();
    }
    return resultat;
}
