/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Trampoline.cpp
 * @brief D�finition des m�thodes de la classe Trampoline
 */

#include <iostream>
#include <QFormLayout>
#include <QString>
#include <QLabel>
#include "include/Trampoline.h"
#include "include/PlanFini.h"
#include "include/Vecteur.h"
#include "include/Obstacle.h"
#include "include/Couleur.h"
#include "include/ObjetMobile.h"
#include "include/ObjetCompose.h"
#include "include/Brique3d.h"

using namespace std;

Trampoline::Trampoline(Vecteur const& origine, Vecteur const& longueur, Vecteur const& largeur, double const& hauteur, Couleur const& colorA, Couleur const& colorB, Couleur const& colorC, Couleur const& colorD, Couleur const& colorE, Couleur const& colorF, double const& alpha)
    : m_brique1(new Brique(origine, longueur, largeur, 0.05, colorA, colorB, colorC, colorD, colorE, colorF)), m_brique2(new Brique(origine+longueur, -longueur, largeur, 0.05, colorB, colorA, colorD, colorC, colorE, colorF, alpha))
{

}


Trampoline::~Trampoline()
{

}

void Trampoline::ajoute_a(Systeme* syst)
{
    m_brique1->ajoute_a(syst);
    m_brique2->ajoute_a(syst);
}
