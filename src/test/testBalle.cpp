/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file testBalle.cpp
 * @brief Fichier de test pour les m�thodes des classes Balle et ChampForces
 */
#include <iostream>
#include "include/ObjetMobile.h"
#include "include/ChampForces.h"
#include "include/Balle.h"


using namespace std;

int main()
{
    Balle b1( {1, 2, 3}, {0, 0.1, 0.2}, 0.2, 3, {0, 0, 0}), b2( {-1, 1.2, 1.3}, {0, 0.1, 0}, 0.1, 1.1, {0, 0, 0}), b3;
    ChampForces gravite;
    cout << "Nous avons :" << endl << endl << "Une balle : " << endl << b1 << endl << endl << "Une autre balle : " << endl << b2 << endl << endl << "Et une Derni�re Balle : " << endl << b3
         << endl << endl << "Un champ de forces : " << endl << gravite << endl << endl << "Et on applique les champs de force sur ces balles";

    gravite.agit_sur(b1);
    gravite.agit_sur(b2);
    gravite.agit_sur(b3);

    cout << endl << endl << "On obtiens alors : " << endl << endl << b1 << endl << endl << b2 << endl << endl << b3 << endl << endl;
    return 0;
}
