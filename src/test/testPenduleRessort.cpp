/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file testPenduleRessort.cpp
 * @brief Fichier de test pour les m�thodes des classes Pendule et Ressort
 */
#include <iostream>
#include "include/ObjetMobile.h"
#include "include/ChampForces.h"
#include "include/Pendule.h"
#include "include/Ressort.h"
#include "include/Integrateur.h"
#include "include/IntegrateurEuler.h"
#include "include/Dessinable.h"

using namespace std;

int main()
{
    ChampForces gravite;
    Ressort ressort( Vecteur( {0.4}), Vecteur(1), 1, 0.02387324, Vecteur(1), 1.38165, 0.01, Vecteur(0, 0.707106781186547, -0.707106781186547), Vecteur(0,0,1) );
    Pendule pendule( Vecteur( {0.4}), Vecteur(1), 1, 0.02387324, Vecteur(1), 0.2, 0.005, Vecteur(1, 0, 0), Vecteur(0,2,1) );

    cout << "Nous avons :" << endl

         << "Un champ de force :" << endl
         << gravite << endl << endl

         << "un ressort" << endl
         << ressort << endl << endl

         << "un pendule" << endl
         << pendule << endl << endl;
         
        IntegrateurEuler Euler( 0.01 );

    for ( size_t i(0) ; i <= 5 ; ++i )
    {
        Euler.integre(ressort);
        Euler.integre(pendule);

        cout << "l'int�gration " << i << " donne :" << endl << endl
             << "un ressort" << endl
             <<  ressort.omega()      << "# ? ressort"          << endl
             <<  ressort.d_omega()    << "# ?' ressort"         << endl
             <<  ressort.evolution()  << "# evolution"          << endl << endl

             << "un pendule" << endl
             <<  pendule.omega()      << "# ? pendule"          << endl
             <<  pendule.d_omega()    << "# ?' pendule"         << endl
             <<  pendule.evolution()  << "# evolution"          << endl << endl;
    }

    return 0;
}
