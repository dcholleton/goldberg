/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file testIntegrateur.cpp
 * @brief Fichier de test pour les m�thodes d'Int�gration
 */
#include <iostream>
#include "include/ObjetMobile.h"
#include "include/ChampForces.h"
#include "include/Balle.h"
#include "include/Integrateur.h"
#include "include/IntegrateurEuler.h"
#include "include/Vecteur.h"

using namespace std;

int main()
{
    Balle baballe( {0,0,0}, {0,0.1,0.2}, 0.2, 3, {0, 0, 0});
    ChampForces gravite;
    IntegrateurEuler heulaire(0.01);

    gravite.agit_sur(baballe);

    cout << "Nous avons : Un champ de forces : " << endl << gravite << endl
         << "Et une Balle : " << endl << baballe << endl;

    heulaire.integre(baballe);

    cout << endl << endl << baballe << endl;

    heulaire.integre(baballe);

    cout << endl << endl << baballe << endl;

    heulaire.integre(baballe);

    cout << endl << endl << baballe << endl;

    heulaire.integre(baballe);

    cout << endl << endl << baballe << endl;

    heulaire.integre(baballe);

    cout << endl << endl << baballe << endl;

    return 0;
}
