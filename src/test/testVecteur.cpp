/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file testVecteur.cpp
 * @brief Fichier de test pour les m�thodes de la classe Vecteur
 */
#include <iostream>
#include "include/Vecteur.h"

using namespace std;

int main()
{
    Vecteur v1(1,2,-0.1), v2(2.6, 3.5, 4.1), v3(v1), v4(3), v5( {1,2});

    cout << "Vecteur 1 : " << v1 << endl;
    cout << "Vecteur 2 : " << v2 << endl;
    cout << "Vecteur 3 : " << v3 << endl;

    cout << "Le vecteur 1 est ";
    if (v1 == v2)
    {
        cout << "�gal au";
    }
    else
    {
        cout << "diff�rent du";
    }
    cout << " vecteur 2," << endl << "et est ";
    if (v1 != v3)
    {
        cout << "diff�rent du";
    }
    else
    {
        cout << "�gal au";
    }
    cout << " vecteur 3." << endl;


    cout << endl << v1 << " + " << v2 << " = " << v1 + v2;
    cout << endl << v2 << " + " << v1 << " = " << v2 + v1;
    cout << endl << "R�sultat : C'est bon ! Commutativit� : C'est bon !";

    cout << endl << endl << "On v�rifie que l'addition du vecteur nul ne fait rien (et au passage la commutativit�) ...";
    cout << endl << v1 << " + " << v4 << " = " << v1 + v4;
    cout << endl << v4 << " + " << v1 << " = " << v4 + v1;
    cout << endl << "C'est bon !";

    cout << endl;
    cout << endl << v1 << " - " << v2 << " = " << v1 - v2;
    cout << endl << "R�sultat : C'est bon !";

    cout << endl << endl << "On v�rifie que la soustraction d'un vecteur � lui-m�me donne le vecteur nul ...";
    cout << endl << v1 << " - " << v1 << " = " << v1 - v1;
    cout << endl << v2 << " - " << v2 << " = " << v2 - v2;
    cout << endl << "C'est bon !";

    cout << endl << endl << "On v�rifie que la somme de vecteurs de dimensions diff�rentes" << endl << "donne un vecteur de la plus grande des deux dimensions,"<< endl << "le plus petit vecteur �tant compl�t� par des \"0\" ...";
    cout << endl << v5 << " + " << v2 << " = " << v5 + v2;
    cout << endl << v2 << " + " << v5 << " = " << v2 + v5;
    cout << endl << "C'est bon !";

    cout << endl;
    cout << endl << "- " << v1 << " = " << -v1;
    cout << endl << "- " << v2 << " = " << -v2;

    cout << endl << endl << "On v�rifie que l'addition d'un oppos� est �gal � la soustraction ...";
    cout << endl << v1 << "- " << v2 << " = " << v1 - v2;
    cout << endl << v1 << " + (-" << v2 << ") = " << v1 + (-v2);
    cout << endl << "C'est bon !";

    cout << endl;
    cout << endl << "3 * " << v1 << " = " << 3*v1;
    cout << endl << v1 << "* 3 = " << v1*3;
    cout << endl << "R�sultat : C'est bon ! Commutativit� : C'est bon !";

    cout << endl;
    cout << endl << v1 << " * " << v2 << " = " << v1 * v2;
    cout << endl << v2 << " * " << v1 << " = " << v2 * v1;
    cout << endl << "R�sultat : C'est bon ! Commutativit� : C'est bon !";

    cout << endl;
    cout << endl << v1 << " ^ " << v2 << " = " << (v1 ^ v2);

    cout << endl;
    cout << endl << "||" << v1 << "||� = " << v1.norme_carre();
    cout << endl << "||" << v2 << "||� = " << v2.norme_carre();

    cout << endl << endl;

    return 0;
}
