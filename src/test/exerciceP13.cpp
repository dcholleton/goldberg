/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



#include <QApplication>
#include "include/Systeme.h"
#include "include/Element.h"
#include "include/Vecteur.h"
#include "include/Balle.h"
#include "include/Balle3d.h"
#include "include/Pendule3d.h"
#include "include/Ressort3d.h"
#include "include/Brique.h"
#include "include/Brique3d.h"
#include "include/Pendule.h"
#include "include/Ressort.h"
#include "include/Plan.h"
#include "include/Plan3d.h"
#include "include/PlanFini3d.h"
#include "include/PlanFini.h"
#include "include/ChampForces.h"
#include "include/gui.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Systeme mon_sys;
    mon_sys.ajoute(new Balle3D({0,0,0.3},{0,0.8,2},0.051111,3));
    mon_sys.ajoute(new Pendule3D({1.5}, {-0.5}, 0.05, 190.985931710274, {0,0,0}, 0.2, 0.005, {0,1,0}, {0,1.4,0.45}));

  //  mon_sys.ajoute(new Plan3D({0,0,0},{0,0,1}));
    mon_sys.ajoute(new Brique3D({-1,-1,0}, {3,0,0}, {0,3,0}, 1 ));
    mon_sys.ajoute(new ChampForces);
    GUI affichage(mon_sys, 50, 0, "exerciceP13");
    affichage.show();
    return app.exec();
}
