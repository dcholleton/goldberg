/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file testChocs.cpp
 * @brief Fichier de test pour les chocs d'ObjetMobiles et d'Obstacles
 */
#include <iostream>
#include "include/Vecteur.h"
#include "include/ObjetMobile.h"
#include "include/ChampForces.h"
#include "include/Balle.h"
#include "include/Plan.h"
#include "include/Obstacle.h"


using namespace std;

int main()
{
    Balle balle1( {0, 0.624, 0.0465234}, {0, 0.8, -2.59108}, 0.051111, 3),
          balle2( {0, 1.36675, 0.283821}, {0, 0.715449, 0.0957368}, 0.051111, 3),
          balle3( {0, 1.46284, 0.260128}, {0, 0.419218, 0.13874}, 0.05, 190.985931710274);
    Plan  plan;
    ChampForces gravite;

    gravite.agit_sur(balle1);
    gravite.agit_sur(balle2);
    gravite.agit_sur(balle3);

    cout << "Cas 1 :" << endl << endl;

    cout << "avant rebond sol :"                           << endl
         << balle1.vitesse()         << "# vitesse balle"  << endl
         << balle1.force()           << "# force balle"    << endl << endl
         << "calculs :"                                    << endl << endl;

    //Ici, on a un copi�/coll� de la m�thode agit_sur, mais on affiche en plus la valeur des donn�es interne au calcul

    Vecteur n(( balle1.position() - plan.pointPlusProche(balle1) ) .unitaire()),  dv(3);
    double vstar(-balle1.vitesse()*n), alpha(0.8), mu(0.01), F_n(balle1.force()*n);
    Vecteur vc(balle1.vitesse() + vstar*n);

    if (F_n < 0)
    {
        balle1.ajoute_force(-F_n*n);
    }
    if ( 7*mu*(1+alpha)*vstar >= 2*vc.norme() )
    {
        dv = (1+alpha)*vstar*n - vc*2/7;
    }
    else
    {
        dv = (1+alpha)*vstar*( n - mu*(vc.unitaire()));
    }

    balle1.set_vitesse(balle1.vitesse() + dv);

    cout << vstar                << " # vstar"                << endl
         << vc                   <<  "# vc"                   << endl
         << 7*mu*(1+alpha)*vstar << " # 7*mu*(1+alpha)*vstar" << endl
         << dv                   << "# dv"                    << endl
         << endl;

    cout << "apr�s rebond sol :" <<  endl              << endl
         << balle1.vitesse()     << "# vitesse balle"  << endl
         << balle1.force()       << "# force balle"    << endl;


    cout << endl << endl << "Cas 2 :" << endl << endl;


    cout << "avant choc :"                                                                 << endl
         << balle2.position()                               << "# position balle2"         << endl
         << balle3.position()                               << "# position balle3"         << endl
         << (balle2.position() - balle3.position()).norme() << " # distance entre centres" << endl
         << balle2.vitesse()                                << "# vitesse balle2"          << endl
         << balle3.vitesse()                                << "# vitesse balle3"          << endl
         << balle2.force()                                  << "# force balle2"            << endl
         << balle3.force()                                  << "# force balle3"    << endl << endl
         << "calculs :"                                                            << endl << endl;


    Vecteur n_bis(( balle2.position() - balle3.position() ).unitaire() ),  dv_bis(3);
    double vstar_bis((balle3.vitesse() - balle2.vitesse())*n_bis), lambda_bis((1+alpha)*(balle3.masse()/(balle2.masse()+balle3.masse()))), F_n_1(balle2.force()*n_bis), F_n_2(balle3.force()*n_bis);
    Vecteur vc_bis( balle2.vitesse() - balle3.vitesse() + vstar_bis*n_bis);

    if (F_n_1 < 0)
    {
        balle2.ajoute_force(-F_n_1*n_bis);
        balle3.ajoute_force(F_n_1*n_bis);
    }
    if (F_n_2 > 0)
    {
        balle2.ajoute_force(F_n_2*n_bis);
        balle3.ajoute_force(-F_n_2*n_bis);
    }
    if ( 7*mu*(1+alpha)*vstar_bis >= 2*vc_bis.norme() )
    {
        dv_bis = lambda_bis*vstar_bis*n_bis - 2/7*balle3.masse()/(balle3.masse()+balle2.masse())*vc_bis;
    }

    else
    {
        dv_bis = lambda_bis*vstar_bis*( n_bis - mu*vc_bis.unitaire());
    }

    balle2.set_vitesse(balle2.vitesse() + dv_bis );
    balle3.set_vitesse(balle3.vitesse() - balle2.masse()/balle3.masse()*dv_bis);

    cout << n_bis                    << "# n"                     << endl
         << lambda_bis                << " # lambda"               << endl
         << F_n_1                    << " # F1n"                  << endl
         << F_n_2                    << " # F2n"                  << endl
         << vstar_bis                << " # vstar"                << endl
         << vc_bis                   << "# vc"                    << endl
         << 7*mu*(1+alpha)*vstar_bis << " # 7 mu (1+alpha) vstar" << endl
         << 2*vc_bis.norme()         << " # 2*vc.norme()"         << endl
         << dv_bis                   << "# dv"                    << endl
         << endl;

    cout << "apr�s choc :"                                   << endl
         << balle2.vitesse()          << "# vitesse balle2"  << endl
         << balle3.vitesse()          << "# vitesse balle3"  << endl
         << balle2.force()            << "# force balle2"    << endl
         << balle3.force()            << "# force balle3"    << endl << endl;

    return 0;
}
