/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file testObstacle.cpp
 * @brief Fichier de test pour les m�thodes des classes Obstacle, Plan, PlanFini et Brique
 */#include <iostream>
#include "include/Plan.h"
#include "include/Brique.h"
#include "include/Balle.h"
#include "include/ChampForces.h"
#include "include/Obstacle.h"
#include "include/PlanFini.h"

using namespace std;

int main()
{
    Plan mon_plan( {0, 0, 0}, {0, 0, 1});
    Brique ma_brique( {0, 0, 0.5}, {0.8, 0, 0}, {0, -0.6, 0}, 0.5);
    Balle ma_balle( {1.2, -0.4, 0.3}, {0, 0, 0}, 0.2, 3);
    ChampForces gravite;

    gravite.agit_sur(ma_balle);

    cout << "Nous avons :" << endl << "un mur plan" << endl << mon_plan << endl << endl
         << "un mur brique" << endl << ma_brique << endl << endl
         << "une balle" << endl << ma_balle << endl << endl
         << "tests :" << endl << mon_plan.pointPlusProche(ma_balle) << "# point plus proche plan" << endl
         << mon_plan.distance(ma_balle) << " # distance plan" << endl
         << ma_brique.pointPlusProche(ma_balle) << "# point plus proche brique" << endl
         << ma_brique.distance(ma_balle) << " # distance brique" << endl << endl;

    ma_balle.set_omega( {1.2, -0.4, 1.3});

    cout << "Je d�place ma balle en (1.2 -0.4 1.3) " << endl << ma_balle << endl << endl
         << "tests :" << endl << mon_plan.pointPlusProche(ma_balle) << "# point plus proche plan" << endl
         << mon_plan.distance(ma_balle) << " # distance plan" << endl
         << ma_brique.pointPlusProche(ma_balle) << "# point plus proche brique" << endl
         << ma_brique.distance(ma_balle) << " # distance brique" << endl << endl;

    ma_balle.set_omega( {0.2, -0.4, 1.3});

    cout << "Je d�place ma balle en (0.2 -0.4 1.3) " << endl << ma_balle << endl << endl
         << "tests :" << endl << mon_plan.pointPlusProche(ma_balle) << "# point plus proche plan" << endl
         << mon_plan.distance(ma_balle) << " # distance plan" << endl
         << ma_brique.pointPlusProche(ma_balle) << "# point plus proche brique" << endl
         << ma_brique.distance(ma_balle) << " # distance brique" << endl;


    return 0;
}

