/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Plan.cpp
 * @brief D�finition des m�thodes de la classe Plan
 */
#include <GL/glu.h>
#include <iostream>
#include <string>
#include <QFormLayout>
#include <QString>
#include <QLabel>
#include "include/Plan.h"
#include "include/Vecteur.h"
#include "include/ObjetMobile.h"
#include "include/Couleur.h"
#include "include/Obstacle.h"
#include "include/Parseur.h"


using namespace std;

Plan::Plan(Vecteur const& origine, Vecteur const& normale, Couleur const& color, double const& alpha)
    : Obstacle(origine, normale, alpha), m_couleur(color)
{

}


Plan::~Plan()
{

}

Vecteur Plan::pointPlusProche(ObjetMobile const& objet) const
{
    return  ( objet.position() + ( (origine() - objet.position() ) * normale() ) * normale()  );
}

void Plan::affiche(ostream& out) const
{
    out << "un plan :"                    << endl
        << origine()  << "# origine plan" << endl
        << normale()  << "# normale plan" << endl
        << endl;
}

string Plan::type() const
{
    return "Plan";
}
QFormLayout* Plan::proprietesLayout() const
{
    QFormLayout* mon_layout = new QFormLayout;
    QLabel* obj_type = new QLabel(QString::fromStdString(type()));
    mon_layout->addRow("Objet : ", obj_type);
    QLabel* obj_orig = new QLabel(QString::number(origine()[0]) + "  " + QString::number(origine()[1]) + "  " + QString::number(origine()[2]));
    mon_layout->addRow("Origine : ", obj_orig);
    QLabel* obj_norm = new QLabel(QString::number(normale()[0]) + "  " + QString::number(normale()[1]) + "  " + QString::number(normale()[2]));
    mon_layout->addRow("Normale au plan : ", obj_norm);
    QLabel* obj_alpha = new QLabel(QString::number(alpha()));
    mon_layout->addRow("Coefficient de rebonds : ", obj_alpha);
    return mon_layout;
}


void Plan::enregistrer(ofstream& out ) const
{
    out << Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::PLAN + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::ORIGINE + "> "
        << origine() << " </" + Parseur::ORIGINE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::NORMALE + "> "
        << normale() << " </" + Parseur::NORMALE + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COEFFICIENT_REBOND + "> "
        << alpha() << " </" + Parseur::COEFFICIENT_REBOND + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + Parseur::TABULATION + "<" + Parseur::COULEUR + "> "
        << m_couleur.rouge() << " " << m_couleur.vert() << " "<< m_couleur.bleu() << " "  << m_couleur.alpha() << " </" + Parseur::COULEUR + ">" << endl
        << Parseur::TABULATION + Parseur::TABULATION + "</" + Parseur::PLAN + ">" << endl;

}
