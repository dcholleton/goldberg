/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file Systeme.cpp
 * @brief D�finition des m�thodes de la classe Systeme
 */
#include <iostream>
#include <memory>
#include <vector>
#include "include/Systeme.h"
#include "include/ObjetMobile.h"
#include "include/Obstacle.h"
#include "include/ChampForces.h"
#include "include/Integrateur.h"
#include "include/IntegrateurEuler.h"
#include "include/IntegrateurRungeKutta.h"
#include "include/IntegrateurNewmark.h"
#include "include/ObjetCompose.h"


using namespace std;

Systeme::Systeme(double const& dt, int const& integrateur)
{
    switch (integrateur) // En fonction de la valeur de integrateur, on utilise un type d'integrateur diff�rent, on utilise ici la liste d'�cum�ration d�finie dans Integrateur
    {
        case TypeIntegrateur::Euler :
        {
            m_integrateur = unique_ptr<Integrateur>(new IntegrateurEuler(dt));
            break;
        }
        case TypeIntegrateur::RungeKutta :
        {
            m_integrateur = unique_ptr<Integrateur>(new IntegrateurRungeKutta(dt));
            break;
        }
        case TypeIntegrateur::Newmark :
        {
            m_integrateur = unique_ptr<Integrateur>(new IntegrateurNewmark(dt));
            break;
        }
    }
}

ostream& operator<<(ostream& out, Systeme const& a_afficher)
{
    out << "Le syst�me est constitu� de :" << endl << endl;

    out << "objets mobiles :" << endl << endl;

// On it�re sur les op�rateurs d'affichage de tous les Elements
for (unique_ptr<ObjetMobile> const& obj : a_afficher.m_objets_mobiles)
    {
        out << *obj;
    }

    cout << "obstacles :" << endl << endl;

for (unique_ptr<Obstacle> const& obst : a_afficher.m_obstacles)
    {
        out << *obst;
    }

    cout << "champs de forces :" << endl << endl;

for (unique_ptr<ChampForces> const& champ : a_afficher.m_champs)
    {
        out << *champ;
    }
    return out;
}

void Systeme::dessine() const
{
	// On it�re sur les m�thodes dessine de tous les Elements
for (unique_ptr<ObjetMobile> const& obj : m_objets_mobiles)
    {
        obj->dessine();
    }

for (unique_ptr<Obstacle> const& obst : m_obstacles)
    {
        obst->dessine();
    }

for (unique_ptr<ChampForces> const& champ : m_champs)
    {
        champ->dessine();
    }
}

void Systeme::ajoute(ObjetMobile* objet)
{
    if(objet) // Si on a bien un pointeur ( objet != NULLPTR )
    {
        m_objets_mobiles.push_back(unique_ptr<ObjetMobile>(objet)); // On rajoute objet dans le Systeme sous forme de unique_ptr
        emit newObjet(objet); // On �met le signal d'ajout d'un nouvel ObjetMobile
    }
// Idem pour les 2 m�thodes suivantes
}

void Systeme::ajoute(Obstacle* obstacle)
{
    if(obstacle) 
    {
        m_obstacles.push_back(unique_ptr<Obstacle>(obstacle)); 
        emit newObstacle(obstacle); 
    }

}
void Systeme::ajoute(ChampForces* champ)
{
    if(champ)
    {
        m_champs.push_back(unique_ptr<ChampForces>(champ));
        emit newChamp(champ);
    }
}

void Systeme::ajoute(ObjetCompose* objet)
{
    objet->ajoute_a(this); // On ajoute objet au Systeme
    delete objet; // On le d�truit
}

void Systeme::evolue()
{
	// Pout tous les objet mobiles ...
for (unique_ptr<ObjetMobile> const& obj : m_objets_mobiles)
    {
        obj->reset_force(); // On r�initialise les forces 
		for (unique_ptr<ChampForces> const& champ : m_champs)
        {
            champ->agit_sur(*obj); // Puis sur chaque objet mobile on ajoute les forces exerc�e par chaque champ de forces
        }
    }

for ( size_t i = 0 ; i < m_objets_mobiles.size() ; ++i ) // Pour chaque objet mobile ...
    {
        for ( size_t j = i+1 ; j < m_objets_mobiles.size() ; ++j) // 1) sur chaque objet mobile suivant ...
        {
            m_objets_mobiles[i]->agit_sur(*m_objets_mobiles[j]); // 1) on applique la m�thode agit_sur
        }
for (unique_ptr<Obstacle> const& obst : m_obstacles) // 2) et pour tous les obstacles ...
        {
            obst->agit_sur(*m_objets_mobiles[i]); // 2) on applique le agit_sur de l'obstacle sur l'objet mobile
        }
    }

for (unique_ptr<ObjetMobile> const& obj : m_objets_mobiles) // Puis pour tous les objets mobiles
    {
        m_integrateur->integre(*obj); // on int�gre
    }
}

vector< unique_ptr<ObjetMobile> >& Systeme::objets_mobiles()
{
    return m_objets_mobiles;
}

vector< unique_ptr<Obstacle> >& Systeme::obstacles()
{
    return m_obstacles;
}

vector< unique_ptr<ChampForces> >& Systeme::champs()
{
    return m_champs;
}


void Systeme::set_dt(double const& new_dt)
{
    m_integrateur->set_dt(new_dt);
}

double Systeme::dt() const
{
    return m_integrateur->dt();
}

void Systeme::set_integrateur(int const& index)
{
    double dt = m_integrateur->dt();
	// On change d'Integrateur en fonction de index, comme dans le constructeur
    switch(index)
    {

        case 0 :
        {
            m_integrateur = unique_ptr<Integrateur>(new IntegrateurEuler(dt));
            cout << endl << "     EULER    " << endl;
            break;
        }
        case 2 :
        {
            m_integrateur = unique_ptr<Integrateur>(new IntegrateurRungeKutta(dt));
            cout << endl << "     RUNGE-KUTTA    " << endl;
            break;
        }
        case 1 :
        {
            m_integrateur = unique_ptr<Integrateur>(new IntegrateurNewmark(dt));
            cout << endl << "     NEWMARK    " << endl;
            break;
        }

    }
}

void Systeme::reset()
{
	// On r�initialise tous les vector
    m_objets_mobiles.clear();
    m_obstacles.clear();
    m_champs.clear();
}
