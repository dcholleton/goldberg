/* Projet d'informatique 2012 - Simulation g�n�rique de syst�mes physiques simples : vers les machines de Rube Goldberg
 *
 * Copyright 2012 Dana�l Cholleton & Joackim De Figueiredo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * Pour plus d'information sur le programme, contactez <danael.cholleton@epfl.ch>
 * 
 */



/**
 * @file IntegrateurNewmark.cpp
 * @brief D�finition des m�thodes de la classe IntegrateurNewmark
 */
#include "../include/Integrateur.h"
#include "../include/IntegrateurNewmark.h"
#include "../include/Vecteur.h"
#include "../include/ObjetMobile.h"
#include <cmath>



using namespace std;

IntegrateurNewmark::IntegrateurNewmark(double const& dt, double const& epsilon)
    : Integrateur(dt), m_epsilon(epsilon)
{

}

IntegrateurNewmark::~IntegrateurNewmark()
{

}

void IntegrateurNewmark::integre(ObjetMobile& objet) const
{
	// Impl�mant� selon le compl�ment math�matique
	// Initialisation
    Vecteur omega (objet.omega()), d_omega (objet.d_omega());
    Vecteur X (objet.evolution()), x(objet.evolution()), E(omega);
    do
    {
		// Et on r�p�te ...
        E = objet.omega();
        x = objet.evolution();
        objet.set_d_omega(d_omega + dt()*(X+x)/2);
        objet.set_omega(omega + dt()*d_omega + dt()*dt()*(X+0.5*x)/3 );

    }
    while( fabs(objet.omega().norme() - E.norme()) >= m_epsilon );
}

